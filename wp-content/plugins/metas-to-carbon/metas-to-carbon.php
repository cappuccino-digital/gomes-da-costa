<?php declare( strict_types = 1 );

/**
 * Plugin Name: GDC | Metas to Carbon Fields.
 * Description: Migra metas que estavam serializados para a nova estrutura.
 * Author: Elvis Henrique Pereira
 * Version: 0.1.0
 */

namespace MetasCarbon;

add_action(
	'admin_init',
	static function () {
		// phpcs:ignore
		if ( ! isset( $_GET['metas_carbon_init_product'] ) ) {
			return;
		}

		convert_product_items_meta();
	}
);

add_action(
	'admin_init',
	static function () {
		// phpcs:ignore
		if ( ! isset( $_GET['metas_carbon_init_recipe'] ) ) {
			return;
		}

		convert_recipe_products_meta();
	}
);

/**
 * Get product items.
 *
 * @return array<string>
 */
function get_old_product_items_meta(): array {
	global $wpdb;

	return $wpdb->get_results( // phpcs:ignore
		"SELECT
			post_id,
			meta_value
		FROM
			{$wpdb->postmeta} pm
		WHERE
			meta_key = '_itens_bkp' AND meta_value IS NOT NULL",
		ARRAY_A
	);
}

/**
 * Get recipe products.
 *
 * @return array<string>
 */
function get_old_recipe_products_meta(): array {
	global $wpdb;

	return $wpdb->get_results( // phpcs:ignore
		"SELECT
			post_id,
			meta_value
		FROM
			{$wpdb->postmeta} pm
		WHERE
			meta_key = '_products_bkp' AND meta_value IS NOT NULL",
		ARRAY_A
	);
}

function convert_product_items_meta(): void {
	$items = get_old_product_items_meta();

	if ( ! $items ) {
		return;
	}

	foreach ( $items as $item ) {
		if ( ! $item['meta_value'] ) {
			continue;
		}

		insert_product_item_by_post_id( unserialize( $item['meta_value'] ), (int) $item['post_id'] ); // phpcs:ignore
	}
}

function convert_recipe_products_meta(): void {
	$items = get_old_recipe_products_meta();

	if ( ! $items ) {
		return;
	}

	foreach ( $items as $item ) {
		if ( ! $item['meta_value'] ) {
			continue;
		}

		insert_recipe_products_by_post_id( unserialize( $item['meta_value'] ), (int) $item['post_id'] ); // phpcs:ignore
	}
}

/**
 * Insert product item.
 *
 * @param array<string> $item Item.
 * @param int           $post_id Post ID.
 * @return void
 */
function insert_product_item_by_post_id( array $item, int $post_id ): void {
	global $wpdb;

	$key_map = [
		'nome' => 'name',
		'qtd_porcao' => 'quantity',
		'valor_diario' => 'vd',
		'indentacao' => 'indentation',
	];

	foreach ( $item as $index => $data ) {
		foreach ( $data as $key => $value ) {
			// phpcs:disable
			$wpdb->insert(
				$wpdb->postmeta,
				[
					'post_id' => $post_id,
					'meta_key' => "_items|{$key_map[ $key ]}|{$index}|0|value",
					'meta_value' => $value,
				]
			);
			// phpcs:enable
		}
	}
}

/**
 * Insert recipe products.
 *
 * @param array<string> $item Item.
 * @param int           $post_id Post ID.
 * @return void
 */
function insert_recipe_products_by_post_id( array $items, int $post_id ): void {
	global $wpdb;

	foreach ( $items as $key => $value ) {
		// phpcs:disable
		$wpdb->insert(
			$wpdb->postmeta,
			[
				'post_id' => $post_id,
				'meta_key' => "_products|||{$key}|value",
				'meta_value' => "post:fw-produtos:{$value}",
			]
		);

		$wpdb->insert(
			$wpdb->postmeta,
			[
				'post_id' => $post_id,
				'meta_key' => "_products|||{$key}|type",
				'meta_value' => 'post',
			]
		);

		$wpdb->insert(
			$wpdb->postmeta,
			[
				'post_id' => $post_id,
				'meta_key' => "_products|||{$key}|subtype",
				'meta_value' => 'fw-produtos',
			]
		);

		$wpdb->insert(
			$wpdb->postmeta,
			[
				'post_id' => $post_id,
				'meta_key' => "_products|||{$key}|id",
				'meta_value' => $value,
			]
		);
	}
}