SELECT
    t.*
FROM
    wp_terms t
INNER JOIN wp_term_taxonomy tt ON
    tt.term_id = t.term_id
WHERE
    tt.taxonomy IN(
        'fw-produto-category',
        'fw-produto-saude',
        'fw-produto-alergico'
    )