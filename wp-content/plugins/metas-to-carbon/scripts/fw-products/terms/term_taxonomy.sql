SELECT
    tt.term_taxonomy_id,
    tt.term_id,
    tt.taxonomy,
    tt.description,
    tt.parent,
    tt.count
FROM
    wp_term_taxonomy tt
WHERE
    tt.taxonomy IN(
        'fw-produto-category',
        'fw-produto-saude',
        'fw-produto-alergico'
    )