SELECT
    pm.post_id,
    '_thumbnail_id' AS meta_key,
    getPhpSerializedArrayValueByKey(
        getPhpSerializedArrayValueByKey(pm.meta_value, 'banner-mobile'),
        'attachment_id'
    ) AS meta_value
FROM
    wp_postmeta pm
INNER JOIN wp_posts p ON
    p.ID = pm.post_id AND p.post_type = 'fw-produtos' AND p.post_status = 'publish' AND getPhpSerializedArrayValueByKey(
        getPhpSerializedArrayValueByKey(pm.meta_value, 'banner-mobile'),
        'attachment_id'
    ) IS NOT NULL