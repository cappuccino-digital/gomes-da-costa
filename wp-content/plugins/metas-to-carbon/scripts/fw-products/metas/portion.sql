SELECT
    pm.post_id,
    '_portion' AS meta_key,
    getPhpSerializedArrayValueByKey(pm.meta_value, 'porcao_livre') AS meta_value
FROM
    wp_postmeta pm
INNER JOIN wp_posts p ON
    p.ID = pm.post_id AND p.post_type = 'fw-produtos' AND p.post_status = 'publish' AND getPhpSerializedArrayValueByKey(pm.meta_value, 'porcao_livre') IS NOT NULL