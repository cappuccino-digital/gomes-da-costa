SELECT
    tr.*
FROM
    wp_term_relationships tr
INNER JOIN wp_term_taxonomy tt ON
    tt.term_taxonomy_id = tr.term_taxonomy_id
WHERE
    tt.taxonomy IN(
        'fw-receita-ocasiao',
        'fw-receita-colecao'
    )