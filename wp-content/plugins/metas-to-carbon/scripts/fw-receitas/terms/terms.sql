SELECT
    t.*
FROM
    wp_terms t
INNER JOIN wp_term_taxonomy tt ON
    tt.term_id = t.term_id
WHERE
    tt.taxonomy IN(
        'fw-receita-ocasiao',
        'fw-receita-colecao'
    )