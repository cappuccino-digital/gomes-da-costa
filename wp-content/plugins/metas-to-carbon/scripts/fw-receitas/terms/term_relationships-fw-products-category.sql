SELECT
    pm.post_id AS object_id,
    tr.term_taxonomy_id,
    tr.term_order
FROM
    wp_term_relationships tr
INNER JOIN wp_term_taxonomy tt ON
    tt.term_taxonomy_id = tr.term_taxonomy_id
INNER JOIN wp_postmeta pm ON
    pm.meta_value = tr.object_id
WHERE
    tt.taxonomy = 'fw-produto-category' AND pm.meta_key LIKE '\_products|||_|id'
GROUP BY
    pm.post_id,
    tr.term_taxonomy_id