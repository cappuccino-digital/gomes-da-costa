SELECT
    pm.post_id,
    '_chef_clue' AS meta_key,
    getPhpSerializedArrayValueByKey(pm.meta_value, 'dica_chef') AS meta_value
FROM
    wp_postmeta pm
INNER JOIN wp_posts p ON
    p.ID = pm.post_id AND p.post_type = 'fw-receitas' AND p.post_status = 'publish' AND getPhpSerializedArrayValueByKey(pm.meta_value, 'dica_chef') IS NOT NULL