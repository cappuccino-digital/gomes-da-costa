SELECT
    pm.post_id,
    '_yield' AS meta_key,
    getPhpSerializedArrayValueByKey(
        getPhpSerializedArrayValueByKey(pm.meta_value, 'porcoes'),
        'from'
    ) AS meta_value
FROM
    wp_postmeta pm
INNER JOIN wp_posts p ON
    p.ID = pm.post_id AND p.post_type = 'fw-receitas' AND p.post_status = 'publish' AND getPhpSerializedArrayValueByKey(pm.meta_value, 'porcoes') IS NOT NULL