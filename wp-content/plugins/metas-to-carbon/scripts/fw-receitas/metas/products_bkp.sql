SELECT
    pm.post_id,
    '_products_bkp' AS meta_key,
    getPhpSerializedArrayValueByKey(pm.meta_value, 'produtos') AS meta_value
FROM
    wp_postmeta pm
INNER JOIN wp_posts p ON
    p.ID = pm.post_id AND p.post_type = 'fw-receitas' AND p.post_status = 'publish' AND getPhpSerializedArrayValueByKey(pm.meta_value, 'produtos') IS NOT NULL