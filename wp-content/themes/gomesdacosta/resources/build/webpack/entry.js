/**
 * The internal dependencies.
 */
const utils = require('../lib/utils');

module.exports = {
  theme: ['@babel/polyfill', utils.srcScriptsPath('theme/index.js')],
  admin: utils.srcScriptsPath('admin/index.js'),
  login: utils.srcScriptsPath('login/index.js'),
  editor: utils.srcScriptsPath('editor/index.js'),
  blocks: utils.srcScriptsPath('blocks/index.js'),
};
