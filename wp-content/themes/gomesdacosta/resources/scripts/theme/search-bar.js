import $ from 'jquery';

const Menu = {
  create() {
    this.component = $('.header');
    this.addEventListeners();
  },

  addEventListeners() {
    this.component.on('click', '.header__iconSearch, .search-form__iconClose', this.onToggle.bind(this));
  },

  onToggle() {
    $('.search-form').toggleClass('search-form--is-active');
  },
};

export default Menu.create();
