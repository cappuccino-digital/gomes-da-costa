import Swiper from 'swiper';

(() => {
  const module = {
    slideHistory: document.querySelector('.page-about-us__story-slide-wrapper'),

    createSlideHistory() {
      if (!this.slideHistory) { return false; }

      return new Swiper(this.slideHistory, {
        slidesPerView: 1,
        spaceBetween: 30,
        autoHeight: true,
        breakpoints: {
          768: {
            slidesPerView: 2,
          },
        },
        navigation: {
          nextEl: '.page-about-us__story-slide-next',
          prevEl: '.page-about-us__story-slide-prev',
        },
      });
    },

    init() {
      if (this.slideHistory) {
        this.createSlideHistory();
      }
    },
  };

  module.init();
})();
