import $ from 'jquery';

const seachReset = {
  create() {
    this.component = $('body');
    this.addEventListeners();
  },

  addEventListeners() {
    this.component.on('click', '.search__input-clean', this.onClickReset.bind(this));
  },

  onClickReset() {
    $('.search__input').val('');
  },
};

export default seachReset.create();
