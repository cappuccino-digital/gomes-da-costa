(() => {
  const module = {
    init() {
      this.component = document.querySelectorAll('[aria-expanded]');
      this.productLink = window.location.href;
      if (!this.component) {
        return;
      }

      [...this.component].forEach((item) => item.addEventListener('click', this.changeUrl.bind(this)));
    },

    changeUrl({ target }) {
      const { title, attributes: { url, 'aria-expanded': { value: expanded } } } = target.closest('.accordion__title');
      if (url) {
        const { value: link } = url;
        if (expanded === 'false') {
          window.history.pushState({}, title, link);
        } else {
          window.history.pushState({}, '', this.productLink);
        }
      }
    },
  };
  module.init();
})();
