import $ from 'jquery';

(() => {
  const module = {
    async init() {
      this.component = $('[data-component="like"]');
      if (!this.component) {
        return;
      }

      this.likedString = 'liked-recipes';
      this.liked = await this.isLiked(this.component.attr('data-recipe-id'));
      if (this.liked) {
        $('.like').addClass('like--liked');
      }
      this.load();
    },

    async isLiked(recipeId) {
      const recipes = JSON.parse(await window.localStorage.getItem(this.likedString));
      return recipes && recipes.indexOf(recipeId) > -1;
    },

    async requestDoLike(data) {
      return new Promise((resolve, reject) => {
        wp.apiRequest({
          method: 'POST',
          path: 'gdc/v1/receitas/curtir',
          data,
        })
          .then((response) => resolve(response))
          .fail((req) => reject(req.responseJSON));
      });
    },

    async requestRemoveLike(data) {
      return new Promise((resolve, reject) => {
        wp.apiRequest({
          method: 'POST',
          path: 'gdc/v1/receitas/descurtir',
          data,
        })
          .then((response) => resolve(response))
          .fail((req) => reject(req.responseJSON));
      });
    },

    updateCounter(value) {
      $('[data-like-counter]').html(value);
    },

    getData() {
      return {
        nonce: this.component.attr('data-nonce'),
        id: this.component.attr('data-recipe-id'),
      };
    },

    async doLike() {
      const returned = await this.requestDoLike(this.getData());
      const { data } = returned;
      this.updateCounter(data);
      const recipes = JSON.parse(await window.localStorage.getItem(this.likedString)) || [];
      window.localStorage.setItem(this.likedString, JSON.stringify([...recipes, this.component.attr('data-recipe-id')]));
      this.liked = !this.liked;
    },

    async removeLike() {
      const returned = await this.requestRemoveLike(this.getData());
      const { data } = returned;
      this.updateCounter(data);
      const recipes = JSON.parse(await window.localStorage.getItem(this.likedString)) || [];
      window.localStorage.setItem(this.likedString, JSON.stringify(recipes.filter((item) => this.component.attr('data-recipe-id') !== item)));
      this.liked = !this.liked;
    },

    async click() {
      if (this.liked) {
        this.removeLike();
      } else {
        await this.doLike();
      }
      $('.like').toggleClass('like--liked');
    },

    load() {
      this.component.click(this.click.bind(this));
    },
  };

  module.init();
})();
