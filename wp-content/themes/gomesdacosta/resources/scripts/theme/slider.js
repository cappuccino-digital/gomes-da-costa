import Swiper from 'swiper';
import 'swiper/css/swiper.min.css';

(() => {
  const module = {
    elSwiper: document.getElementsByClassName('swiper-container')[0],
    createSwiper() {
      return new Swiper(this.elSwiper, {
        slidesPerView: 2,
        width: 260,
        spaceBetween: 15,
        breakpoints: {
          360: {
            width: 300,
          },
          420: {
            width: 360,
          },
          520: {
            width: 400,
          },
          768: {
            slidesPerView: 3,
            width: 640,
          },
          992: {
            slidesPerView: 4,
            width: 860,
          },
          1200: {
            slidesPerView: 5,
            width: 1050,
          },
        },
      });
    },
    init() {
      if (this.elSwiper) {
        this.createSwiper();
      }
    },
  };

  module.init();
})();
