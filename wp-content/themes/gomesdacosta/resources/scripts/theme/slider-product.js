import Swiper from 'swiper';

({
  elProductSlide: document.querySelector('.product-slide__container'),
  createProductSlide() {
    return new Swiper(this.elProductSlide, {
      spaceBetween: 15,
      breakpoints: {
        768: {
          slidesPerView: 2,
        },
      },
      navigation: {
        nextEl: '.product-slide__next',
        prevEl: '.product-slide__prev',
      },
    });
  },
  init() {
    if (this.elProductSlide) {
      this.createProductSlide();
    }
  },
}).init();
