import $ from 'jquery';

$('[data-component="filter-toggle"]').hide();
$('[data-component="open-filter"]').click(() => {
  $('[data-component="filter-toggle"]').slideToggle();
});

(() => {
  const module = {
    init() {
      this.components = $('[data-parameter-label]');

      if (!this.components) {
        return;
      }

      this.urlParams = new URLSearchParams(window.location.search);

      this.checkCurrent();
      this.toggleLabel();
      this.components.click(this.click.bind(this));

      this.bullets = $('[data-bullet-label]');

      if (!this.bullets) {
        return;
      }

      this.bullets.click(this.remove.bind(this));
    },

    remove({ target }) {
      let label = null;
      if ($(target).attr('data-bullet-label')) {
        label = $(target).attr('data-bullet-label');
      } else {
        const parent = target.parentNode;
        label = $(parent).attr('data-bullet-label');
      }
      if (label) {
        this.urlParams.delete(label);
        window.location.href = `${window.recipeLink}?${this.urlParams.toString()}`;
      }
    },

    toggleLabel() {
      if (!document.getElementById('toggle_others_filters')) {
        return;
      }
      document.getElementById('toggle_others_filters')
        .addEventListener('click', ({ target }) => {
          if (target.innerHTML.trim() === 'Mais filtros') {
            // eslint-disable-next-line no-param-reassign
            target.innerHTML = 'Menos filtros';
          } else {
            // eslint-disable-next-line no-param-reassign
            target.innerHTML = 'Mais filtros';
          }
        });
    },

    addBullet(label, value) {
      document.getElementById('filter_bullets').appendChild(
        document.createRange().createContextualFragment(`<div class="filter__bullet" data-bullet-label="${label}"><span>${this.formatValue(value)}</span><span class="filter__bullet-close">X</span></div>`),
      );
    },

    formatValue(value) {
      if (value.indexOf('|') > -1) {
        return `${value.replace('|', ' a ')} porções`;
      }

      if (value === '60') {
        return '1 hora';
      }

      if (value === 's60') {
        return '+1 hora';
      }

      return `${value} min`;
    },

    checkCurrent() {
      Array.from(this.components).forEach((item) => {
        const label = $(item).attr('data-parameter-label');
        const value = $(item).attr('data-parameter-value');
        const param = this.getParam(label);

        let isBullet = false;
        const pathname = window.location.pathname.split('/').filter((x) => x !== '');

        if (pathname.length > 0 && pathname[1] === value) {
          isBullet = true;
        }

        if (label !== 'tipo-de-produto' && label !== 'tipo-produto' && value === param) {
          this.addBullet(label, value);
        }

        $(item).addClass(param === value || isBullet === true ? 'card-bullet--is-active' : '');
      });
    },

    getParam(label) {
      return this.urlParams.get(label);
    },

    click({ currentTarget }) {
      const label = $(currentTarget).attr('data-parameter-label');
      const value = $(currentTarget).attr('data-parameter-value');

      if (this.urlParams.has(label)) {
        this.urlParams.delete(label);
      }

      this.urlParams.append(label, value);
      if (label === 'tipo-de-produto') {
        window.location.href = `/receitas?${this.urlParams.toString()}`;
      } else if (label === 'tipo-produto') {
        window.location.href = `/download-de-imagens?${this.urlParams.toString()}`;
      } else {
        window.location.href = `/${label}/${value}`;
      }
    },
  };
  module.init();
})();
