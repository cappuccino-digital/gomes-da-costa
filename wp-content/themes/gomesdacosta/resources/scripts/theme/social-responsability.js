import Swiper from 'swiper';

({
  elResponsability: Array.from(document.getElementsByClassName('page-about-us__responsability-slide')),

  createResponsabilitySlide() {
    return this.elResponsability.map((element) => {
      const swiperElement = element.getElementsByClassName('swiper-wrapper');
      const elPagination = element.getElementsByClassName('page-about-us__responsability-pagination');

      let allowTouchMove = false;
      let pagination = {};

      if (swiperElement && swiperElement[0].getElementsByClassName('nav-item').length > 4) {
        allowTouchMove = true;
        pagination = {
          el: elPagination,
          clickable: true,
        };
      }

      return new Swiper(element, {
        slidesPerView: 3,
        allowTouchMove,
        pagination,
        breakpoints: {
          360: {
            width: 300,
          },
          576: {
            width: 440,
          },
          992: {
            slidesPerView: 4,
            width: 860,
          },
          1200: {
            slidesPerView: 6,
            width: 1060,
          },
        },
      });
    });
  },
  init() {
    if (this.elResponsability) {
      this.createResponsabilitySlide();
    }
  },
}).init();
