const icons = require.context('@images/svg-sprite', true, /\.svg$/);

icons.keys().forEach((filename) => icons(filename));
