import $ from 'jquery';

const Menu = {
  create() {
    this.component = $('.header');
    this.addEventListeners();
  },

  addEventListeners() {
    this.component.on('click', '.btn-hamburguer', this.onClickOpen.bind(this));
  },

  onClickOpen() {
    $('.navbar').toggleClass('navbar--is-active');
  },
};

export default Menu.create();
