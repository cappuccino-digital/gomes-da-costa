import $ from 'jquery';

(() => {
  const module = {
    init() {
      this.component = $('[data-component="expand-special-recipes"]');
      if (!this.component) {
        return;
      }
      this.component.click(this.click.bind(this));
    },
    click() {
      $('.special-list__item').addClass('special-list__item--show');
      this.component.attr('style', 'display:none');
    },
  };
  module.init();
})();
