import $ from 'jquery';

(() => {
  const module = {
    init: function init() {
      this.components = $('.search__result-categories__tags');
      const urlString = window.location.href;
      const url = new URL(urlString);
      this.currentType = url.searchParams.get('type');

      if (!this.components || !this.currentType) {
        return;
      }

      this.isActive();
    },

    isActive: function isActive() {
      const dataValue = $(`[data-value=${this.currentType}]`);
      if (dataValue) {
        dataValue.addClass('search__result-categories--active');
        $('#search-filter-all').removeClass('search__result-categories--active');
      }
    },
  };
  module.init();
})();
