import Swiper from 'swiper';
import 'swiper/css/swiper.min.css';

({
  homeSlideTop() {
    const elSlideTop = document.querySelector('.home-page__slide-top-container');
    const elSlideList = document.querySelectorAll('.home-page__slide-top-container .swiper-slide');
    const elSlideTopPagination = document.querySelector('.home-page__slide-top .swiper-pagination');
    const elButtonNext = document.querySelector('.home-page__slide-top .swiper-button-next');
    const elButtonPrev = document.querySelector('.home-page__slide-top .swiper-button-prev');
    const conditionToCreate = elSlideTop || (elSlideList && elSlideList.length);

    const createSingleImage = () => {
      elButtonNext.style.display = 'none';
      elButtonPrev.style.display = 'none';

      return new Swiper(elSlideTop, {
        slidesPerView: 1,
        allowSlidePrev: false,
        allowSlideNext: false,
      });
    };

    const createMultipleImages = () => {
      const slide = new Swiper(elSlideTop, {
        slidesPerView: 1,
        loop: true,
        autoplay: {
          delay: 5000,
        },
        pagination: {
          el: elSlideTopPagination,
          dynamicBullets: true,
        },
        navigation: {
          nextEl: elButtonNext,
          prevEl: elButtonPrev,
        },
      });

      return slide;
    };

    if (!conditionToCreate) {
      return false;
    }

    if (elSlideList && elSlideList.length === 1) {
      return createSingleImage();
    }

    return createMultipleImages();
  },
  init() {
    this.homeSlideTop();
  },
}).init();
