({
  callBack(mutations) {
    mutations.forEach((mutation) => {
      if (mutation.type === 'attributes') {
        const [icon] = mutation.target.getElementsByClassName('accordion__title-icon');
        const { attributes: { 'aria-expanded': { value } } } = mutation.target;
        if (icon && value === 'true') {
          icon.classList.add('accordion__title-icon--is-opened');
        } else {
          icon.classList.remove('accordion__title-icon--is-opened');
        }
      }
    });
  },
  init() {
    this.components = [...document.getElementsByClassName('accordion__title')];
    const observer = new MutationObserver(this.callBack);
    this.components.forEach((item) => {
      observer.observe(item, {
        attributes: true,
      });
    });
  },
}).init();
