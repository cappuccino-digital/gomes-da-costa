import Swiper from 'swiper';
import 'swiper/css/swiper.min.css';

({
  sliderBulletCategories: document.querySelector('.slider-bullet__categories'),
  sliderBulletTime: document.querySelector('.slider-bullet__time'),
  sliderBulletPortion: document.querySelector('.slider-bullet__portion'),

  returnIfNotExists(element) {
    if (!element) {
      return false;
    }
    return true;
  },

  getProductType() {
    const urlString = window.location.href;
    const url = new URL(urlString);
    const productType = url.searchParams.get('tipo-de-produto');

    if (productType) {
      return productType;
    }

    return false;
  },

  fixedBulletPosition() {
    if (!this.getProductType()) {
      return 0;
    }

    const bulletsArray = Array.from(document.querySelectorAll('.card-bullet-category'));

    const bulletToMove = bulletsArray.filter((b) => b.attributes['data-parameter-value'] && b.attributes['data-parameter-value'].value === this.getProductType())[0];
    const bulletToMoveIndex = bulletsArray.indexOf(bulletToMove);

    if (!bulletToMoveIndex) {
      return 0;
    }

    return bulletToMoveIndex;
  },

  createSlideCategories() {
    this.returnIfNotExists(this.sliderBulletCategories);

    const slide = new Swiper(this.sliderBulletCategories, {
      initialSlide: this.fixedBulletPosition(),
      slidesPerView: 4,
      spaceBetween: 15,
      width: 280,
      breakpoints: {
        400: {
          slidesPerView: 5,
          width: 360,
        },
        460: {
          slidesPerView: 6,
          width: 420,
        },
        520: {
          slidesPerView: 7,
          width: 480,
        },
        768: {
          slidesPerView: 7,
          width: 660,
        },
        992: {
          slidesPerView: 9,
          spaceBetween: 20,
          width: 885,
        },
        1200: {
          slidesPerView: 11,
          spaceBetween: 20,
          width: 1080,
        },
      },
    });

    if (!slide) {
      return false;
    }

    return true;
  },

  createSlideTime() {
    this.returnIfNotExists(this.sliderBulletTime);
    return new Swiper(this.sliderBulletTime, {
      slidesPerView: 4,
      spaceBetween: 15,
      width: 280,
      breakpoints: {
        400: {
          slidesPerView: 5,
          width: 360,
        },
        460: {
          slidesPerView: 6,
          width: 420,
        },
        520: {
          slidesPerView: 7,
          width: 480,
        },
        768: {
          slidesPerView: 7,
          width: 660,
        },
        992: {
          slidesPerView: 9,
          spaceBetween: 20,
          width: 885,
        },
        1200: {
          slidesPerView: 11,
          spaceBetween: 20,
          width: 1080,
        },
      },
    });
  },

  createSlidePortion() {
    this.returnIfNotExists(this.sliderBulletPortion);
    return new Swiper(this.sliderBulletPortion, {
      slidesPerView: 4,
      spaceBetween: 15,
      width: 280,
      breakpoints: {
        400: {
          slidesPerView: 5,
          width: 360,
        },
        460: {
          slidesPerView: 6,
          width: 420,
        },
        520: {
          slidesPerView: 7,
          width: 480,
        },
        768: {
          slidesPerView: 7,
          width: 660,
        },
        992: {
          slidesPerView: 9,
          spaceBetween: 20,
          width: 885,
        },
        1200: {
          slidesPerView: 11,
          spaceBetween: 20,
          width: 1080,
        },
      },
    });
  },

  init() {
    if (this.createSlideCategories()) {
      this.fixedBulletPosition();
    }
    this.createSlideTime();
    this.createSlidePortion();
  },
}).init();
