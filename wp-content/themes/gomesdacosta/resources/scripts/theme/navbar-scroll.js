({
  elNavbar: document.querySelector('header.header'),
  initialScroll: window.innerHeight / 2,
  scrollPosition: 0,
  createScrollNavigation() {
    window.addEventListener('scroll', () => {
      if (window.pageYOffset > this.initialScroll) {
        if (this.scrollPosition < window.pageYOffset) {
          this.elNavbar.style.top = `-${this.elNavbar.clientHeight + 5}px`;
          this.scrollPosition = window.pageYOffset;
        } else {
          this.elNavbar.style.top = '0px';
          this.scrollPosition = window.pageYOffset;
        }
      }
    });
  },
  init() {
    if (this.elNavbar && this.initialScroll) {
      this.createScrollNavigation();
    }
  },
}).init();
