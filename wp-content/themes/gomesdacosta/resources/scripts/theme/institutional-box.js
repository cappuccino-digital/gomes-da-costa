import $ from 'jquery';

(() => {
  const institutionalBox = {
    navitagionTabs: $('a[data-toggle="tab"]'),

    createNavigationTabs(element) {
      const toggleClass = (ev) => {
        const { target: el } = ev;

        if ($(el).hasClass('active')) {
          Array.from(el.children).forEach((e) => {
            if ($(e).hasClass('circled-icon')) {
              $(e).addClass('circled-icon--is-active');
            }

            if ($(e).hasClass('box-navigation__title')) {
              $(e.children[0]).addClass('box-navigation__title-content--is-active');
            }
          });
        } else {
          Array.from(el.children).forEach((e) => {
            if ($(e).hasClass('circled-icon')) {
              $(e).removeClass('circled-icon--is-active');
            }

            if ($(e).hasClass('box-navigation__title')) {
              $(e.children[0]).removeClass('box-navigation__title-content--is-active');
            }
          });
        }
      };

      $(element).on('shown.bs.tab', (e) => toggleClass(e));
      $(element).on('hidden.bs.tab', (e) => toggleClass(e));
    },

    init() {
      if (this.navitagionTabs) {
        this.createNavigationTabs(this.navitagionTabs);
      }
    },
  };

  institutionalBox.init();
})();
