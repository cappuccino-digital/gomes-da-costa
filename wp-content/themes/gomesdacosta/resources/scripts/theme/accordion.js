({
  init() {
    this.components = [...document.getElementsByClassName('accordion__title')];
    this.components.forEach((item) => {
      item.addEventListener('click', this.closeOtherThan.bind(this));
    });
  },
  closeOtherThan({ target }) {
    let el = {};
    if (!target.hasAttribute('data-toggle')) {
      el = target.parentNode;
    } else {
      el = target;
    }
    const elementsToClose = this.components.filter((item) => item.id !== el.id);
    elementsToClose.forEach((item) => {
      item.setAttribute('aria-expanded', 'false');
      const body = item.parentNode.querySelector('[aria-labelledby]');
      if (body) {
        body.classList.remove('show');
      }
    });
  },
}).init();
