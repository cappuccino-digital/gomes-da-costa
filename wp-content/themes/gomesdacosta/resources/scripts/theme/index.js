// eslint-disable-next-line no-unused-vars
import config from '@config';
import './vendor/*.js';
import '@styles/theme';
import '@images/favicon.ico';
import 'airbnb-browser-shims';
import './svg-sprite';
import './accordion';
import './menu';
import './slider';
import './slider-bullet';
import './slider-home';
import './slider-history';
import './filter';
import './like';
import './special-recipes';
import './institutional-box';
import './social-responsability';
import './acordeon-change-url';
import './accordion-icon';
import './navbar-scroll';
import './slider-product';
import './search-bar';
import './search-clean';
import './search-filter';
// Your code goes here ...
