import wp, { i18n, editor } from 'wp';

const { InnerBlocks } = wp.blockEditor;
const { __ } = i18n;
const { RichText } = editor;
const { withSelect } = wp.data;

export default withSelect( (select, ownProps) => {
    const { clientId } = ownProps;
    const blocks = select("core/block-editor").getBlocks();

    const getNumber = (clientId, blocks) => {
        for (const block in blocks) {
            for (const index in blocks[block].innerBlocks) {
                if (blocks[block].innerBlocks[index].clientId == clientId) {
                    return index;
                }
            }
        }
        return 0;
    }

    const getTitle = (clientId, blocks) => {
        for (const block in blocks) {
            for (const index in blocks[block].innerBlocks) {
                if (blocks[block].innerBlocks[index].clientId == clientId) {
                    return blocks[block].attributes.title;
                }
            }
        }
        return '';
    }

    return {
        number: getNumber(clientId, blocks),
        title: getTitle(clientId, blocks)
    }
})(({ number, title, setAttributes }) => {
    setAttributes( { number } );
    setAttributes( { title } );

    return (
        <div>
            <hr/>
            <div>
                <label className="title--content">{__("Conteudo do Slider", "app")}</label>
                <InnerBlocks />
            </div>
        </div>
    );
});