import wp from 'wp';

const { InnerBlocks } = wp.blockEditor; 

export default ({attributes}) => {
    const first = attributes.number == '0' ;
    const showActive = first ? 'show active' : '';
    const dataRef = String( attributes.title ).toLowerCase().replaceAll(" ", "-").replaceAll(",", "");
    const id = `nav-${attributes.number}-${dataRef}`;
    const arialLabelledby = `nav-${attributes.number}-tab`; 

    return (
        <div className={"tab-pane fade " + showActive} id={id} role="tabpanel" aria-labelledby={arialLabelledby}>
            <InnerBlocks.Content />
        </div>
    );
};