import wp, { i18n,editor } from 'wp';

const { __ } = i18n;
const { RichText } = editor;
const { withSelect } = wp.data;

export default withSelect( (select, ownProps) => {
    const { clientId } = ownProps;
    const blocks = select("core/block-editor").getBlocks();

    const getNumber = (clientId, blocks) => {
        for (const block in blocks) {
            for (const index in blocks[block].innerBlocks) {
                if (blocks[block].innerBlocks[index].clientId == clientId) {
                    return index;
                }
            }
        }
        return 0;
    }

    const getTitle = (clientId, blocks) => {
        for (const block in blocks) {
            for (const index in blocks[block].innerBlocks) {
                if (blocks[block].innerBlocks[index].clientId == clientId) {
                    return blocks[block].attributes.title;
                }
            }
        }
        return '';
    }

    return {
        number: getNumber(clientId, blocks),
        title: getTitle(clientId, blocks)
    }
})(({ number, title, setAttributes, attributes }) => {
    setAttributes( { number } );
    setAttributes( { title } );

    const handleOnChangeName = name => {
        setAttributes( { name } )
    }

    const handleOnChangeSVG = svg => {
        setAttributes( { svg } )
    }
    
    return (
        <div>
            <hr/>
            <div>
                <label className="title--content">{__("Nome do Slider", "app")}</label>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite o nome do slider", "app")}
                    value={attributes.name}
                    onChange={handleOnChangeName}
                />
            </div>
            <br/>
            <div>
                <label className="title--content">{__("Icone", "app")}</label>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite o icone do slider", "app")}
                    value={attributes.svg}
                    onChange={handleOnChangeSVG}
                />
            </div>
        </div>
    );
});