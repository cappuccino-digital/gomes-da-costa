
export default ( {teste, attributes} ) => {
    
    const useTag = `<use xlink:href="#${attributes.svg}" />`;
    const name = String( attributes.name ).toLowerCase().replaceAll(" ", "-").replaceAll(",", "");
    const id = `nav-${attributes.number}-tab`;
    const dataRef = String( attributes.title ).toLowerCase().replaceAll(" ", "-").replaceAll(",", "");
    const href = `#nav-${attributes.number}-${dataRef}`;
    const arialControls = `nav-${attributes.number}`;
    const first = attributes.number == '0';
    const slideActive = first ? 'active' : '';
    const circleActive = first ? 'circled-icon--is-active' : '';  
    
    return (
        <a aria-label={name} className={"nav-item nav-link swiper-slide box-navigation__slide-item " + slideActive} id={id} data-toggle="tab" data-ref={dataRef} href={href} role="tab" aria-controls={arialControls} aria-selected={first}>
            <div className={"circled-icon m-auto " + circleActive}> 
                <div className="circled-icon__content" >
                    <svg dangerouslySetInnerHTML={{__html: useTag}}/>
                </div>
            </div>
        </a>
    );
};