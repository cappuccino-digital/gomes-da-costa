import wp from 'wp';
import Editor from './Editor';
import Save from './Save';

const { registerBlockType } = wp.blocks;

registerBlockType('app/responsability-item', {
    attributes: {
        title: {
            type: "string"
        },
        number: {
            type: "string"
        },
        svg: {
            type: "string"
        },
        name: {
            type: "string"
        }
    },
    edit: props => <Editor {...props} />,
    save: props => <Save {...props} />
});