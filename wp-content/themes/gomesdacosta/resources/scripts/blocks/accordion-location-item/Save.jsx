import wp from 'wp'

const { InnerBlocks } = wp.blockEditor;

export default ({attributes}) => {
  return (
    <div className="accordion__wrapper">
		<h3 className="title title--black mb-2">
			{attributes.title}
		</h3>
		<InnerBlocks.Content className="accordion__wrapper-text" />
	</div>
  );
};
