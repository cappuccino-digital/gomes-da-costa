import wp, { editor, i18n } from "wp";

const { RichText } = editor;
const { __ } = i18n;
const { InnerBlocks } = wp.blockEditor;

export default ( { setAttributes, attributes } ) => {

  const handleOnChangeTitle = title => {
      setAttributes( { title } );
  }
  
  return (
    <div>
        <div>
            <label className="title--content">{__("Titulo da localização", "app")}</label>
            <br/>
            <RichText
                tagname="h2"
                className=""
                placeholder={__("Digite aqui o título", "app")}
                value={attributes.title}
                onChange={handleOnChangeTitle}
            />
        </div>
        <br/>
        <div>
            <label className="title--content">{__("Endereço", "app")}</label>
            <InnerBlocks 
                template={[["core/paragraph", {className: ""}]]}
                templateLock={false}
            />
        </div>          
    </div>
);
}
