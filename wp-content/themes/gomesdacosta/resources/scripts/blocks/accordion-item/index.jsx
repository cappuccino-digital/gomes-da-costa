import wp from 'wp';
import Editor from './Editor';
import Save from './Save';

const { registerBlockType } = wp.blocks;

registerBlockType('app/accordion-item', {
  attributes: {
    title: {
      type: "string"
    },
    url: {
      url: "string"
    },
    isChecked: {
      type: "boolean"
    }
  },
  edit: (props) => <Editor {...props} />,
  save: (props) => <Save {...props} />,
});
