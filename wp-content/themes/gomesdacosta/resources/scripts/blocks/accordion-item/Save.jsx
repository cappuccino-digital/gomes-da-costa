import wp from 'wp'

export default ({attributes}) => {
	console.log(attributes.isChecked)
	if (!attributes.isChecked) {
		return (
			<li className={`accordion__page-list-item`}>
				<a href={attributes.url} className="accordion__page-list-link" target="_blank" rel="noopener noreferrer">
					{attributes.title}
					<div className="accordion__page-list-icon">
						<svg dangerouslySetInnerHTML={{__html: `<use xlink:href="#download" />`}}/>
					</div>
				</a>
			</li>
		);
	}

	return (
		<div className="page-about-us__certifications-box-item">
			<div className="box">
				<div className="box__description">
					<div className="box__title">
						<h4 className="title title--black title--regular box__title-content">{attributes.title}</h4>
					</div>
					<div className="box__button">
						<a href={attributes.url} className="btn-default" target="_blank" rel="noopener noreferrer">visualizar</a>
					</div>
				</div>
			</div>
		</div>	
	);

};
