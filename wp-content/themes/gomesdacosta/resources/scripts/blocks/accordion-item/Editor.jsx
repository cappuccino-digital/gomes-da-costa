import wp, { editor, i18n, blockEditor, element, components } from "wp";

const { RichText } = editor;
const { __ } = i18n;
const { MediaUpload, MediaUploadCheck } = blockEditor;
const { Button,CheckboxControl } = components;
const { useState } = element;

export default ( { className, setAttributes, attributes } ) => {
    const [localMedia, setLocalMedia] = useState(attributes.url);
    const [ isChecked ] = useState(attributes.isChecked );

    const handleOnChangeTitle = title => {
        setAttributes( { title } );
    }

    const handleOnSelectFile = media => {
        setAttributes( { url: media.url } );
        setLocalMedia( media.url );
    }

    const handleOnSelectTypeButton = isChecked => {
        console.log(isChecked);
        setAttributes( { isChecked } );
    }
  
    return (
        <div className={className}>
            <div>
                <label className="title--content">{__("Selecionar arquivo", "app")}</label>
                <MediaUploadCheck>
                    <MediaUpload
                        onSelect={handleOnSelectFile}
                        render={({ open }) => (
                        <>
                            <Button
                            className="components-button components-icon-button editor-media-placeholder__button block-editor-media-placeholder__button has-text is-button is-default is-large"
                            onClick={open}
                            >
                                {__("Enviar", "app") }
                            </Button>
                        </>
                        )}
                    />
                </MediaUploadCheck>
            </div>
            
            <br/>
            <img src={localMedia} />

            <br/>
            <hr/>
            <br/>

            <div>
                <label className="title--content">{__("Titulo do item", "app")}</label>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite aqui o título", "app")}
                    value={attributes.title}
                    onChange={handleOnChangeTitle}
                />
            </div>
            <br/>

            <div>
                <label className="title--content">{__("Escolha o tipo de botão", "app")}</label>
                <CheckboxControl
                    heading=""
                    label="Visualizar/Download"
                    help="Se estiver marcado então o botão será do tipo Visualizar, caso contrário será do tipo Download."
                    checked={ attributes.isChecked }
                    onChange={ handleOnSelectTypeButton }
                />
            </div>
        </div>
    );
}
