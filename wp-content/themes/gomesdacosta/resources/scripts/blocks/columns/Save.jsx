import wp from 'wp';

const { InnerBlocks } = wp.blockEditor;

export default () => {
  return (
    <div className={"page-contact-us__contact-box"}>
      <div className={"contact-box"}>
        <InnerBlocks.Content />
      </div>
    </div>
  );
}
