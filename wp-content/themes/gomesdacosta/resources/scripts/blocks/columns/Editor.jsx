import wp from 'wp';

const { InnerBlocks, InspectorControls } = wp.blockEditor;

export default (props) => {
  const { attributes } = props;
  const changeColumnsCount = ({ target: { value } }) => {
    props.setAttributes({ columns: value });
  }

  let fields = [];

  for (let i = fields.length; i < attributes.columns; i++) {
    fields = [...fields, ['app/container', {}]];
  }

  return (
    <div>
      {
        <InspectorControls>
          <div className={"components-base-control"}>
            <label>Quantidade de colunas</label>
            <input
              type="number"
              className={"components-text-control__input"}
              value={attributes.columns}
              onChange={changeColumnsCount}
              min="1"
              max="4"
            />
          </div>
        </InspectorControls>
      }
      <div className={"app-column-editor"}>
        <InnerBlocks
          template={fields}
          templateLock="all"
        />
      </div>
    </div>
  );
};
