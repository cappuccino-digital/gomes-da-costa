import wp from 'wp';
import Editor from './Editor';
import Save from './Save';

const { registerBlockType } = wp.blocks;

registerBlockType('app/columns', {
  attributes: {
    columns: {
      type: 'integer',
      default: 3
    },
  },
  edit: (props) => <Editor {...props} />,
  save: (props) => <Save {...props} />,
});
