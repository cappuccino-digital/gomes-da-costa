import wp, { editor, i18n } from "wp";

const { InnerBlocks } = wp.blockEditor;
const { RichText } = editor;
const { __ } = i18n;

export default ( { setAttributes, attributes } ) => {

    const handleOnChangeTitle = title => {
        setAttributes( { title } );
    }

    const handleOnChangeIcon = icon => {
        setAttributes( { icon } );
    }

    return (
        <div>
            <hr/>
            <div>
                <label className="title title--accent title--head title--large">{__("Seção 'Trabalhe Conosco'", "app")}</label>
            </div>
            <br/>
            <div>
                <label className="title--content">{__("Titulo da seção", "app")}</label>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite aqui o título", "app")}
                    value={attributes.title}
                    onChange={handleOnChangeTitle}
                />
            </div>
            <br/>
            <div>
                <label className="title--content">{__("Icone", "app")}</label>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite o nome do icone", "app")}
                    value={attributes.icon}
                    onChange={handleOnChangeIcon}
                />
            </div>
            <br/>
            <hr/>
            <br/>
            <div>
                <label className="title--content">{__("Links para Contatos", "app")}</label>
                <br/>
                <InnerBlocks allowedBlocks={['app/work-link']} />
            </div>
        </div>
    );
}