import wp, { editor, i18n }from 'wp';

const { InnerBlocks } = wp.blockEditor;

export default ({className, attributes}) => {
    const useTag = `<use xlink:href="#${attributes.icon}" />`;

    return ( 
        <div className={`contact-box__item ${className}`}>
            <div className="contact-box__item-header">
                <div className="contact-box__item-icon">
                    <svg dangerouslySetInnerHTML={{__html: useTag}}/>
                </div>
                <div className="contact-box__item-title">
                    <h4 className="contact-box__item-title-content">
                        {attributes.title}
                    </h4>
                </div>
            </div>
            <div className="contact-box__item-body">
                <div class="contact-box__item-description">
                    <div class="contact-box__item-description-wrapper">
                        <InnerBlocks.Content />
                    </div>
                </div>
            </div>
        </div>
    );
};



