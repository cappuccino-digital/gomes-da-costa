import wp, { editor } from "wp";

const { RichText } = editor;
const { InnerBlocks } = wp.blockEditor;

export default ( { attributes, className} ) => {
    return (
        <div className="swiper-slide">
            <div className="page-about-us__story-slide-box">
                <div className="page-about-us__story-slide-img">
                    <img src={attributes.url} className="home-page__slide-top-img" alt="" />
                </div>
                <div className="page-about-us__story-description" data-element="slide-desc">
                    <div className="page-about-us__story-description-title">
                        <h2 className="title title--accent title--head title--large">{attributes.title}</h2>
                    </div>
                    <div className="page-about-us__story-description-wrapper" data-element="slide-desc-content">
                        <InnerBlocks.Content className="page-about-us__story-description-text" />
                    </div>
                </div>
            </div>
        </div>
    );
};