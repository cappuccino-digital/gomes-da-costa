import wp, { editor, i18n, blockEditor, element, components } from "wp";

const { InnerBlocks } = wp.blockEditor;
const { RichText } = editor;
const { __ } = i18n;
const { MediaUpload, MediaUploadCheck } = blockEditor;
const { Button } = components;
const { useState } = element;

export default ( { className, setAttributes, attributes } ) => {
    const [localMedia, setLocalMedia] = useState(attributes.url);

    const handleOnChangeTitle = title => {
        setAttributes( { title } );
    }

    const handleOnSelectImage = media => {
        setAttributes( { url: media.url } );
        setLocalMedia( media.url );
    }
    
    return (
        <div className={className}>
            <hr/>
            <div>
                <label className="title--content">{__("Selecionar Imagem", "app")}</label>
                <br/>
                <MediaUploadCheck>
                    <MediaUpload
                        onSelect={handleOnSelectImage}
                        allowedTypes={["image"]}
                        render={({ open }) => (
                        <>
                            <Button
                            className="components-button components-icon-button editor-media-placeholder__button block-editor-media-placeholder__button has-text is-button is-default is-large"
                            onClick={open}
                            >
                                {__("Enviar", "app") }
                            </Button>
                        </>
                        )}
                    />
                </MediaUploadCheck>
            </div>
            <br/>
            <img src={localMedia} />
            
            <br/>
            <div>
                <label className="title--content">{__("Ano da História", "app")}</label>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite aqui o ano", "app")}
                    value={attributes.title}
                    onChange={handleOnChangeTitle}
                />
            </div>
            <br/>
            <div>
                <label className="title--content">{__("Descrição", "app")}</label>
                <InnerBlocks 
                    template={[["core/paragraph", {className: ""}]]}
                    templateLock={false}
                />
            </div>
        </div>
    );
}