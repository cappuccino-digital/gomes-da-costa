import wp, { editor, i18n }from 'wp';

const { InnerBlocks } = wp.blockEditor;

export default ({attributes}) => {
    const useTag = `<use xlink:href="#${attributes.icon}" />`;

    return (
        <div className="contact-box__item">
            <div className="contact-box__item-header">
                <div className="contact-box__item-icon">
                    <svg dangerouslySetInnerHTML={{__html: useTag}}/>
                </div> 
                <div className="contact-box__item-title">
                    <h4 className="contact-box__item-title-content">
                        {attributes.title}
                    </h4>
                </div>
            </div>
            <div className="contact-box__item-body">
                <InnerBlocks.Content />
            </div>
        </div>
    );
};



