
export default ({attributes}) => {
    return (
        <p className="contact-box__item-link">
            <a href={attributes.link} className="link link--medium" target="_blank" rel="noopener noreferrer">{attributes.text}</a>
        </p>
    );
};



