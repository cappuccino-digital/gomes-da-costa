import { editor, i18n } from "wp";

const { RichText } = editor;
const { __ } = i18n;

export default ( { setAttributes, attributes } ) => {

    const handleOnChangeLink = link => {
        setAttributes( { link } );
    }

    const handleOnChangeText = text => {
        setAttributes( { text } );
    }

    return (
        <div>
            <div>
                <label className="title--content">{__("Texto do link", "app")}</label>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite aqui o texto", "app")}
                    value={attributes.text}
                    onChange={handleOnChangeText}
                />
            </div>
            <br/>
            <div>
                <label className="title--content">{__("Link", "app")}</label>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite aqui o link", "app")}
                    value={attributes.link}
                    onChange={handleOnChangeLink}
                />
            </div>
        </div>
    );
}