import wp from 'wp';

const { InnerBlocks } = wp.blockEditor;

export default ({attributes}) => {
    const title = String( attributes.title ).toLowerCase().replaceAll(" ", "-").replaceAll(",", "");

    return (
        <div className="box-navigation__content tab-content" id={`nav-${title}-content`}>
            <InnerBlocks.Content />
        </div>
    );
};