import wp, { i18n,editor } from 'wp';

const { InnerBlocks } = wp.blockEditor;
const { __ } = i18n;
const { RichText } = editor;

export default ({setAttributes, attributes}) => {
    
    const handleOnChangeTitle = title => {
        setAttributes( { title } );
    }

    return (
        <div>
            <div>
                <label className="title title--accent title--head title--large">{__("Bloco de descrição dos sliders", "app")}</label>
            </div>
            <br/>
            <hr/>
            <div>
                <label className="tile--content">{__("Titulo do Slider", "app")}</label>
                <br/>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Título do slider de Responsabilidade", "app")}
                    value={attributes.title}
                    onChange={handleOnChangeTitle}
                />
            </div>
            <br/>
            <hr/>
            <br/>
            <div>
                <label className="title title--accent title--head title--large">{__("Descrição dos Sliders", "app")}</label>
                <br/>
                <InnerBlocks 
                    allowedBlocks={['app/description-item']}
                />
            </div>
        </div>
    );
};