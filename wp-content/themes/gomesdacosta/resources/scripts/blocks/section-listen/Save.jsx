import wp, { editor, i18n }from 'wp';

const { InnerBlocks } = wp.blockEditor;

export default ({attributes}) => {

    return (
        <div className="page-contact-us__listen">
			<div className="page-contact-us__title">
				<h1 className="title title--accent title--head">
					{attributes.title}
				</h1>
			</div>
			<div className="page-contact-us__contact-box">
				<div className="contact-box">
					<div className="contact-box__title">
						<h3 className="title title--black">
							Contate-nos
						</h3>
					</div>
					<InnerBlocks.Content />
				</div>
			</div>
		</div>
    );
};



