import wp, { editor, i18n } from "wp";

const { InnerBlocks } = wp.blockEditor;
const { RichText } = editor;
const { __ } = i18n;

export default ( {  setAttributes, attributes } ) => {

    const handleOnChangeTitle = title => {
        setAttributes( { title } );
    }

    return (
        <div>
            <div>
                <label className="title--content">{__("Título principal", "app")}</label>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite aqui o título", "app")}
                    value={attributes.title}
                    onChange={handleOnChangeTitle}
                />
            </div>
            <br/>
            <hr/>
            <br/>
            <div>
                <InnerBlocks template={[
                    ['app/contact-attendance', {} ],
                    ['app/contact-advisory', {} ],
                    ['app/contact-work', {} ],
                ]} />
            </div>
        </div>
    );
}