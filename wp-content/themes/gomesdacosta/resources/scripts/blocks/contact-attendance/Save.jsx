import wp, { editor, i18n }from 'wp';

const { InnerBlocks } = wp.blockEditor;

export default ({attributes}) => {
    const useTag = `<use xlink:href="#${attributes.icon}" />`;

    return (
        <div className="contact-box__item">
            <div className="contact-box__item-header">
                <div className="contact-box__item-icon">
                    <svg dangerouslySetInnerHTML={{__html: useTag}}/>
                </div> 
                <div className="contact-box__item-title">
                    <h4 className="contact-box__item-title-content">
                        {attributes.title}
                    </h4>
                </div>
            </div>
            <div className="contact-box__item-body">
                <div className="contact-box__item-highlighted">
                    <a className="contact-box__item-highlighted-content" href={`tel:${attributes.phone}`}>{attributes.phone}</a>
                </div>
                <div className="contact-box__item-highlighted">
                    <a className="contact-box__item-highlighted-content" href={`mailto:${attributes.email}`}>{attributes.email}</a>
                </div>

                <div className="contact-box__item-description">
                    <div className="contact-box__item-description--text-blue contact-box__item-description--no-margin"><InnerBlocks.Content /></div>
                </div>
            </div>
        </div>
    );
};



