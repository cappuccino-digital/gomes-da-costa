import wp from 'wp';
import Editor from './Editor';
import Save from './Save';

const { registerBlockType } = wp.blocks;

registerBlockType( 'app/contact-attendance', {
    attributes: {
        title: {
          type: "string"
        },
        icon: {
            type: "string"
        },
        phone: {
            type: "string"
        },
        email: {
            type: "string"
        }
    },
    edit: props => <Editor {...props} />,
    save: props => <Save {...props} />
})