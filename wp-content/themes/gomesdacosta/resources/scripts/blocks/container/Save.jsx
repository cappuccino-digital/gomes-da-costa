import wp from 'wp';

const { InnerBlocks } = wp.blockEditor;

export default ({ className }) => {
  return (
    <div className={className}>
      <InnerBlocks.Content />
    </div>
  );
}
