import wp from 'wp';

const { InnerBlocks } = wp.blockEditor;

export default ({ className }) => {
  return (
    <div className={className}>
      <InnerBlocks
        template={ [
          [ 'core/paragraph', { className:"" } ]
        ] }
        templateLock={false}
      />
    </div>
  );
}
