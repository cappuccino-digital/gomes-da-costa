import wp from 'wp';
import Editor from './Editor';
import Save from './Save';

const { registerBlockType } = wp.blocks;

registerBlockType('app/container', {
  edit: (props) => <Editor />,
  save: (props) => <Save />,
});
