import wp from 'wp';

const { InnerBlocks } = wp.blockEditor;

export default ({ attributes }) => {
  const heading = `heading-${attributes.title}`;
  const collapse = `collapse-${attributes.title}`;
  const accordion = `accordion-${attributes.title}`;

  return (
    <div className={`accordion__page`}>
      <div className={`accordion__container`}>
        <div id={accordion} className="accordion accordion--shadow">
          <div className="accordion__title" id={heading} data-toggle="collapse" data-target={`#${collapse}`} aria-expanded="true" aria-controls={collapse}>
            <h2 className="title title--sun title--head title--large">
              {attributes.title}
            </h2>
            <div className="accordion__title-icon">
            </div>
          </div>
          <div id={collapse} className="collapse" aria-labelledby={heading} data-parent={`#${accordion}`}>
            <div className="accordion__content pb-1 px-4">
              <InnerBlocks.Content />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
