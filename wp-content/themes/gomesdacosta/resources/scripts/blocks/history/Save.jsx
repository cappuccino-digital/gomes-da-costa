import wp from "wp";

const { InnerBlocks } = wp.blockEditor;

export default ({attributes}) => {
    return (
        <div class="page-about-us__story">
            <div class="page-about-us__story-title">
                <h1 class="title title--accent title--head">
                    {attributes.title}
                </h1>
            </div>
            <div className="page-about-us__story-slide" data-component="slide">
                <div className="page-about-us__story-slide-wrapper">
                    <div className="swiper-wrapper">
                        <InnerBlocks.Content />
                    </div>
                    <div className="swiper-button-next page-about-us__story-slide-next"></div>
                    <div className="swiper-button-prev page-about-us__story-slide-prev"></div>
                </div>
            </div>
        </div>
    );
};