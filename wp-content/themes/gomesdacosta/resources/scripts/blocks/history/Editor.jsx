import wp, { i18n,editor } from 'wp';

const { InnerBlocks } = wp.blockEditor;
const { __ } = i18n;
const { RichText } = editor;

export default ({setAttributes, attributes}) => {
    const handleOnChangeTitle = title => {
        console.log(setAttributes)
        setAttributes( { title } );
    }
    
    return (
        <div>
            <div>
                <label className="title--content">{__("Título da seção", "app")}</label>
                <br/>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite aqui o título", "app")}
                    value={attributes.title}
                    onChange={handleOnChangeTitle}
                />
            </div>
            <br/>
            <hr/>
            <br/>
            <div>
                <label className="title title--accent title--head title--large">{__("Sliders sobre a História", "app")}</label>
                <br/>
                <InnerBlocks
                    allowedBlocks={['app/history-item']} 
                />
            </div>
        </div>
    )
}