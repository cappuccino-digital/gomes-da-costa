import wp from "wp";
import Editor from './Editor';
import Save from './Save';

const { registerBlockType } = wp.blocks

registerBlockType( "app/history", {
    attributes: {
        title: {
            type: "string"
        }
    },
    edit: props => <Editor {...props} />,
    save: props => <Save {...props} />
})