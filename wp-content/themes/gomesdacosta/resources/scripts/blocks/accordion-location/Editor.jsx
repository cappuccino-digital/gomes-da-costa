import wp, { i18n,editor } from 'wp';

const { InnerBlocks } = wp.blockEditor;
const { __ } = i18n;
const { RichText } = editor;

export default ({setAttributes, attributes}) => {

  const handleOnChangeTitle = title => {
    setAttributes({title})
  }

  return (
    <div>
        <div className="page-about-us__responsability-title">
            <label className="title title--sun title--head title--large">{__("Titulo do Acordeon", "app")}</label>
            <br/>
            <RichText
                tagname="h2"
                classname=""
                placehold={__('Digite aqui o título', 'app')}
                value={attributes.title}
                onChange={handleOnChangeTitle}
            />
        </div>

        <br/>
        <hr/>
        <br/>

        <div className="page-about-us__responsability-title">
            <label className="title title--sun title--head title--large">{__("Itens do Acordeon", "app")}</label>
            <br/>
            <InnerBlocks
                allowedBlocks={[
                'app/accordion-location-item'
                ]}
            />
        </div>
    </div>
  );
}
