import wp from 'wp';
import Editor from './Editor';
import Save from './Save';

const { registerBlockType } = wp.blocks;

registerBlockType( 'app/advisory-item', {
    attributes: {
        name: {
          type: "string"
        },
        phone: {
            type: "string"
        },
        email: {
            type: "string"
        }
    },
    edit: props => <Editor {...props} />,
    save: props => <Save {...props} />
})