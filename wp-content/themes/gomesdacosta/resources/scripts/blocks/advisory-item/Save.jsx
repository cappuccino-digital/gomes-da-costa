
export default ({attributes}) => {
    return (
        <div class="contact-box__item-description">
            <div class="contact-box__item-description-wrapper">
                <p class="contact-box__item-description--text-blue">{attributes.name}</p>
                <p class="contact-box__item-description--text-blue">
                    <a href={`tel:${attributes.phone}`} class="contact-box__item-description--text-blue">
                        {attributes.phone}
                    </a>
                </p>
                <p class="contact-box__item-description--text-blue">
                    <a href={`mailto:${attributes.email}`} class="contact-box__item-description--text-blue">
                        {attributes.email}
                    </a>
                </p>
            </div>
        </div>
    );
};



