import { editor, i18n } from "wp";

const { RichText } = editor;
const { __ } = i18n;

export default ( { setAttributes, attributes } ) => {

    const handleOnChangeName = name => {
        setAttributes( { name } );
    }

    const handleOnChangePhone = phone => {
        setAttributes( { phone } );
    }

    const handleOnChangeEmail = email => {
        setAttributes( { email } );
    }

    return (
        <div>
            <div>
                <label className="title--content">{__("Nome", "app")}</label>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite o nome", "app")}
                    value={attributes.name}
                    onChange={handleOnChangeName}
                />
            </div>
            <br/>
            <div>
                <label className="title--content">{__("Telefone", "app")}</label>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite o telefone", "app")}
                    value={attributes.phone}
                    onChange={handleOnChangePhone}
                />
            </div>
            <br/>
            <div>
                <label className="title--content">{__("Email", "app")}</label>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite o email", "app")}
                    value={attributes.email}
                    onChange={handleOnChangeEmail}
                />
            </div>
        </div>
    );
}