import wp, { i18n,editor } from 'wp';

const { InnerBlocks } = wp.blockEditor;
const { __ } = i18n;
const { RichText } = editor;

export default ({setAttributes, attributes}) => {
    const handleOnChangeTitle = title => {
        setAttributes( { title } );
    }
    
    return (
        <div>
            <div>
                <label className="title title--accent title--head title--large">{__("Título da seção", "app")}</label>
                <br/>
                <RichText
                    tagname="h2"
                    className=""
                    placeholder={__("Digite aqui o título", "app")}
                    value={attributes.title}
                    onChange={handleOnChangeTitle}
                />
            </div>
            
            <br/>
            <hr/>
            <br/>

            <div>
                <label className="title title--accent title--head title--large">{__("Sliders", "app")}</label>
            </div>
            <br/>
            <InnerBlocks
                allowedBlocks={['app/responsability-item']}
                teste="teste"
            />

            <h5 className="title--content">Use o bloco "Descrição" para colocar a descrição de cada item do slider</h5>
        </div>
    );
};