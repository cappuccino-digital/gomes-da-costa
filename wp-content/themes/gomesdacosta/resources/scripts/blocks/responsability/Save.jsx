import wp, { editor, i18n }from 'wp';

const { InnerBlocks } = wp.blockEditor;

export default ({attributes}) => {
    console.log(attributes)
    const title = attributes.title;
    const titleClass = String( title ).toLowerCase().replaceAll(" ", "-").replaceAll(",", "");

    return (
        <div className="page-about-us__responsability">
            <div className="page-about-us__responsability-title">
                <h2 className="title title--accent title--head title--large">
                    {title}
                </h2>
            </div>

            <div className="page-about-us__responsability-box">
                <div className="box-navigation box-navigation--full">
                    <div className={`box-navigation__slide page-about-us__responsability-slide`}>
                        <div className="swiper-wrapper box-navigation__slide-wrapper nav" id={"nav-tab-" + titleClass} role="tablist">
                            <InnerBlocks.Content />
                        </div>
                        <div className={`page-about-us__responsability-pagination swiper-pagination`}></div>
                    </div>
                </div>
            </div>
        </div>
    );
};
