<?php declare( strict_types = 1 );

/**
 * Declare all your actions and filters here.
 *
 * @package WPEmergeTheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

use App\Services\SCService;
use WPEmerge\Facades\Application;

$sc_service = Application::resolve( SCService::class );
/**
 * ------------------------------------------------------------------------
 * WordPress
 * ------------------------------------------------------------------------
 */

/**
 * Assets
 */
add_action( 'wp_enqueue_scripts', 'App\enqueue_theme_assets' );
add_action( 'admin_enqueue_scripts', 'App\enqueue_admin_assets' );
add_action( 'login_enqueue_scripts', 'App\enqueue_login_assets' );
add_action( 'enqueue_block_editor_assets', 'App\enqueue_editor_assets' );

add_action( 'wp_head', 'App\add_favicon', 5 );
add_action( 'login_head', 'App\add_favicon', 5 );
add_action( 'admin_head', 'App\add_favicon', 5 );

add_filter( 'upload_dir', 'App\fix_upload_dir_url_schema' );

/**
 * Content
 */
add_filter( 'excerpt_more', 'App\get_excerpt_more' );
add_filter( 'excerpt_length', 'App\get_excerpt_length', 999 );
add_filter( 'the_content', 'App\fix_shortcode_empty_paragraphs' );

// Attach all suitable hooks from `the_content` on `get_content`.
add_filter( 'get_content', 'do_shortcode', 9 );
add_filter( 'get_content', 'App\fix_shortcode_empty_paragraphs', 10 );
add_filter( 'get_content', 'wptexturize', 10 );
add_filter( 'get_content', 'wpautop', 10 );
add_filter( 'get_content', 'shortcode_unautop', 10 );
add_filter( 'get_content', 'prepend_attachment', 10 );
add_filter( 'get_content', 'wp_make_content_images_responsive', 10 );
add_filter( 'get_content', 'convert_smilies', 20 );

/**
 * Login
 */
add_filter( 'login_headerurl', 'App\get_login_headerurl' );

if ( version_compare( get_bloginfo( 'version' ), '5.2', '<' ) ) {
	add_filter( 'login_headertitle', 'App\get_login_headertext' );
}

add_filter( 'login_headertext', 'App\get_login_headertext' );

/**
 * Enqueue sprite svg.
 */
add_action( 'wp_body_open', 'App\enqueue_sprite_svg' );

add_action( 'pre_get_posts', 'App\apply_product_type_filter' );
add_action( 'pre_get_posts', 'App\apply_time_filter' );
add_action( 'pre_get_posts', 'App\apply_portion_filter' );
add_action( 'pre_get_posts', 'App\apply_general_search_filters' );

/**
 * Customize.
 */
add_action( 'customize_register', 'App\customize_register' );

/**
 * Gravity forms
 */
add_action(
	'gform_after_submission',
	[$sc_service, 'send_email_by_form'],
	10,
	2
);
/**
 * ------------------------------------------------------------------------
 * External Libraries and Plugins.
 * ------------------------------------------------------------------------
 */

/**
 * Carbon Fields
 */
add_action( 'after_setup_theme', 'App\bootstrap_carbon_fields', 100 );
add_action( 'carbon_fields_register_fields', 'App\bootstrap_carbon_fields_register_fields' );
add_filter( 'carbon_fields_map_field_api_key', 'App\get_google_maps_api_key' );

add_filter( 'comment_form_fields', 'App\order_comment' );

add_filter( 'block_categories', 'App\block_categories' );
add_action( 'init', 'App\register_blocks' );

add_filter( 'wpusb_svg_symbols', 'App\change_svg_load' );
