<?php declare( strict_types = 1 );

/**
 * Register custom taxonomies.
 *
 * @link https://developer.wordpress.org/reference/functions/register_taxonomy/
 * @hook init
 * @package WPEmergeTheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

register_taxonomy(
	'fw-produto-category',
	['fw-produtos', 'fw-receitas'],
	[
		'labels' => [
			'name' => __( 'Tipos de Produto', 'app' ),
			'singular_name' => __( 'Tipo do Produto', 'app' ),
			'search_items' => __( 'Buscar Tipos de Produto', 'app' ),
			'all_items' => __( 'Todos os Tipos de Produto', 'app' ),
			'parent_item' => __( 'Tipo do Produto pai', 'app' ),
			'parent_item_colon' => __( 'Tipo do Produto pai:', 'app' ),
			'view_item' => __( 'Ver Tipo do Produto', 'app' ),
			'edit_item' => __( 'Editar Tipo do Produto', 'app' ),
			'update_item' => __( 'Atualizar Tipo do Produto', 'app' ),
			'add_new_item' => __( 'Adicionar novo Tipo do Produto', 'app' ),
			'new_item_name' => __( 'Nome novo do Tipo do Produto', 'app' ),
			'menu_name' => __( 'Tipos de Produto', 'app' ),
		],
		'hierarchical' => true,
		'public' => true,
		'show_admin_column' => true,
		'show_ui' => true,
		'rewrite' => [
			'slug' => 'produto-categoria',
		],
	]
);

register_taxonomy(
	'fw-produto-saude',
	['fw-produtos'],
	[
		'labels' => [
			'name' => __( 'Mais saúde para você', 'app' ),
			'singular_name' => __( 'Mais saúde para você', 'app' ),
			'search_items' => __( 'Buscar Mais saúde para você', 'app' ),
			'all_items' => __( 'Todos os Mais saúde para você', 'app' ),
			'parent_item' => __( 'Mais saúde para você pai', 'app' ),
			'parent_item_colon' => __( 'Mais saúde para você pai:', 'app' ),
			'view_item' => __( 'Ver Mais saúde para você', 'app' ),
			'edit_item' => __( 'Editar Mais saúde para você', 'app' ),
			'update_item' => __( 'Atualizar Mais saúde para você', 'app' ),
			'add_new_item' => __( 'Adicionar novo Mais saúde para você', 'app' ),
			'new_item_name' => __( 'Nome novo do Mais saúde para você', 'app' ),
			'menu_name' => __( 'Mais saúde para você', 'app' ),
		],
		'hierarchical' => true,
		'public' => false,
		'show_admin_column' => true,
		'show_ui' => true,
	]
);

register_taxonomy(
	'fw-produto-alergico',
	['fw-produtos'],
	[
		'labels' => [
			'name' => __( 'Alérgicos', 'app' ),
			'singular_name' => __( 'Alérgico', 'app' ),
			'search_items' => __( 'Buscar Alérgicos', 'app' ),
			'all_items' => __( 'Todos os Alérgicos', 'app' ),
			'parent_item' => __( 'Alérgico pai', 'app' ),
			'parent_item_colon' => __( 'Alérgico pai:', 'app' ),
			'view_item' => __( 'Ver Alérgico', 'app' ),
			'edit_item' => __( 'Editar Alérgico', 'app' ),
			'update_item' => __( 'Atualizar Alérgico', 'app' ),
			'add_new_item' => __( 'Adicionar novo Alérgico', 'app' ),
			'new_item_name' => __( 'Nome novo do Alérgico', 'app' ),
			'menu_name' => __( 'Alérgicos', 'app' ),
		],
		'hierarchical' => true,
		'public' => false,
		'show_admin_column' => true,
		'show_ui' => true,
	]
);

register_taxonomy(
	'fw-receita-ocasiao',
	['fw-receitas'],
	[
		'labels' => [
			'name' => __( 'Ocasiões', 'app' ),
			'singular_name' => __( 'Ocasião', 'app' ),
			'search_items' => __( 'Buscar Ocasiões', 'app' ),
			'all_items' => __( 'Todos as Ocasiões', 'app' ),
			'parent_item' => __( 'Ocasião pai', 'app' ),
			'parent_item_colon' => __( 'Ocasião pai:', 'app' ),
			'view_item' => __( 'Ver Ocasião', 'app' ),
			'edit_item' => __( 'Editar Ocasião', 'app' ),
			'update_item' => __( 'Atualizar Ocasião', 'app' ),
			'add_new_item' => __( 'Adicionar nova Ocasião', 'app' ),
			'new_item_name' => __( 'Nome novo da Ocasião', 'app' ),
			'menu_name' => __( 'Ocasiões', 'app' ),
		],
		'hierarchical' => true,
		'public' => true,
		'show_admin_column' => true,
		'rewrite' => [
			'slug' => 'ocasiao',
		],
		'show_ui' => true,
	]
);

register_taxonomy(
	'fw-receita-colecao',
	['fw-receitas'],
	[
		'labels' => [
			'name' => __( 'Coleções', 'app' ),
			'singular_name' => __( 'Coleção', 'app' ),
			'search_items' => __( 'Buscar Coleções', 'app' ),
			'all_items' => __( 'Todos as Coleções', 'app' ),
			'parent_item' => __( 'Coleção pai', 'app' ),
			'parent_item_colon' => __( 'Coleção pai:', 'app' ),
			'view_item' => __( 'Ver Coleção', 'app' ),
			'edit_item' => __( 'Editar Coleção', 'app' ),
			'update_item' => __( 'Atualizar Coleção', 'app' ),
			'add_new_item' => __( 'Adicionar nova Coleção', 'app' ),
			'new_item_name' => __( 'Nome novo da Coleção', 'app' ),
			'menu_name' => __( 'Coleções', 'app' ),
		],
		'hierarchical' => true,
		'public' => false,
		'show_admin_column' => true,
		'show_ui' => true,
	]
);
