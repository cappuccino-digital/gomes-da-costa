<?php declare( strict_types = 1 );

/**
 * Register image sizes.
 *
 * @link https://developer.wordpress.org/reference/functions/add_image_size/
 * @hook after_setup_theme
 * @package WPEmergeTheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_image_size( 'small-card', 150, 200, true );
add_image_size( 'icon-card', 42, 42, true );
add_image_size( 'medium-card', 350, 290, true );
add_image_size( 'mobile', 576, 288, true );
add_image_size( 'desktop', 1.200, 600, true );
add_image_size( 'recipe-small', 42, 42, true );
add_image_size( 'social-small', 324, 164, true );
add_image_size( 'recipe-single', 325, 170, true );
