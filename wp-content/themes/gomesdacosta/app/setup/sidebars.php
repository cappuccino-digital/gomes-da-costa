<?php declare( strict_types = 1 );

/**
 * Register sidebars.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 * @hook widgets_init
 * @package WPEmergeTheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Array of default options.
 *
 * @var array
 */
$default_options = [
	'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h2 class="widget__title">',
	'after_title' => '</h2>',
];

/**
 * Default sidebar.
 */
register_sidebar(
	array_merge(
		$default_options,
		[
			'name' => __( 'Default Sidebar', 'app' ),
			'id' => 'default-sidebar',
		]
	)
);

/**
 * Footer 1
 */
register_sidebar(
	array_merge(
		$default_options,
		[
			'name' => __( 'Footer 1', 'app' ),
			'id' => 'footer-1',
		]
	)
);

/**
 * Footer 2
 */
register_sidebar(
	array_merge(
		$default_options,
		[
			'name' => __( 'Footer 2', 'app' ),
			'id' => 'footer-2',
		]
	)
);

/**
 * Footer 3
 */
register_sidebar(
	array_merge(
		$default_options,
		[
			'name' => __( 'Footer 3', 'app' ),
			'id' => 'footer-3',
		]
	)
);
