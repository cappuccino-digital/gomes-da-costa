<?php declare( strict_types = 1 );

/**
 * Theme Options.
 *
 * Here, you can register Theme Options using the Carbon Fields library.
 *
 * @link https://carbonfields.net/docs/containers-theme-options/
 * @package WPEmergeCli
 */

use Carbon_Fields\Container\Container;
use Carbon_Fields\Field\Field;

Container::make( 'theme_options', __( 'Configurações', 'app' ) )
	->set_page_parent( 'edit.php?post_type=fw-produtos' )
	->add_fields(
		[
			Field::make( 'text', 'related_product_count', __( 'Quantidade de receitas recomendadas', 'app' ) )
				->set_default_value( 10 ),
		]
	);
