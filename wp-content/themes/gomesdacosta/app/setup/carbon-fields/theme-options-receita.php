<?php declare( strict_types = 1 );

/**
 * Theme Options.
 *
 * Here, you can register Theme Options using the Carbon Fields library.
 *
 * @link https://carbonfields.net/docs/containers-theme-options/
 * @package WPEmergeCli
 */

use Carbon_Fields\Container\Container;
use Carbon_Fields\Field\Field;

Container::make( 'theme_options', __( 'Configurações', 'app' ) )
	->set_page_parent( 'edit.php?post_type=fw-receitas' )
	->add_tab(
		__( 'Single', 'app' ),
		[
			Field::make( 'text', 'related_recipes_count', __( 'Quantidade de receitas recomendadas', 'app' ) )
				->set_default_value( 10 ),
		]
	)
	->add_tab(
		__( 'Home', 'app' ),
		[
			Field::make( 'image', 'popular_recipes_all_image', __( 'Imagem para categoria todos', 'app' ) ),
			Field::make( 'association', 'recipe_product_category', __( 'Categorias para filtrar receitas', 'app' ) )
				->set_types(
					[
						[
							'type' => 'term',
							'taxonomy' => 'fw-produto-category',
						],
					]
				),
			Field::make( 'text', 'popular_recipes_count', __( 'Quantidade de receitas populares', 'app' ) )
				->set_default_value( 10 ),
			Field::make( 'text', 'recipes_count', __( 'Quantidade de receitas na home de receitas', 'app' ) )
				->set_default_value( 10 ),
			Field::make( 'text', 'special_recipe_title', __( 'Título da página de ocasiões especiais', 'app' ) )
				->set_default_value( 'Olá, o que você vai cozinhar hoje?' ),
			Field::make( 'association', 'special_recipe', __( 'Ocasiões especiais', 'app' ) )
				->set_types(
					[
						[
							'type' => 'term',
							'taxonomy' => 'fw-receita-ocasiao',
						],
					]
				),
		]
	);
