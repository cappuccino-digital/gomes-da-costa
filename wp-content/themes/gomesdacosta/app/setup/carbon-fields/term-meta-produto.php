<?php declare( strict_types = 1 );

/**
 * Post Meta.
 *
 * Here, you can register custom post meta fields using the Carbon Fields library.
 *
 * @link https://carbonfields.net/docs/containers-post-meta/
 * @package WPEmergeCli
 */

use Carbon_Fields\Container\Container;
use Carbon_Fields\Field\Field;

Container::make( 'term_meta', __( 'Extra', 'app' ) )
	->where( 'term_taxonomy', 'IN', ['fw-produto-category', 'fw-produto-saude', 'fw-produto-alergico', 'fw-receita-colecao', 'fw-receita-ocasiao'] )
	->add_fields(
		[
			Field::make( 'image', 'thumbnail', __( 'Imagem em miniatura', 'app' ) ),
			Field::make( 'image', 'large', __( 'Imagem grande', 'app' ) ),
			Field::make( 'color', 'color', __( 'Cor de destaque', 'app' ) )
				->set_default_value( '#fdb24a' ),
			Field::make( 'checkbox', 'hidde', __( 'Esconder da home', 'app' ) )
				->set_default_value( 'no' ),
		]
	);
