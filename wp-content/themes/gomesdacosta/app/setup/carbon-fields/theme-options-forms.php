<?php declare( strict_types = 1 );

/**
 * Theme Options.
 *
 * Here, you can register Theme Options using the Carbon Fields library.
 *
 * @link https://carbonfields.net/docs/containers-theme-options/
 * @package WPEmergeCli
 */

use Carbon_Fields\Container\Container;
use Carbon_Fields\Field\Field;

Container::make( 'theme_options', __( 'Formulários', 'app' ) )
	->set_page_parent( 'themes.php' )
	->add_fields(
		[
			Field::make( 'gravity_form', 'newsletter_form', __( 'Formulário de newsletter', 'app' ) ),
			Field::make( 'gravity_form', 'contact_form', __( 'Formulário de contato', 'app' ) ),
		]
	);
