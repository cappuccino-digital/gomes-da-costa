<?php declare( strict_types = 1 );

/**
 * Post Meta.
 *
 * Here, you can register custom post meta fields using the Carbon Fields library.
 *
 * @link https://carbonfields.net/docs/containers-post-meta/
 * @package WPEmergeCli
 */

use Carbon_Fields\Container\Container;
use Carbon_Fields\Field\Field;

Container::make( 'post_meta', __( 'Detalhes', 'app' ) )
	->where( 'post_type', '=', 'fw-produtos' )
	->add_fields(
		[
			Field::make( 'rich_text', 'ingredients', __( 'Ingredientes', 'app' ) ),
			Field::make( 'text', 'weight', __( 'Peso', 'app' ) ),
			Field::make( 'file', 'crb_file', __( 'Arquivo para Download', 'app' ) )
			->set_value_type( 'url' ),
		]
	);

Container::make( 'post_meta', __( 'Tabela Nutricional', 'app' ) )
	->where( 'post_type', '=', 'fw-produtos' )
	->add_fields(
		[
			Field::make( 'text', 'portion_si', __( 'Porção (g)', 'app' ) ),
			Field::make( 'text', 'portion', __( 'Porção (medida caseira)', 'app' ) ),
			Field::make( 'complex', 'items', __( 'Itens', 'app' ) )
				->set_collapsed()
				->add_fields(
					[
						Field::make( 'text', 'name', __( 'Nome', 'app' ) ),
						Field::make( 'text', 'quantity', __( 'Quantidade por Porção', 'app' ) ),
						Field::make( 'text', 'vd', __( '%VD', 'app' ) ),
						Field::make( 'text', 'indentation', __( 'Indentação', 'app' ) )
							->set_attribute( 'type', 'number' )
							->set_default_value( 0 ),
					]
				)
				->set_header_template(
					'<% if (name && vd && quantity) { %>
						<%- name %>: <%- quantity %> (%VD <%- vd %>)
					<% } %>'
				),
			Field::make( 'rich_text', 'notes', __( 'Observações', 'app' ) ),
		]
	);

