<?php declare( strict_types = 1 );

/**
 * Theme Options.
 *
 * Here, you can register Theme Options using the Carbon Fields library.
 *
 * @link https://carbonfields.net/docs/containers-theme-options/
 * @package WPEmergeCli
 */

use Carbon_Fields\Container\Container;
use Carbon_Fields\Field\Field;

Container::make( 'theme_options', __( 'Social Medias', 'app' ) )
	->set_page_parent( 'themes.php' )
	->add_fields(
		[
			Field::make( 'text', 'footer_social_text', __( 'Título da seção de redes sociais', 'app' ) )
				->set_default_value( 'Siga-nos em nossas redes' ),
			Field::make( 'complex', 'footer_social_items', __( 'Redes sociais', 'app' ) )
				->add_fields(
					[
						Field::make( 'text', 'link', __( 'Link da rede social', 'app' ) ),
					]
				),
		]
	);
