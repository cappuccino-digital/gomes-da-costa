<?php declare( strict_types = 1 );

/**
 * Theme Options.
 *
 * Here, you can register Theme Options using the Carbon Fields library.
 *
 * @link https://carbonfields.net/docs/containers-theme-options/
 * @package WPEmergeCli
 */

use Carbon_Fields\Container\Container;
use Carbon_Fields\Field\Field;

Container::make( 'theme_options', __( 'Títulos', 'app' ) )
->set_page_parent( 'themes.php' )
->add_fields(
	[
		Field::make( 'text', 'title-recipes', __( 'Título da página de receitas', 'app' ) ),
		Field::make( 'text', 'title-products', __( 'Título da página de produtos', 'app' ) ),
		Field::make( 'text', 'title-social', __( 'Título do bloco das redes sociais', 'app' ) ),
		Field::make( 'text', 'title-products-section', __( 'Título da seção de produtos', 'app' ) ),
		Field::make( 'text', 'title-recipes-section', __( 'Título da seção de receitas', 'app' ) ),
		Field::make( 'text', 'title-recipes-most-liked', __( 'Título da seção de receitas mais curtidas', 'app' ) ),
		Field::make( 'text', 'title-downloads', __( 'Título da página de download de imagens', 'app' ) ),
	]
);
