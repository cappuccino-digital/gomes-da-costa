<?php declare( strict_types = 1 );

/**
 * Post Meta.
 *
 * Here, you can register custom post meta fields using the Carbon Fields library.
 *
 * @link https://carbonfields.net/docs/containers-post-meta/
 * @package WPEmergeCli
 */

// phpcs:disable
/*
use Carbon_Fields\Container\Container;
use Carbon_Fields\Field\Field;

Container::make( 'user_meta', __( 'Custom Data', 'app' ) )
	->add_fields( array(
		Field::make( 'image', 'crb_img' ),
	));
*/
// phpcs:enable
