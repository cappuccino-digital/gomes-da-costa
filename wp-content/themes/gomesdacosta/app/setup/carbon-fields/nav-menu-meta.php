<?php declare( strict_types = 1 );

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'nav_menu_item', 'Menu Settings' )
	->add_fields(
		[
			Field::make( 'image', 'icon', __( 'Ícone do campo', 'app' ) ),
		]
	);
