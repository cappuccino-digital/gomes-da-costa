<?php declare( strict_types = 1 );

/**
 * Post Meta.
 *
 * Here, you can register custom post meta fields using the Carbon Fields library.
 *
 * @link https://carbonfields.net/docs/containers-post-meta/
 * @package WPEmergeCli
 */

use Carbon_Fields\Container\Container;
use Carbon_Fields\Field\Field;

Container::make( 'post_meta', __( 'Detalhes', 'app' ) )
	->where( 'post_type', '=', 'fw-receitas' )
	->add_fields(
		[
			Field::make( 'oembed', 'crb_oembed', __( 'Video destacado', 'app' ) ),
			Field::make( 'text', 'time', __( 'Tempo total', 'app' ) ),
			Field::make( 'text', 'yield', __( 'Rendimento', 'app' ) ),
			Field::make( 'association', 'products', __( 'Produtos da receita', 'app' ) )
				->set_types(
					[
						[
							'type' => 'post',
							'post_type' => 'fw-produtos',
						],
					]
				),
			Field::make( 'rich_text', 'ingredients', __( 'Ingredientes', 'app' ) ),
			Field::make( 'rich_text', 'steps', __( 'Modo de preparo', 'app' ) ),
			Field::make( 'rich_text', 'chef_clue', __( 'Dica do chef', 'app' ) ),
			Field::make( 'rich_text', 'variation', __( 'Variação', 'app' ) ),
		]
	);
