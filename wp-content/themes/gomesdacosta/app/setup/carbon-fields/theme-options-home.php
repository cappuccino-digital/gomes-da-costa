<?php declare( strict_types = 1 );

/**
 * Post Meta.
 *
 * Here, you can register custom post meta fields using the Carbon Fields library.
 *
 * @link https://carbonfields.net/docs/containers-post-meta/
 * @package WPEmergeCli
 */

use Carbon_Fields\Container\Container;
use Carbon_Fields\Field\Field;

Container::make( 'theme_options', __( 'Home Page', 'app' ) )
	->set_page_parent( 'themes.php' )
	->add_fields(
		[
			Field::make( 'complex', 'home_banner', __( 'Slide da Home Page', 'app' ) )
				->add_fields(
					[
						Field::make( 'image', 'featured_banner', __( 'Imagem destacada Desktop', 'app' ) ),
						Field::make( 'image', 'featured_banner_mobile', __( 'Imagem destacada Mobile', 'app' ) ),
						Field::make( 'text', 'cta_text', __( 'Texto do CTA', 'app' ) ),
						Field::make( 'text', 'cta_link', __( 'Link do CTA', 'app' ) ),
						Field::make( 'checkbox', 'new_tab', __( 'Abrir em nova aba', 'app' ) ),
					]
				),
			Field::make( 'text', 'home_recipes_count_mobile', __( 'Quantidade de receitas exibidas no mobile', 'app' ) )
				->set_attribute( 'type', 'number' )
				->set_default_value( 3 ),
			Field::make( 'text', 'home_recipes_count_desktop', __( 'Quantidade de receitas exibidas no desktop', 'app' ) )
				->set_attribute( 'type', 'number' )
				->set_default_value( 10 ),
			Field::make( 'complex', 'social_medias', __( 'Mídias sociais', 'app' ) )
				->add_fields(
					[
						Field::make( 'complex', 'cta_media', __( 'Chamada', 'app' ) )
							->set_max( 1 )
							->set_required()
							->add_fields(
								'image',
								'Imagem',
								[
									Field::make( 'image', 'mobile', __( 'Imagem Mobile', 'app' ) ),
									Field::make( 'image', 'desktop', __( 'Imagem Desktop', 'app' ) ),
								]
							)
							->add_fields(
								'video',
								'Vídeo',
								[
									Field::make( 'oembed', 'video', __( 'Vídeo', 'app' ) ),
								]
							),
						Field::make( 'text', 'media_title', __( 'Título da rede social', 'app' ) ),
						Field::make( 'text', 'cta_text', __( 'Texto do CTA', 'app' ) ),
						Field::make( 'text', 'cta_link', __( 'Link do CTA', 'app' ) ),
						Field::make( 'checkbox', 'new_tab', __( 'Abrir em nova aba', 'app' ) ),
					]
				),
			Field::make( 'image', 'footer_banner', __( 'Call To Action mobile do final da página inicial', 'app' ) ),
			Field::make( 'image', 'footer_banner_desktop', __( 'Call To Action desktop do final da página inicial', 'app' ) ),
			Field::make( 'text', 'footer_banner_link', __( 'Link do Call To Action', 'app' ) ),
			Field::make( 'checkbox', 'footer_banner_new_tab', __( 'Abrir Call To Action em nova aba', 'app' ) ),
		]
	);
