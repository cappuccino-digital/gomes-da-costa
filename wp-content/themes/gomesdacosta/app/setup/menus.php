<?php declare( strict_types = 1 );

namespace App;

/**
 * Register menu locations.
 *
 * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
 * @hook after_setup_theme
 * @package WPEmergeTheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

register_nav_menu( 'app_primary', __( 'Menu do site', 'app' ) );

