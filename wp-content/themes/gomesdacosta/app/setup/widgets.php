<?php declare( strict_types = 1 );

/**
 * Register widgets.
 *
 * @link https://developer.wordpress.org/reference/functions/register_widget/
 * @hook widgets_init
 * @package WPEmergeTheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Rich Text widget
 */
register_widget( App\Widgets\Carbon_Rich_Text_Widget::class );
