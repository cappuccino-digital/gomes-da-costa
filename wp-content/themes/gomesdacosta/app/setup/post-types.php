<?php declare( strict_types = 1 );

/**
 * Register post types.
 *
 * @link https://developer.wordpress.org/reference/functions/register_post_type/
 * @hook init
 * @package WPEmergeTheme
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

register_post_type(
	'fw-receitas',
	[
		'labels' => [
			'name' => __( 'Receitas', 'app' ),
			'singular_name' => __( 'Receita', 'app' ),
			'add_new' => __( 'Adicionar nova', 'app' ),
			'add_new_item' => __( 'Adicionar nova Receita', 'app' ),
			'view_item' => __( 'Ver Receita', 'app' ),
			'edit_item' => __( 'Editar Receita', 'app' ),
			'new_item' => __( 'Nova Receita', 'app' ),
			'search_items' => __( 'Buscar Receitas', 'app' ),
			'not_found' => __( 'Nenhuma Receita encontrada', 'app' ),
			'not_found_in_trash' => __( 'Nenhuma Receita encontrada na lixeira', 'app' ),
		],
		'public' => true,
		'has_archive' => true,
		'	' => 'dashicons-carrot',
		'supports' => [ 'title', 'thumbnail', 'comments' ],
		'rewrite' => [
			'slug' => 'receitas',
		],
	]
);

register_post_type(
	'fw-produtos',
	[
		'labels' => [
			'name' => __( 'Produtos', 'app' ),
			'singular_name' => __( 'Produto', 'app' ),
			'add_new' => __( 'Adicionar novo', 'app' ),
			'add_new_item' => __( 'Adicionar novo Produto', 'app' ),
			'edit_item' => __( 'Editar Produto', 'app' ),
			'new_item' => __( 'Novo Produto', 'app' ),
			'view_item' => __( 'Ver Produto', 'app' ),
			'search_items' => __( 'Procurar Produtos', 'app' ),
			'not_found' => __( 'Não foram encontrados Produtos', 'app' ),
			'not_found_in_trash' => __( 'Não foram encontrados Produtos na lixeira', 'app' ),
		],
		'public' => true,
		'has_archive' => false,
		'menu_icon' => 'dashicons-products',
		'supports' => [ 'title', 'editor', 'thumbnail', 'page-attributes' ],
		'rewrite' => [
			'slug' => 'produtos',
		],
	]
);
