<?php declare( strict_types = 1 );

/**
 * Recipe.
 *
 * @package WPEmergeTheme
 */

namespace App;

use App\Controllers\RestApi\RecipeController;
use WPEmerge\Facades\Application;

register_rest_route(
	'gdc/v1',
	'/receitas/curtir',
	[
		'methods' => 'POST',
		'callback' => static function () {
			return call_user_func_array( //phpcs:ignore NeutronStandard.Functions.DisallowCallUserFunc.CallUserFunc
				[Application::resolve( RecipeController::class ), 'do_like'],
				func_get_args()
			);
		},
		'args' => [
			'id' => [
				'description' => __( 'Id da receita.', 'app' ),
				'required' => true,
				'type' => 'integer',
			],
			'nonce' => [
				'description' => __( 'Nonce.', 'app' ),
				'required' => true,
				'type' => 'string',
				'validate_callback' => static function( $nonce ): bool {
					return (bool) wp_verify_nonce( $nonce, 'do_like_recipe' );
				},
			],
		],
	]
);

register_rest_route(
	'gdc/v1',
	'/receitas/descurtir',
	[
		'methods' => 'POST',
		'callback' => static function () {
			return call_user_func_array( //phpcs:ignore NeutronStandard.Functions.DisallowCallUserFunc.CallUserFunc
				[Application::resolve( RecipeController::class ), 'remove_like'],
				func_get_args()
			);
		},
		'args' => [
			'id' => [
				'description' => __( 'Id da receita.', 'app' ),
				'required' => true,
				'type' => 'integer',
			],
			'nonce' => [
				'description' => __( 'Nonce.', 'app' ),
				'required' => true,
				'type' => 'string',
				'validate_callback' => static function( $nonce ): bool {
					return (bool) wp_verify_nonce( $nonce, 'do_like_recipe' );
				},
			],
		],
	]
);
