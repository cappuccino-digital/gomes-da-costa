<?php declare( strict_types = 1 );

/**
 * Views.
 *
 * @link https://docs.wpemerge.com/#/framework/views/overview
 * @package WPEmergeTheme
 */

use WPEmerge\Facades\View;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * ------------------------------------------------------------------------
 * Globals
 *
 * @link https://docs.wpemerge.com/#/framework/views/overview
 * ------------------------------------------------------------------------
 */

// phpcs:ignore
// View::addGlobal( 'foo', 'bar' );

/**
 * ------------------------------------------------------------------------
 * View composers
 *
 * @link https://docs.wpemerge.com/#/framework/views/view-composers
 * ------------------------------------------------------------------------
 */

View::addComposer( 'partials/recipes/header', App\View\Recipes\HeaderViewComposer::class );
View::addComposer( 'partials/recipes/related-recipes', App\View\Recipes\RelatedRecipesViewComposer::class );
View::addComposer( 'partials/recipes/popular-recipes', App\View\Recipes\PopularRecipesViewComposer::class );
View::addComposer( 'partials/recipes/used-products', App\View\Recipes\UsedProductsViewComposer::class );
View::addComposer( 'partials/recipes/metadata', App\View\Recipes\MetadataViewComposer::class );
View::addComposer( 'partials/recipes/filters', App\View\Recipes\FiltersViewComposer::class );
View::addComposer( 'partials/recipes/recipes-category', App\View\Recipes\CategoryViewComposer::class );
View::addComposer( 'partials/recipes/home-header', App\View\Recipes\HomeHeaderViewComposer::class );
View::addComposer( 'partials/recipes/like', App\View\Recipes\LikeViewComposer::class );
View::addComposer( 'partials/products/related-recipes', App\View\Products\RelatedRecipesViewComposer::class );
View::addComposer( 'partials/products/metadata', App\View\Products\MetadataViewComposer::class );
View::addComposer( 'partials/products/header', App\View\Products\HeaderViewComposer::class );
View::addComposer( 'partials/products/filters', App\View\Products\FiltersViewComposer::class );
View::addComposer( 'partials/products/product-type-list', App\View\Products\ProductTypeListViewComposer::class );
View::addComposer( 'partials/products/nutritional-information', App\View\Products\NutritionalInformationViewComposer::class );
View::addComposer( 'partials/homepage/header', App\View\Homepage\HeaderViewComposer::class );
View::addComposer( 'partials/homepage/featured-banner', App\View\Homepage\FeaturedBannerViewComposer::class );
View::addComposer( 'partials/homepage/products-section', App\View\Homepage\ProductSectionViewComposer::class );
View::addComposer( 'partials/homepage/social', App\View\Homepage\SocialSectionViewComposer::class );
View::addComposer( 'partials/homepage/footer-banner', App\View\Homepage\FooterBennerViewComposer::class );
View::addComposer( 'partials/shared/special-recipes', App\View\Recipes\SpecialRecipesViewComposer::class );
View::addComposer( 'partials/shared/social-media', App\View\SocialMediaViewComposer::class );
View::addComposer( 'partials/shared/navbar-social', App\View\SocialMediaViewComposer::class );
View::addComposer( 'partials/shared/newsletter-form', App\View\NewsletterFormViewComposer::class );
View::addComposer( 'partials/shared/picture', App\View\PictureViewComposer::class );
View::addComposer( 'partials/shared/pagination', App\View\PaginationViewComposer::class );
View::addComposer( 'partials/shared/video', App\View\VideoViewComposer::class );
View::addComposer( 'partials/downloads/products-accordion', App\View\Downloads\ProductArchiveViewComposer::class );
View::addComposer( 'partials/downloads/products-category', App\View\Downloads\ProductsCategoryViewComposer::class );
View::addComposer( 'partials/search/search-filters', App\View\SearchViewComposer::class );

