<?php declare( strict_types = 1 );

/**
 * Backwards compatibility shims.
 *
 * @package WPEmergeTheme
 */

namespace App;

/**
 * Shim for wp_body_open() avoiding the "wp_" prefix rule violation in Theme Check.
 */
function shim_wp_body_open(): void {
	if ( function_exists( 'wp_body_open' ) ) {
		wp_body_open();
	} else {
		do_action( 'wp_body_open' );
	}
}
