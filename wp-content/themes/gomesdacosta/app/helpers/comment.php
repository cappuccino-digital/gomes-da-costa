<?php declare( strict_types = 1 );

namespace App;

/**
 * Order Comments
 *
 * @param array<string> $fields Fields.
 * @return array<string>
 */
function order_comment( array $fields ): array {

	$comment_field = $fields['comment'];
	unset( $fields['comment'] );

	$cookie_field = $fields['cookies'];
	unset( $fields['cookies'] );

	$fields['comment'] = $comment_field;
	$fields['cookies'] = $cookie_field;

	return $fields;
}
