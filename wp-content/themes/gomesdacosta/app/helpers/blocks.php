<?php declare( strict_types = 1 );

namespace App;

/**
 * Register block categories
 *
 * @param array<string> $categories Categories.
 * @return array<string>
 */
function block_categories( array $categories ): array {
	$categories[] = [
		'slug' => 'gdc-blocks',
		'title' => __( 'Blocos Gomes', 'app' ),
	];

	return $categories;
}

/**
 * Register block type.
 *
 * @since 0.1.0
 */
function register_blocks(): void {
	register_block_type(
		'app/container',
		[
			'title' => __( 'Container', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/columns',
		[
			'title' => __( 'Colunas', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/accordion',
		[
			'title' => __( 'Acordeon', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/accordion-item',
		[
			'title' => __( 'Item de acordeon', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_blocks_about_us();
	register_blocks_contact_us();
}

/**
 * Register block type at Page Contact Us.
 *
 * @since 0.1.0
 */
function register_blocks_contact_us(): void {
	register_block_type(
		'app/section-listen',
		[
			'title' => __( 'Container de Contatos', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/contact-work',
		[
			'title' => __( 'Trabalhe Conosco', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/contact-advisory',
		[
			'title' => __( 'Assessoria de imprensa', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/advisory-item',
		[
			'title' => __( 'Item de Assessoria', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/work-link',
		[
			'title' => __( 'Link de Trabalho', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/contact-attendance',
		[
			'title' => __( 'Atendimento ao Cliente', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/accordion-location-item',
		[
			'title' => __( 'Localização Item Acordeon', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/accordion-location',
		[
			'title' => __( 'Acordeon de Localização', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
}

/**
 * Register block type at Page About Us.
 *
 * @since 0.1.0
 */
function register_blocks_about_us(): void {
	register_block_type(
		'app/history',
		[
			'title' => __( 'Slider de História', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/history-item',
		[
			'title' => __( 'Item de História', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/responsability',
		[
			'title' => __( 'Slider de Responsabilidades', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/responsability-item',
		[
			'title' => __( 'Item de Responsabilidades', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/description',
		[
			'title' => __( 'Descrição', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
	register_block_type(
		'app/description-item',
		[
			'title' => __( 'Descrição Item', 'app' ),
			'category' => 'gdc-blocks',
		]
	);
}
