<?php declare( strict_types = 1 );

namespace App;

/**
 * Get allowed attributes for image.
 *
 * @return array<string>
 */
function get_allowed_html_attributes_for_image(): array {
	return [
		'use' => [
			'xlink:href' => [],
		],
		'svg' => [],
		'img' => [
			'alt' => [],
			'class' => [],
			'src' => [],
			'srcset' => [],
			'sizes' => [],
		],
	];
}
