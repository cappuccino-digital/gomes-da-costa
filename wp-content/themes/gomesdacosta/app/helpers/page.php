<?php declare( strict_types = 1 );

namespace App;

/**
 * ID of a template page
 *
 * Retrieve the ID from a page that use a specific Template Page.
 *
 * @param string $template_page The file name of the template page to check.
 * @return int Return the page ID if exists a page with the $template_page. If more than one
 * pages uses the $template_page is returned only ID of the first returned by mysql.
 */
function get_template_page_id( string $template_page ): int {
	global $wpdb;

	if ( ! $template_page ) {
		return 0;
	}

	$cache_key = "_wp_page_template/{$template_page}";
	$page_id = wp_cache_get( $cache_key );

	if ( $page_id === false ) {
		// phpcs:ignore WordPress.DB.DirectDatabaseQuery
		$page_id = $wpdb->get_var(
			$wpdb->prepare(
				"SELECT
					post_id
				FROM
					{$wpdb->postmeta}
				WHERE
					meta_key = '_wp_page_template' AND meta_value = %s",
				$template_page
			)
		);

		wp_cache_set( $cache_key, $page_id );
	}

	return (int) $page_id;
}
