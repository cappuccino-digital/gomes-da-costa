<?php declare( strict_types = 1 );

/**
 * Custom Shortcodes.
 *
 * Here, you can register Custom Shortcode for use in the Theme.
 *
 * @link https://developer.wordpress.org/reference/functions/add_shortcode/
 * @link https://developer.wordpress.org/reference/functions/shortcode_atts/
 * @package WPEmergeTheme
 */

namespace App;

