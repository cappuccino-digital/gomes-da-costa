<?php declare( strict_types = 1 );

namespace App;

function apply_product_type_filter( \WP_Query $query ): void {
	if ( ! $query->is_main_query() || is_admin() ) {
		return;
	}

	if ( ! is_post_type_archive( 'fw-receitas' ) && ! is_tax( 'fw-receita-ocasiao' ) ) {
		return;
	}

	// phpcs:ignore
	$term = $_GET['tipo-de-produto'] ?? false;

	if ( ! $term ) {
		return;
	}

	$tax_query = [
		[
			'taxonomy' => 'fw-produto-category',
			'field' => 'slug',
			'terms' => [ $term ],
			'operator' => 'IN',
		],
	];
	$query->set( 'tax_query', $tax_query );
}

function apply_time_filter( \WP_Query $query ): void {
	if ( ! $query->is_main_query() || is_admin() ) {
		return;
	}

	if ( ! is_post_type_archive( 'fw-receitas' ) && ! is_tax( 'fw-receita-ocasiao' ) ) {
		return;
	}

	// phpcs:ignore
	$time = $_GET['tempo'] ?? false;

	if ( ! $time ) {
		return;
	}

	$reverse = strrpos( $time, 's' );

	if ( $reverse ) {
		$value = (int) str_replace( 's', '', $time );
		$compare = '>';
	} else {
		$value = [( (int) $time ) - 15, (int) $time];
		$compare = 'BETWEEN';
	}

	$meta_query = [
		[
			'key' => '_time',
			'value' => $value,
			'compare' => $compare,
			'type' => 'NUMERIC',
		],
	];

	$query->set( 'meta_query', $meta_query );
}

function apply_general_search_filters( \WP_Query $query ): void {
	if ( ! $query->is_main_query() || is_admin() || ! is_search() ) {
		return;
	}

	// phpcs:ignore
	$type = $_GET['type'] ?? false;

	if ( ! $type ) {
		return;
	}

	$query->set( 'post_type', $type );
}

function apply_portion_filter( \WP_Query $query ): void {
	if ( ! $query->is_main_query() || is_admin() ) {
		return;
	}

	if ( ! is_post_type_archive( 'fw-receitas' ) && ! is_tax( 'fw-receita-ocasiao' ) ) {
		return;
	}

	// phpcs:ignore
	$portion = $_GET['porcoes'] ?? false;

	if ( ! $portion ) {
		return;
	}

	$yields = explode( '|', $portion );

	$meta_query = [
		[
			'key' => '_yield',
			'value' => count( $yields ) > 1 ? $yields : (int) $yields[0],
			'compare' => count( $yields ) > 1 ? 'BETWEEN' : '>' ,
			'type' => 'NUMERIC',
		],
	];

	$query->set( 'meta_query', $meta_query );
}
