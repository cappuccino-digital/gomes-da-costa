<?php declare( strict_types = 1 );

/**
 * Login helpers.
 *
 * @package WPEmergeTheme
 */

namespace App;

/**
 * Changes the URL of the logo on the login screen.
 *
 * @return string Link to the Homepage.
 */
function get_login_headerurl(): string {
	return home_url( '/' );
}

/**
 * Changes the text of the logo on the login Screen.
 *
 * @return string Site Title.
 */
function get_login_headertext(): string {
	return get_bloginfo( 'name' );
}
