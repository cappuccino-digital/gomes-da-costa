<?php declare( strict_types = 1 );

namespace App;

function customize_register( \WP_Customize_Manager $wp_customize ): void {
	$custom_logo_args = get_theme_support( 'custom-logo' );

	$wp_customize->add_setting( 'footer_logo' );

	$wp_customize->add_control(
		new \WP_Customize_Cropped_Image_Control(
			$wp_customize,
			'footer_logo',
			[
				'label' => __( 'Logo do rodapé', 'app' ),
				'section' => 'title_tagline',
				'settings' => 'footer_logo',
				'priority' => 8,
				'height' => $custom_logo_args[0]['height'],
				'width' => $custom_logo_args[0]['width'],
				'flex_height' => $custom_logo_args[0]['flex-height'],
				'flex_width' => $custom_logo_args[0]['flex-width'],
				'button_labels' => [
					'select' => __( 'Select logo' ),
					'change' => __( 'Change logo' ),
					'remove' => __( 'Remove' ),
					'default' => __( 'Default' ),
					'placeholder' => __( 'No logo selected' ),
					'frame_title' => __( 'Select logo' ),
					'frame_button' => __( 'Choose logo' ),
				],
			]
		)
	);
}
