<?php declare( strict_types = 1 );

namespace App;

function get_zero_parent_id( \WP_Term $term ): int {
	if ( $term->parent === 0 ) {
		return $term->term_id;
	}

	$parent_term = get_term( $term->parent );

	return is_a( $parent_term, 'WP_Term' )
		? get_zero_parent_id( $parent_term )
		: $term->term_id;
}

function get_current_search_link(): string {

	$term = get_queried_object();

	if ( is_a( $term, 'WP_Term' ) && is_tax( 'fw-receita-ocasiao' ) ) {
		return get_term_link( $term );
	}

	return get_post_type_archive_link( 'fw-receitas' );
}

/**
 * Checks whether the current term is fw_receita_ocasiao.
 */
function is_fw_receita_ocasiao(): bool {
	$term = get_queried_object();

	return is_a( $term, 'WP_Term' ) && is_tax( 'fw-receita-ocasiao' );
}
