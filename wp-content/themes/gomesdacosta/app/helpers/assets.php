<?php declare( strict_types = 1 );

/**
 * Asset helpers.
 *
 * @package WPEmergeTheme
 */

namespace App;

use WPEmergeTheme\Facades\Assets;
use WPEmergeTheme\Facades\Theme;

/**
 * Enqueue front-end assets.
 */
function enqueue_theme_assets(): void {
	$template_dir = Theme::uri();

	/**
	 * Enqueue the built-in comment-reply script for singular pages.
	 */
	if ( is_singular() ) {
		wp_enqueue_script( 'comment-reply' );
	}

	/**
	 * Enqueue scripts.
	 */
	wp_enqueue_script(
		'theme-js-bundle',
		$template_dir . '/dist/theme.js',
		['jquery', 'wp-api'],
		filemtime( get_theme_file_path( '../dist/theme.js' ) ),
		true
	);

	wp_enqueue_script(
		'main-js-bundle',
		$template_dir . '/dist/main.js',
		['jquery', 'wp-api'],
		filemtime( get_theme_file_path( '../dist/main.js' ) ),
		true
	);

	$recipe_link = get_post_type_archive_link( 'fw-receitas' );

	if ( is_tax( 'fw-receita-ocasiao' ) ) {
		$recipe_link = get_term_link( get_queried_object() );
	}

	wp_localize_script( 'theme-js-bundle', 'recipeLink', $recipe_link );

	/**
	 * Enqueue styles.
	 */
	wp_enqueue_style(
		'theme-css-bundle',
		$template_dir . '/dist/styles/theme.css',
		[],
		filemtime( get_theme_file_path( '../dist/styles/theme.css' ) )
	);

	/**
	 * Enqueue theme's style.css file to allow overrides for the bundled styles.
	 */
	Assets::enqueueStyle( 'theme-styles', get_template_directory_uri() . '/style.css' );
}

/**
 * Enqueue admin assets.
 */
function enqueue_admin_assets(): void {
	$template_dir = Theme::uri();

	/**
	 * Enqueue scripts.
	 */
	Assets::enqueueScript(
		'theme-admin-js-bundle',
		$template_dir . '/dist/admin.js',
		['jquery'],
		true
	);

	Assets::enqueueScript(
		'theme-admin-blocks-js-bundle',
		$template_dir . '/dist/blocks.js',
		['wp-blocks', 'wp-i18n', 'wp-element', 'wp-components'],
		true
	);

	/**
	 * Enqueue styles.
	 */
	Assets::enqueueStyle(
		'theme-admin-css-bundle',
		$template_dir . '/dist/styles/admin.css'
	);
}

/**
 * Enqueue login assets.
 */
function enqueue_login_assets(): void {
	$template_dir = Theme::uri();

	/**
	 * Enqueue scripts.
	 */
	Assets::enqueueScript(
		'theme-login-js-bundle',
		$template_dir . '/dist/login.js',
		['jquery'],
		true
	);

	/**
	 * Enqueue styles.
	 */
	Assets::enqueueStyle(
		'theme-login-css-bundle',
		$template_dir . '/dist/styles/login.css'
	);
}

/**
 * Enqueue editor assets.
 */
function enqueue_editor_assets(): void {
	$template_dir = Theme::uri();

	/**
	 * Enqueue scripts.
	 */
	Assets::enqueueScript(
		'theme-editor-js-bundle',
		$template_dir . '/dist/editor.js',
		['jquery'],
		true
	);

	/**
	 * Enqueue styles.
	 */
	Assets::enqueueStyle(
		'theme-editor-css-bundle',
		$template_dir . '/dist/styles/editor.css'
	);
}

/**
 * Add favicon proxy.
 *
 * @link WPEmergeTheme\Assets\Assets::addFavicon()
 */
function add_favicon(): void {
	Assets::addFavicon();
}

/**
 * Enqueue sprite svg.
 */
function enqueue_sprite_svg(): void {
	$file = APP_DIST_DIR . 'images/svg-sprite.svg';

	if ( ! file_exists( $file ) ) {
		return;
	}

	include $file;
}
