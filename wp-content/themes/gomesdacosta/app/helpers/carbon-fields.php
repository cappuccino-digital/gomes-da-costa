<?php declare( strict_types = 1 );

/**
 * Load Carbon Fields.
 *
 * @package WPEmergeCli
 */

namespace App;

use Carbon_Fields\Carbon_Fields;

/**
 * Bootstrap Carbon Fields.
 */
function bootstrap_carbon_fields(): void {
	Carbon_Fields::boot();
}

/**
 * Bootstrap Carbon Fields container definitions.
 */
function bootstrap_carbon_fields_register_fields(): void {
	include_once APP_APP_SETUP_DIR . 'carbon-fields' . DIRECTORY_SEPARATOR . 'theme-options-receita.php';
	include_once APP_APP_SETUP_DIR . 'carbon-fields' . DIRECTORY_SEPARATOR . 'theme-options-produtos.php';
	include_once APP_APP_SETUP_DIR . 'carbon-fields' . DIRECTORY_SEPARATOR . 'theme-options-home.php';
	include_once APP_APP_SETUP_DIR . 'carbon-fields' . DIRECTORY_SEPARATOR . 'theme-options-forms.php';
	include_once APP_APP_SETUP_DIR . 'carbon-fields' . DIRECTORY_SEPARATOR . 'theme-options-social-media.php';
	include_once APP_APP_SETUP_DIR . 'carbon-fields' . DIRECTORY_SEPARATOR . 'theme-options-titles.php';
	include_once APP_APP_SETUP_DIR . 'carbon-fields' . DIRECTORY_SEPARATOR . 'post-meta-produto.php';
	include_once APP_APP_SETUP_DIR . 'carbon-fields' . DIRECTORY_SEPARATOR . 'post-meta-receita.php';
	include_once APP_APP_SETUP_DIR . 'carbon-fields' . DIRECTORY_SEPARATOR . 'term-meta-produto.php';
	include_once APP_APP_SETUP_DIR . 'carbon-fields' . DIRECTORY_SEPARATOR . 'user-meta.php';
	include_once APP_APP_SETUP_DIR . 'carbon-fields' . DIRECTORY_SEPARATOR . 'nav-menu-meta.php';
}

/**
 * Filter Google Maps API key for Carbon Fields.
 */
function get_google_maps_api_key(): string {
	return carbon_get_theme_option( 'crb_google_maps_api_key' );
}
