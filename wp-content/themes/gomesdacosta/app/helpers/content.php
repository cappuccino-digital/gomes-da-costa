<?php declare( strict_types = 1 );

/**
 * Content helpers.
 *
 * @package WPEmergeTheme
 */

namespace App;

/**
 * Filter excerpt more.
 *
 * @link https://codex.wordpress.org/Plugin_API/Filter_Reference/excerpt_more/
 */
function get_excerpt_more(): string {
	return '...';
}

/**
 * Filter excerpt length.
 *
 * @link https://developer.wordpress.org/reference/hooks/excerpt_length/
 */
function get_excerpt_length(): int {
	return 55;
}

/**
 * Filter shortcode usage leading to empty paragraphs around it.
 *
 * @param  string $content Rich content to filter.
 */
function fix_shortcode_empty_paragraphs( string $content ): string {
	$replacements = [
		'<p>[' => '[',
		']</p>' => ']',
		']<br />' => ']',
		']<br>' => ']',
	];

	$content = strtr( $content, $replacements );

	return $content;
}

/**
 * Fix upload_dir urls having incorrect url schema.
 *
 * The wp_upload_dir() function urls' schema depends on the site_url option which
 * can cause issues when HTTPS is forced using a plugin, for example.
 *
 * @link https://core.trac.wordpress.org/ticket/25449
 * @param  array<string> $upload_dir Array containing the current upload directory’s path and url.
 * @return array<string> Filtered array.
 */
function fix_upload_dir_url_schema( array $upload_dir ): array {
	if ( is_ssl() ) {
		$upload_dir['url'] = set_url_scheme( $upload_dir['url'], 'https' );
		$upload_dir['baseurl'] = set_url_scheme( $upload_dir['baseurl'], 'https' );
	} else {
		$upload_dir['url'] = set_url_scheme( $upload_dir['url'], 'http' );
		$upload_dir['baseurl'] = set_url_scheme( $upload_dir['baseurl'], 'http' );
	}

	return $upload_dir;
}

/**
 * Get text suitable for the title attribute of a permalink anchor tag.
 *
 * @return string The title attribute value
 */
function get_permalink_title(): string {
	/* translators: post link title attribute */
	return sprintf( __( 'Permanent Link to %s', 'app' ), get_the_title() );
}

/**
 * Escape user input from WYSIWYG editors.
 *
 * Calls all filters usually executed on `the_content`.
 *
 * @param  string $content The content that needs to be escaped.
 * @return string The escaped content.
 */
function get_content( string $content ): string {
	return apply_filters( 'get_content', $content );
}

/**
 * Echo classnames based on a key value array.
 *
 * @param array<string> $items Items.
 */
function the_classnames( array $items ): void {
	echo esc_attr(
		implode(
			' ',
			array_keys(
				$items,
				true,
				true
			)
		)
	);
}

/**
 * Get text suitable for the title attribute of a permalink anchor tag.
 *
 * @return string The title attribute value
 */
function get_index_404_message(): string {
	if ( is_category() ) {
		/* translators: no posts found for category */
		return sprintf( __( 'Sorry, but there aren\'t any posts in the %s category yet.', 'app' ), single_cat_title( '', false ) );
	}

	if ( is_date() ) {
		return __( 'Sorry, but there aren\'t any posts with this date.', 'app' );
	}

	if ( is_author() ) {
		$userdata = get_user_by( 'id', get_queried_object_id() );

		/* translators: no posts found for author */
		return sprintf( __( 'Sorry, but there aren\'t any posts by %s yet.', 'app' ), $userdata->display_name );
	}

	if ( is_search() ) {
		return __( 'No posts found. Try a different search?', 'app' );
	}

	return __( 'No posts found.', 'app' );
}
