<?php declare( strict_types = 1 );

namespace App;

function change_svg_load(): string {
	return 'wp_body_open';
}

/**
 * Returns a custom logo colorful, linked to home.
 *
 * @return string Custom logo markup.
 */
function get_footer_logo(): string {
	$html = '';
	$custom_logo_id = get_theme_mod( 'footer_logo' );

	// We have a logo. Logo is go.
	if ( $custom_logo_id ) {
		$custom_logo_attr = [
			'class' => 'custom-logo',
		];

		/*
		 * If the logo alt attribute is empty, get the site title and explicitly
		 * pass it to the attributes used by wp_get_attachment_image().
		 */
		$image_alt = get_post_meta( $custom_logo_id, '_wp_attachment_image_alt', true );

		if ( ! $image_alt ) {
			$custom_logo_attr['alt'] = get_bloginfo( 'name', 'display' );
		}

		/*
		 * If the alt attribute is not empty, there's no need to explicitly pass
		 * it because wp_get_attachment_image() already adds the alt attribute.
		 */
		$html = sprintf(
			'<a href="%1$s" class="custom-logo-link" rel="home">%2$s</a>',
			esc_url( home_url( '/' ) ),
			wp_get_attachment_image( $custom_logo_id, 'full', false, $custom_logo_attr )
		);
	} elseif ( is_customize_preview() ) {
		// If no logo is set but we're in the Customizer, leave a placeholder (needed for the live preview).
		$html = sprintf(
			'<a href="%1$s" class="custom-logo-link" style="display:none;"><img class="custom-logo"/></a>',
			esc_url( home_url( '/' ) )
		);
	}

	return $html;
}

/**
 * GEt image alt
 *
 * @param int $image_id Image id.
 */
function get_image_alt( int $image_id ): string {
	$image_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );

	if ( ! $image_alt ) {
		$image_alt = get_the_title( $image_id );
	}

	return $image_alt;
}
