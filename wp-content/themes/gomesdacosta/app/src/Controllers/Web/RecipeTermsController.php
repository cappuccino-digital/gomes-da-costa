<?php declare( strict_types = 1 );

namespace App\Controllers\Web;

use App\Services\RecipeService;
use WPEmerge;
use WPEmerge\Requests\Request;
use WPEmerge\View\PhpView;

class RecipeTermsController {

	/**
	 * RecipeService
	 *
	 * @var \App\Services\RecipeService
	 */
	protected $recipe_service;

	public function __construct( RecipeService $recipe_service ) {
		$this->recipe_service = $recipe_service;
	}

	/**
	 * Recipe Term archive.
	 *
	 * @param \WPEmerge\Requests\Request $request Request.
	 * @param string                     $view View.
	 * @SuppressWarnings("unused")
	 */
	public function special_recipes_archive( Request $request, string $view ): PhpView {
		$search = $request->get( 's' );

		return WPEmerge\view( $view )
			->with(
				[
					'not_found_recipes' => $this->get_not_found_text( $search ),
					'title' => $this->get_title( $search ),
					'all_recipe_image' => wp_get_attachment_image( \carbon_get_theme_option( 'popular_recipes_all_image' ), 'thumbnail', false, [ 'class' => 'box__image-content' ] ),
					'recipes' => $this->recipe_service->get_recipes( (int) \carbon_get_theme_option( 'recipes_count' ) ),
				]
			);
	}

	private function get_not_found_text( ?string $search ): string {
		if ( $search ) {
			return "Não encontramos nenhuma receita para o termo {$search} :(";
		}

		return 'Não foram encontradas receitas.';
	}

	private function get_title( ?string $search ): string {
		if ( is_search() && $search ) {
			return "Receitas encontradas para {$search}";
		}

		if ( is_tax( 'fw-produto-category' ) ) {
			$term_name = get_queried_object()->name;

			return "Veja abaixo receitas de {$term_name}";
		}

		if ( is_post_type_archive( 'fw-receitas' ) ) {
			$page_id = get_template_page_id( 'templates/recipes-home.php' );

			return get_the_title( $page_id );
		}

		if ( is_tax( 'fw-receita-ocasiao' ) ) {
			$title = carbon_get_theme_option( 'special_recipe_title' );

			if ( ! $title ) {
				$title = get_queried_object()->name;
			}

			return $title;
		}

		return get_the_title();
	}

}
