<?php declare( strict_types = 1 );

namespace App\Controllers\Web;

use WPEmerge;
use WPEmerge\Requests\Request;
use WPEmerge\View\PhpView;

class ContactUsController {

	/**
	 * Index.
	 *
	 * @param \WPEmerge\Requests\Request $request Request.
	 * @param string                     $view View.
	 * @SuppressWarnings("unused")
	 */
	public function index( Request $request, string $view ): PhpView {
		return WPEmerge\view( $view )->with(
			[
				'form_id' => carbon_get_theme_option( 'contact_form' ),
			]
		);
	}

}
