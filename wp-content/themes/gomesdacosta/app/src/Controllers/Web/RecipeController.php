<?php declare( strict_types = 1 );

namespace App\Controllers\Web;

use App\Services\ProductCategoryService;
use App\Services\RecipeService;
use WPEmerge;
use WPEmerge\Requests\Request;
use WPEmerge\View\PhpView;
use function App\get_template_page_id;

class RecipeController {

	/**
	 * RecipeService
	 *
	 * @var \App\Services\RecipeService
	 */
	protected $recipe_service;

	/**
	 * ProductCategoryService
	 *
	 * @var \App\Services\ProductCategoryService
	 */
	protected $pcategory_service;

	public function __construct( RecipeService $recipe_service, ProductCategoryService $pcategory_service ) {
		$this->recipe_service = $recipe_service;
		$this->pcategory_service = $pcategory_service;
	}

	/**
	 * Index.
	 *
	 * @param \WPEmerge\Requests\Request $request Request.
	 * @param string                     $view View.
	 * @SuppressWarnings("unused")
	 */
	public function index( Request $request, string $view ): PhpView {
		return WPEmerge\view( $view )->with( $this->recipe_service->get_data_by_id( get_the_ID() ) );
	}

	/**
	 * Template home recipe.
	 *
	 * @param \WPEmerge\Requests\Request $request Request.
	 * @param string                     $view View.
	 * @SuppressWarnings("unused")
	 */
	public function home_recipe( Request $request, string $view ): PhpView {
		return WPEmerge\view( $view )
			->with(
				[
					'title' => $this->get_title( $request->get( 's' ) ),
					'recipes_link' => get_post_type_archive_link( 'fw-receitas' ),
					'recipes' => $this->recipe_service->get_recipes( (int) \carbon_get_theme_option( 'recipes_count' ) ),
					'all_recipe_image' => wp_get_attachment_image( \carbon_get_theme_option( 'popular_recipes_all_image' ), 'thumbnail', false, [ 'class' => 'box__image-content' ] ),
				]
			);
	}

	/**
	 * Recipe archive.
	 *
	 * @param \WPEmerge\Requests\Request $request Request.
	 * @param string                     $view View.
	 */
	public function recipes_archive( Request $request, string $view ): PhpView {
		$search = $request->get( 's' );

		if ( ! $request->get( 's' ) ) {
			$search = '';
		}

		$product_type = $request->get( 'tipo-de-produto' );

		if ( ! $request->get( 'tipo-de-produto' ) ) {
			$product_type = '';
		}

		$request_uri = $request->server( 'REQUEST_URI' );
		$page = 1;

		if ( strpos( $request_uri, 'receitas/page' ) ) {
			$page = (int) explode( '/', $request_uri )[3];
		}

		return WPEmerge\view( $view )
		->with(
			[
				'not_found_recipes' => 'Não foram encontradas receitas para o termo buscado. Tente novamente.',
				'title' => $this->get_title( $search ),
				'all_recipe_image' => wp_get_attachment_image( \carbon_get_theme_option( 'popular_recipes_all_image' ), 'thumbnail', false, [ 'class' => 'box__image-content' ] ),
				'recipes' => $this->recipe_service->get_recipes( (int) \carbon_get_theme_option( 'recipes_count' ), false, $page, $search, $product_type ),
			]
		);
	}

	private function get_title( ?string $search ): string {
		if ( is_search() && $search ) {
			return 'Receitas encontradas para "' . $search . '"';
		}

		if ( is_tax( 'fw-produto-category' ) ) {
			$term_name = get_queried_object()->name;

			return "Veja abaixo receitas de {$term_name}";
		}

		if ( is_post_type_archive( 'fw-receitas' ) ) {
			$page_id = get_template_page_id( 'templates/recipes-home.php' );

			return get_the_title( $page_id );
		}

		if ( is_tax( 'fw-receita-ocasiao' ) ) {
			return get_queried_object()->name;
		}

		return get_the_title();
	}

}
