<?php declare( strict_types = 1 );

namespace App\Controllers\Web;

use App\Services\ProductService;
use WPEmerge;
use WPEmerge\Requests\Request;
use WPEmerge\View\PhpView;
use function App\get_zero_parent_id;

class ProductController {

	/**
	 * Product Service
	 *
	 * @var \App\Services\ProductService
	 */
	protected $product_service;

	public function __construct( ProductService $product_service ) {
		$this->product_service = $product_service;
	}

	/**
	 * Index.
	 *
	 * @param \WPEmerge\Requests\Request $request Request.
	 * @param string                     $view View.
	 * @SuppressWarnings("unused")
	 */
	public function index( Request $request, string $view ): PhpView {
		$term = get_queried_object();

		$term_id = null;
		$current_term = 0;

		if ( is_a( $term, 'WP_Term' ) ) {
			$term_id = get_zero_parent_id( $term );
			$current_term = $term->term_id;
		}

		return WPEmerge\view( $view )->with(
			[
				'products_category' => $this->product_service->get_products_archive( $term_id ?? 0 ),
				'current_term' => $current_term,
			]
		);
	}

	/**
	 * Singular page
	 *
	 * @param \WPEmerge\Requests\Request $request Request.
	 * @param string                     $view     View.
	 * @SuppressWarnings("unused")
	 */
	public function singular( Request $request, string $view ): PhpView {
		$ingredients = wpautop( \carbon_get_the_post_meta( 'ingredients' ) );

		return WPEmerge\view( $view )->with(
			[
				'ingredients' => $ingredients,
			]
		);
	}

	public function products_archive( Request $request, string $view ): PhpView {
		$product_type = $request->get( 'tipo-produto' ) ?? '';
		$products_category = $this->product_service->get_products_per_categories( $product_type );

		return WPEmerge\View( $view )->with(
			[
				'products_per_category' => $products_category,
			]
		);
	}

}
