<?php declare( strict_types = 1 );

namespace App\Controllers\RestApi;

use App\Services\RecipeService;

class RecipeController {

	/**
	 * Recipe service.
	 *
	 * @var \App\Services\RecipeService
	 */
	protected $recipe_service;

	public function __construct( RecipeService $recipe_service ) {
		$this->recipe_service = $recipe_service;
	}

	public function do_like( \WP_REST_Request $request ): \WP_REST_Response {
		try {
			$count = $this->recipe_service->do_like( $request->get_param( 'id' ) );

			return new \WP_REST_Response(
				[
					'data' => $count,
				]
			);
		} catch ( \Throwable $th ) {
			return new \WP_REST_Response(
				[
					'message' => $th->getMessage(),
					'data' => null,
				],
				500
			);
		}
	}

	public function remove_like( \WP_REST_Request $request ): \WP_REST_Response {
		try {
			$count = $this->recipe_service->remove_like( $request->get_param( 'id' ) );

			return new \WP_REST_Response(
				[
					'data' => $count,
				]
			);
		} catch ( \Throwable $th ) {
			return new \WP_REST_Response(
				[
					'message' => $th->getMessage(),
					'data' => null,
				],
				500
			);
		}
	}

}
