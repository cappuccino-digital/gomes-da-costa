<?php declare( strict_types = 1 );

namespace App\Controllers;

use App\Services\ProductCategoryService;
use App\Services\ProductService;
use App\Services\RecipeService;
use WPEmerge\ServiceProviders\ServiceProviderInterface;

/**
 * Service provider.
 *
 * @codeCoverageIgnore
 */
class ServiceProvider implements ServiceProviderInterface {

	/**
	 * {@inheritDoc}
	 *
	 * @param \Pimple\Container $container Container.
	 * @return void
	 */
	public function register( $container ): void {
		$container[ RestApi\RecipeController::class ] = static function( $container ) {
			return new RestApi\RecipeController( $container[ RecipeService::class ] );
		};

		$container[ Web\ProductController::class ] = static function( $container ) {
			return new Web\ProductController( $container[ ProductService::class ] );
		};

		$container[ Web\RecipeController::class ] = static function( $container ) {
			return new Web\RecipeController(
				$container[ RecipeService::class ],
				$container[ ProductCategoryService::class ]
			);
		};

		$container[ Web\RecipeTermsController::class ] = static function( $container ) {
			return new Web\RecipeTermsController(
				$container[ RecipeService::class ]
			);
		};
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param  \Pimple\Container $container Container.
	 * @return void
	 */
	public function bootstrap( $container ): void {
		// Do nothing.
	}

}
