<?php declare( strict_types = 1 );

namespace App\Walkers;

use function App\get_allowed_html_attributes_for_image;

/**
 * Custom walker class.
 *
 * @link https://developer.wordpress.org/reference/functions/wp_nav_menu/ .
 */
class CustomWalkerNavMenu extends \Walker_Nav_Menu {

	/**
	 * Start the element output.
	 *
	 * Adds main/sub-classes to the list items and links.
	 *
	 * @param string    $output  Passed by reference. Used to append additional content.
	 * @param \WP_Post  $item    Menu item data object.
	 * @param int       $depth   Depth of menu item. Used for padding.
	 * @param \stdClass $args    An object of wp_nav_menu() arguments.
	 * @param int       $item_id Current item ID.
	 * @SuppressWarnings("unused")
	 */
	public function start_el( &$output, $item, $depth = 0, $args = null, $item_id = 0 ): void { // phpcs:ignore
		$indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' );

		$classes = ! $item->classes
			? []
			: (array) $item->classes;
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

		$output .= $indent . '<li id="nav-menu-item-' . $item->ID . '" class="navbar__list-item ' . $class_names . '">';

		$attributes = $item->attr_title
			? ' title="' . esc_attr( $item->attr_title ) . '"'
			: '';
		$attributes .= $item->target
			? ' target="' . esc_attr( $item->target ) . '"'
			: '';
		$attributes .= $item->xfn
			? ' rel="' . esc_attr( $item->xfn ) . '"'
			: '';
		$attributes .= $item->url
			? ' href="' . esc_attr( $item->url ) . '"'
			: '';
		$attributes .= ' class="navbar__list-link menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

		$image = wp_get_attachment_image( carbon_get_nav_menu_item_meta( $item->ID, 'icon' ), 'full' );

		$item_output = sprintf(
			'%1$s<a%2$s>%3$s <div class="navbar__list-icon"><div class="navbar__list-icon-content">'
			. wp_kses( $image, get_allowed_html_attributes_for_image() ) .
			'</div></div><span class="navbar__list-text"> %4$s </span> %5$s</a>%6$s',
			$args->before,
			$attributes,
			$args->link_before,
			apply_filters( 'the_title', $item->title, $item->ID ),
			$args->link_after,
			$args->after
		);
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

}
