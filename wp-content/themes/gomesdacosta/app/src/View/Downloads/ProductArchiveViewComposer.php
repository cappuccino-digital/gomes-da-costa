<?php declare( strict_types = 1 );

namespace App\View\Downloads;

use App\Services\ProductService;

class ProductArchiveViewComposer {

	/**
	 * Product Service
	 *
	 * @var \App\Services\ProductService
	 */
	protected $product_service;

	public function __construct( ProductService $product_service ) {
		$this->product_service = $product_service;
	}

	public function compose(): void {
		// No variables to parse $view.
	}

}
