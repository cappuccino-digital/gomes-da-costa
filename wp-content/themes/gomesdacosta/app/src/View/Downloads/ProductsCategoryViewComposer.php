<?php declare( strict_types = 1 );

namespace App\View\Downloads;

use App\Services\ProductCategoryService;
use WPEmerge\View\PhpView;

class ProductsCategoryViewComposer {

	/**
	 * RecipeService
	 *
	 * @var \App\Services\ProductCategoryService
	 */
	protected $pcategory_service;

	public function __construct( ProductCategoryService $pcategory_service ) {
		$this->pcategory_service = $pcategory_service;
	}

	public function compose( PhpView $view ): void {
		$categories = $this->pcategory_service->get_product_category_by_id();

		$view->with(
			[
				'categories' => $categories,
				'products_link' => get_permalink(),
				'all_recipe_image' => wp_get_attachment_image( \carbon_get_theme_option( 'popular_recipes_all_image' ), 'thumbnail', false, [ 'class' => 'box__image-content' ] ),
			]
		);
	}

}
