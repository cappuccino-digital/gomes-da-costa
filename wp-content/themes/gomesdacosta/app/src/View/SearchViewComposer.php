<?php declare( strict_types = 1 );

namespace App\View;

use WPEmerge\View\PhpView;

class SearchViewComposer {

	public function compose( PhpView $view ): void {

		$search_value = get_search_query( true );

		$view->with(
			[
			'search' => $search_value,
			'count' => $this->get_totals( $search_value ),
			]
		);
	}

	// phpcs:disable
	private function get_totals( string $search ): array {
		global $wpdb;

		$search = "%$search%";


		$prepared_query = $wpdb->prepare(
			"
		SELECT
			todos.contagem AS todos,
			receitas.contagem AS receitas,
			produtos.contagem AS produtos
		FROM
			(
			SELECT
				COUNT(wp_posts.ID) AS 'contagem'
			FROM
				wp_posts
			WHERE
				1 = 1 AND(
					(
						(
							wp_posts.post_title LIKE '%s'
						) OR(
							wp_posts.post_excerpt LIKE '%s'
						) OR(
							wp_posts.post_content LIKE '%s'
						)
					)
				) AND wp_posts.post_type IN(
					'post',
					'page',
					'attachment',
					'fw-receitas',
					'fw-produtos'
				) AND(
					wp_posts.post_status = 'publish' OR wp_posts.post_author = 1 AND wp_posts.post_status = 'private'
				)
		) AS todos,
		(
			SELECT
				COUNT(wp_posts.ID) AS 'contagem'
			FROM
				wp_posts
			WHERE
				1 = 1 AND(
					(
						(
							wp_posts.post_title LIKE '%s'
						) OR(
							wp_posts.post_excerpt LIKE '%s'
						) OR(
							wp_posts.post_content LIKE '%s'
						)
					)
				) AND wp_posts.post_type IN('fw-receitas') AND(
					wp_posts.post_status = 'publish' OR wp_posts.post_author = 1 AND wp_posts.post_status = 'private'
				)
		) AS receitas,
		(
			SELECT
				COUNT(wp_posts.ID) AS 'contagem'
			FROM
				wp_posts
			WHERE
				1 = 1 AND(
					(
						(
							wp_posts.post_title LIKE '%s'
						) OR(
							wp_posts.post_excerpt LIKE '%s'
						) OR(
							wp_posts.post_content LIKE '%s'
						)
					)
				) AND wp_posts.post_type IN('fw-produtos') AND(
					wp_posts.post_status = 'publish' OR wp_posts.post_author = 1 AND wp_posts.post_status = 'private'
				)
		) AS produtos
		",
			[
			$search,
			$search,
			$search,
			$search,
			$search,
			$search,
			$search,
			$search,
			$search,
			 ]
		);

		return $wpdb->get_row( $prepared_query, ARRAY_A );
		// phpcs:enable
	}

}

