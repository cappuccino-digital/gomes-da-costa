<?php declare( strict_types = 1 );

namespace App\View;

use WPEmerge\View\PhpView;

class NewsletterFormViewComposer {

	public function compose( PhpView $view ): void {
		$view->with(
			[
				'form_id' => carbon_get_theme_option( 'newsletter_form' ),
			]
		);
	}

}

