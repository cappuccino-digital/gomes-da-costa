<?php declare( strict_types = 1 );

namespace App\View;

use WPEmerge\View\PhpView;

class PaginationViewComposer {

	public function compose( PhpView $view ): void {
		global $wp_query;

		$max_num_pages = $wp_query->max_num_pages;
		$current_page = (int) get_query_var( 'paged' );

		$view->with(
			[
				'firsts_pages' => 3 >= $current_page,
				'lasts_pages' => $current_page > $max_num_pages - 3,
				'show_first_ellipsis' => 4 >= $current_page,
				'show_last_ellipsis' => $current_page > $max_num_pages - 4,
			]
		);
	}

}
