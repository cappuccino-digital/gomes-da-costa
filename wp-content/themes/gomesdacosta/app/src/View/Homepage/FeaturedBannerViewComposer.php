<?php declare( strict_types = 1 );

namespace App\View\Homepage;

use App\Services\RecipeService;
use WPEmerge\View\PhpView;

class FeaturedBannerViewComposer {

	/**
	 * RecipeService
	 *
	 * @var \App\Services\RecipeService
	 */
	protected $recipe_service;

	public function __construct( RecipeService $recipe_service ) {
		$this->recipe_service = $recipe_service;
	}

	public function compose( PhpView $view ): void {

		$view->with(
			[
				'banners' => $this->recipe_service->get_home_featured_banner_parameters(),
			]
		);
	}

}
