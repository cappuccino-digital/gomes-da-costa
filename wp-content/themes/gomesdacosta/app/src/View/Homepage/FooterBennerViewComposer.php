<?php declare( strict_types = 1 );

namespace App\View\Homepage;

use WPEmerge\View\PhpView;
use function App\get_image_alt;

class FooterBennerViewComposer {

	public function compose( PhpView $view ): void {
		$banner = carbon_get_theme_option( 'footer_banner' );
		$banner_desktop = carbon_get_theme_option( 'footer_banner_desktop' );

		$image_alt = get_image_alt( (int) $banner ?? (int) $banner_desktop );

		$view->with(
			[
				'image_sizes' => array_filter(
					[
						'mobile' => $banner ? wp_get_attachment_image_url( $banner, 'full' ) : null,
						'desktop' => $banner_desktop ? wp_get_attachment_image_url( $banner_desktop, 'full' ) : null,
					]
				),
				'alt' => $image_alt,
				'link' => carbon_get_theme_option( 'footer_banner_link' ),
				'new_tab' => carbon_get_theme_option( 'footer_banner_new_tab' ),
			]
		);
	}

}
