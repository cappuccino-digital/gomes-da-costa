<?php declare( strict_types = 1 );

namespace App\View\Homepage;

use App\Services\SocialLinksService;
use WPEmerge\View\PhpView;
use function App\get_image_alt;

class SocialSectionViewComposer {

	public function compose( PhpView $view ): void {
		$view->with(
			[
				'social_medias' => $this->get_social_media_parameters(),
			]
		);
	}

	/**
	 * Get social media parameters
	 *
	 * @return array<string>
	 */
	protected function get_social_media_parameters(): array {
		$medias = carbon_get_theme_option( 'social_medias' );

		return array_filter(
			array_map(
				static function( $item ) {
					if ( count( $item['cta_media'] ) > 0 ) {
						$cta_media = $item['cta_media'][0];
					}

					$social = SocialLinksService::get_params_by_url( $item['cta_link'] );
					$item['id'] = $social['id'];

					if ( $cta_media['_type'] === 'image' ) {
						$image_alt = get_image_alt( (int) $cta_media['desktop'] ?? (int) $cta_media['mobile'] );

						$item['cta_media'] = [
							'_type' => 'picture',
							'parameters' => [
								'image_sizes' => [
									'mobile' => wp_get_attachment_image_url( $cta_media['mobile'], 'medium-card' ),
									'desktop' => wp_get_attachment_image_url( $cta_media['desktop'], 'medium-card' ),
								],
								'picture_class' => 'box__image-picture',
								'image_class' => 'unset-sizes',
								'alt' => $image_alt,
							],
						];
					} else {
						$item['cta_media'] = [
								'_type' => 'video',
								'parameters' => [
									'video' => $cta_media['video'],
								],
							];
					}

					return $item;
				},
				$medias
			)
		);
	}

}
