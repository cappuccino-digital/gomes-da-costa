<?php declare( strict_types = 1 );

namespace App\View\Homepage;

use App\Services\ProductCategoryService;
use WPEmerge\View\PhpView;
use function App\get_template_page_id;

class ProductSectionViewComposer {

	/**
	 * Product Category Service
	 *
	 * @var \App\Services\ProductCategoryService
	 */
	protected $p_category_service;

	public function __construct( ProductCategoryService $p_category_service ) {
		$this->p_category_service = $p_category_service;
	}

	public function compose( PhpView $view ): void {

		$view->with(
			[
				'product_types' => $this->p_category_service->get_product_category(),
				'products_link' => get_permalink( get_template_page_id( 'templates/products-home.php' ) ),
			]
		);
	}

}
