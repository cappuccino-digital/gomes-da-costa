<?php declare( strict_types = 1 );

namespace App\View\Recipes;

use App\Services\RecipeService;
use WPEmerge\View\PhpView;

class MetadataViewComposer {

	/**
	 * Recipe Service
	 *
	 * @var \App\Services\RecipeService
	 */
	protected $recipe_service;

	public function __construct( RecipeService $recipe_service ) {
		$this->recipe_service = $recipe_service;
	}

	public function compose( PhpView $view ): void {
		$view->with(
			$this->recipe_service->get_metadata_parameters_by_id( get_the_id() )
		);
	}

}
