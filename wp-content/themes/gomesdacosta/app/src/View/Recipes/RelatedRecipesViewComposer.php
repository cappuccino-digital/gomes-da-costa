<?php declare( strict_types = 1 );

namespace App\View\Recipes;

use App\Services\RecipeService;
use WPEmerge\View\PhpView;

class RelatedRecipesViewComposer {

	/**
	 * RecipeService
	 *
	 * @var \App\Services\RecipeService
	 */
	protected $recipe_service;

	public function __construct( RecipeService $recipe_service ) {
		$this->recipe_service = $recipe_service;
	}

	public function compose( PhpView $view ): void {

		$limit = (int) carbon_get_theme_option( 'related_recipes_count' );

		if ( $limit === 0 ) {
			$limit = 10;
		}

		$recipes = $this->recipe_service->get_random_related( (int) get_the_ID(), $limit );

		$view->with(
			[
				'recipes' => $recipes,
			]
		);
	}

}
