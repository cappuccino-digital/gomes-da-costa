<?php declare( strict_types = 1 );

namespace App\View\Recipes;

use WPEmerge\View\PhpView;
use function App\get_current_search_link;

class HomeHeaderViewComposer {

	public function compose( PhpView $view ): void {
		$view->with(
			[
				'recipes_link' => get_current_search_link(),
			]
		);
	}

}
