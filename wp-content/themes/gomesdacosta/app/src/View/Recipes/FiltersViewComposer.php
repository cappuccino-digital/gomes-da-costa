<?php declare( strict_types = 1 );

namespace App\View\Recipes;

use WPEmerge\View\PhpView;

class FiltersViewComposer {

	public function compose( PhpView $view ): void {
		$view->with(
			[
				'time' => self::get_time(),
				'portion' => self::get_portions(),
				'recipes_link' => get_post_type_archive_link( 'fw-receitas' ),
			]
		);
	}

	/**
	 * Get time filter values
	 *
	 * @return array<string>
	 */
	private static function get_time(): array {
		$times = [];

		for ( $index = 15; $index <= 45; $index += 15 ) {
			$times[] = [
				'unit' => 'min',
				'value' => (string) $index,
				'label' => (string) $index,
			];
		}

		$times[] = [
			'unit' => 'hora',
			'value' => '60',
			'label' => '1',
		];

		$times[] = [
			'unit' => 'hora',
			'value' => 's60',
			'label' => '+1',
		];

		return $times;
	}

	/**
	 * Get portions filter values
	 *
	 * @return array<string>
	 */
	private static function get_portions(): array {
		$portions = [];

		for ( $index = 1; $index <= 46; $index += 5 ) {
			$max = $index + 4;
			$portions[] = "{$index}|{$max}";
		}

		$portions[] = '+50';

		return $portions;
	}

}
