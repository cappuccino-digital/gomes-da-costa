<?php declare( strict_types = 1 );

namespace App\View\Recipes;

use WPEmerge\View\PhpView;

class LikeViewComposer {

	public function compose( PhpView $view ): void {
		$view->with(
			[
				'nonce' => wp_create_nonce( 'do_like_recipe' ),
				'recipe_id' => get_the_id(),
			]
		);
	}

}
