<?php declare( strict_types = 1 );

namespace App\View\Recipes;

use App\Services\RecipeService;
use WPEmerge\View\PhpView;

class CategoryViewComposer {

	/**
	 * RecipeService
	 *
	 * @var \App\Services\RecipeService
	 */
	protected $recipe_service;

	public function __construct( RecipeService $recipe_service ) {
		$this->recipe_service = $recipe_service;
	}

	public function compose( PhpView $view ): void {
		$categories = $this->recipe_service->get_home_product_categories();
		$special_terms = $this->recipe_service->get_special_recipes();

		$view->with(
			[
				'categories' => array_merge( $categories, $special_terms ),
				'recipes_link' => get_post_type_archive_link( 'fw-receitas' ),
				'all_recipe_image' => wp_get_attachment_image( \carbon_get_theme_option( 'popular_recipes_all_image' ), 'thumbnail', false, [ 'class' => 'box__image-content' ] ),
			]
		);
	}

}
