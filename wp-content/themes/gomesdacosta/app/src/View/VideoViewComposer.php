<?php declare( strict_types = 1 );

namespace App\View;

use WPEmerge\View\PhpView;

class VideoViewComposer {

	public function compose( PhpView $view ): void {
		$view->with(
			[
				'video' => carbon_get_post_meta( get_the_id(), 'crb_oembed' ),
			]
		);
	}

}
