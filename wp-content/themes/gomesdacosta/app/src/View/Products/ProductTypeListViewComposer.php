<?php declare( strict_types = 1 );

namespace App\View\Products;

use App\Services\ProductService;
use WPEmerge\View\PhpView;

class ProductTypeListViewComposer {

	/**
	 * Product Service
	 *
	 * @var \App\Services\ProductService
	 */
	protected $product_service;

	public function __construct( ProductService $product_service ) {
		$this->product_service = $product_service;
	}

	public function compose( PhpView $view ): void {
		$view->with(
			[
				'categories' => $this->product_service->get_selected_product_categories(),
				'product_link' => get_post_type_archive_link( 'fw-produtos' ),
			]
		);
	}

}
