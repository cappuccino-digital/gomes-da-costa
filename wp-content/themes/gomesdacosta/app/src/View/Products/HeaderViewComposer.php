<?php declare( strict_types = 1 );

namespace App\View\Products;

use WPEmerge\View\PhpView;
use function App\get_template_page_id;
use function App\get_zero_parent_id;

class HeaderViewComposer {

	public function compose( PhpView $view ): void {
		$view->with(
			[
				'thumbnail' => $this->get_post_thumbnail(),
				'title' => $this->get_title(),
				'products_link' => get_post_type_archive_link( 'fw-produtos' ),
			]
		);
	}

	/**
	 * Get thumbnail's page
	 *
	 * @return array<string>
	 */
	private function get_post_thumbnail(): array {
		$term = get_queried_object();

		if ( is_a( $term, 'WP_Term' ) ) {
			$parent_term = get_term( get_zero_parent_id( $term ) );

			return [
				'image_sizes' => [
					'desktop' => wp_get_attachment_image_url( carbon_get_term_meta( $parent_term->term_id, 'large' ), 'full' ),
					'mobile' => wp_get_attachment_image_url( carbon_get_term_meta( $parent_term->term_id, 'large' ), 'medium_large' ),
				],
				'image_class' => 'attachment-post-thumbnail size-post-thumbnail wp-post-image',
				'picture_class' => '',
				'alt' => $parent_term->name,
			];
		}

		$page_id = get_template_page_id( 'templates/products-home.php' );

		return [
			'image_sizes' => [
				'desktop' => get_the_post_thumbnail_url( $page_id, 'recipe-single' ),
				'mobile' => get_the_post_thumbnail_url( $page_id, 'medium_large' ),
			],
			'image_class' => 'attachment-post-thumbnail size-post-thumbnail wp-post-image',
			'picture_class' => '',
			'alt' => get_the_title( $page_id ),
		];
	}

	/**
	 * Get page title.
	 */
	private function get_title(): string {
		$term = get_queried_object();

		if ( is_a( $term, 'WP_Term' ) ) {
			$term_name = ucwords( strtolower( $term->name ) );

			return "{$term_name} Gomes da Costa";
		}

		return get_the_title( get_template_page_id( 'templates/products-home.php' ) );
	}

}
