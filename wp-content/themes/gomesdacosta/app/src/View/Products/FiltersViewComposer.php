<?php declare( strict_types = 1 );

namespace App\View\Products;

use App\Services\ProductService;
use WPEmerge\View\PhpView;

class FiltersViewComposer {

	/**
	 * Product Service
	 *
	 * @var \App\Services\ProductService
	 */
	protected $product_service;

	public function __construct( ProductService $product_service ) {
		$this->product_service = $product_service;
	}

	public function compose( PhpView $view ): void {
		$view->with(
			[
				'filters' => $this->product_service->get_product_categories(),
			]
		);
	}

}
