<?php declare( strict_types = 1 );

namespace App\View\Products;

use App\Services\ProductService;
use WPEmerge\View\PhpView;

class RelatedRecipesViewComposer {

	/**
	 * ProductService
	 *
	 * @var \App\Services\RecipeService
	 */
	protected $product_service;

	public function __construct( ProductService $product_service ) {
		$this->product_service = $product_service;
	}

	public function compose( PhpView $view ): void {

		$limit = (int) carbon_get_theme_option( 'related_product_count' );

		if ( $limit === 0 ) {
			$limit = 10;
		}

		$recipes = $this->product_service->get_random_related( get_the_ID(), $limit );

		$view->with(
			[
				'recipes' => $recipes,
			]
		);
	}

}
