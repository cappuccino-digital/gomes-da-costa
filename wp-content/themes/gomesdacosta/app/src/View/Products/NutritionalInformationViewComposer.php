<?php declare( strict_types = 1 );

namespace App\View\Products;

use App\Services\ProductService;
use WPEmerge\View\PhpView;

class NutritionalInformationViewComposer {

	/**
	 * Product Service
	 *
	 * @var \App\Services\ProductService
	 */
	protected $product_service;

	public function __construct( ProductService $product_service ) {
		$this->product_service = $product_service;
	}

	public function compose( PhpView $view ): void {
		$view->with( $this->product_service->get_nutritional_information_by_id( get_the_ID() ) );
	}

}
