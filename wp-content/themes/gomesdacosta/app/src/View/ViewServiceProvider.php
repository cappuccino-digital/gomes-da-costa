<?php declare( strict_types = 1 );

namespace App\View;

use App\Services\ProductCategoryService;
use App\Services\ProductService;
use App\Services\RecipeService;
use App\View\Downloads\ProductArchiveViewComposer;
use App\View\Downloads\ProductsCategoryViewComposer;
use App\View\Homepage\FeaturedBannerViewComposer;
use App\View\Homepage\HeaderViewComposer as HomepageHeaderViewComposer;
use App\View\Homepage\ProductSectionViewComposer;
use App\View\Products\FiltersViewComposer;
use App\View\Products\MetadataViewComposer as ProductMetadataViewComposer;
use App\View\Products\NutritionalInformationViewComposer;
use App\View\Products\ProductTypeListViewComposer;
use App\View\Products\RelatedRecipesViewComposer as ProductRelated;
use App\View\Recipes\CategoryViewComposer;
use App\View\Recipes\HeaderViewComposer;
use App\View\Recipes\MetadataViewComposer as RecipeMetadataViewComposer;
use App\View\Recipes\PopularRecipesViewComposer;
use App\View\Recipes\RelatedRecipesViewComposer as RecipeRelated;
use App\View\Recipes\SpecialRecipesViewComposer;
use App\View\Recipes\UsedProductsViewComposer;
use WPEmerge\ServiceProviders\ServiceProviderInterface;

/**
 * Provides custom route conditions.
 * This is an example class so feel free to modify or remove it.
 *
 * @SuppressWarnings("couplingbetweenobjects")
 */
class ViewServiceProvider implements ServiceProviderInterface {

	/**
	 * {@inheritDoc}
	 *
	 * @param \Pimple\Container $container Container.
	 * @return void
	 */
	public function register( $container ) {
		$container[HeaderViewComposer::class] = static function( $container ) {
			return new HeaderViewComposer( $container[RecipeService::class] );
		};
		$container[NutritionalInformationViewComposer::class] = static function( $container ) {
			return new NutritionalInformationViewComposer(
				$container[ProductService::class]
			);
		};
		$container[ProductMetadataViewComposer::class] = static function( $container ) {
			return new ProductMetadataViewComposer(
				$container[ProductService::class]
			);
		};
		$container[ProductSectionViewComposer::class] = static function( $container ) {
			return new ProductSectionViewComposer(
				$container[ProductCategoryService::class]
			);
		};
		$container[RecipeRelated::class] = static function( $container ) {
			return new RecipeRelated(
				$container[RecipeService::class]
			);
		};
		$container[SpecialRecipesViewComposer::class] = static function( $container ) {
			return new SpecialRecipesViewComposer(
				$container[RecipeService::class]
			);
		};
		$container[FeaturedBannerViewComposer::class] = static function( $container ) {
			return new FeaturedBannerViewComposer(
				$container[RecipeService::class]
			);
		};
		$container[HomepageHeaderViewComposer::class] = static function( $container ) {
			return new HomepageHeaderViewComposer(
				$container[RecipeService::class]
			);
		};
		$container[ProductRelated::class] = static function( $container ) {
			return new ProductRelated(
				$container[ProductService::class]
			);
		};
		$container[FiltersViewComposer::class] = static function( $container ) {
			return new FiltersViewComposer(
				$container[ProductService::class]
			);
		};
		$container[ProductTypeListViewComposer::class] = static function( $container ) {
			return new ProductTypeListViewComposer(
				$container[ProductService::class]
			);
		};
		$container[PopularRecipesViewComposer::class] = static function( $container ) {
			return new PopularRecipesViewComposer(
				$container[RecipeService::class]
			);
		};
		$container[CategoryViewComposer::class] = static function( $container ) {
			return new CategoryViewComposer(
				$container[RecipeService::class]
			);
		};
		$container[HomeHeaderViewComposer::class] = static function() {
			return new HomeHeaderViewCompose;
		};
		$container[UsedProductsViewComposer::class] = static function( $container ) {
			return new UsedProductsViewComposer(
				$container[RecipeService::class]
			);
		};
		$container[RecipeMetadataViewComposer::class] = static function( $container ) {
			return new RecipeMetadataViewComposer(
				$container[RecipeService::class]
			);
		};
		$container[ProductArchiveViewComposer::class] = static function( $container ) {
			return new ProductArchiveViewComposer(
				$container[ProductService::class]
			);
		};
		$container[ProductsCategoryViewComposer::class] = static function( $container ) {
			return new ProductsCategoryViewComposer(
				$container[ProductCategoryService::class]
			);
		};
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param \Pimple\Container $container Container.
	 * @return void
	 */
	public function bootstrap( $container ) { // phpcs:ignore
		require_once APP_APP_DIR . 'views.php';
	}

}
