<?php declare( strict_types = 1 );

namespace App\View;

use WPEmerge\View\PhpView;
use WPEmergeTheme\Facades\Config;

class PictureViewComposer {

	public function compose( PhpView $view ): void {
		$view->with(
			[
				'mobile_width' => Config::get( 'variables.breakpoint.mobile', '575.98px' ),
			]
		);
	}

}
