<?php declare( strict_types = 1 );

namespace App\View;

use App\Services\SocialLinksService;
use WPEmerge\View\PhpView;

class SocialMediaViewComposer {

	public function compose( PhpView $view ): void {

		$items = array_filter(
			array_map(
				static function( $item ): ?array {
					$params = SocialLinksService::get_params_by_url( $item['link'] );

					if ( ! $params ) {
						return null;
					}

					return array_merge(
						$params,
						[
							'url' => $item['link'],
						]
					);
				},
				carbon_get_theme_option( 'footer_social_items' )
			)
		);

		$view->with(
			[
				'footer_social_text' => carbon_get_theme_option( 'footer_social_text' ),
				'footer_social_items' => $items,
			]
		);
	}

}

