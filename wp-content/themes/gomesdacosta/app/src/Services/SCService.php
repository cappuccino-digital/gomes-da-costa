<?php declare( strict_types = 1 );

namespace App\Services;

class SCService {

	/**
	 * SCService api gateway.
	 *
	 * @var string
	 */
	protected $api_gateway;

	/**
	 * SCService uri
	 *
	 * @var string
	 */
	protected $uri = 'https://visualsc.com.br/api/';

	/**
	 * SCService username api
	 *
	 * @var string
	 */
	protected $username = 'sac.gomesdacosta';

	/**
	 * SCService password api
	 *
	 * @var string
	 */
	protected $password = '$C4195@';

	/**
	 * SCService client mode api
	 *
	 * @var string
	 */
	protected $client = 'gomesdacosta';

	public function __construct( ApiGateway $api_gateway ) {
		$this->api_gateway = $api_gateway;
	}

	/**
	 * Get data by ID.
	 *
	 * @param array<string> $post_values Values of Post.
	 */
	public function send_email_by_form( array $post_values ): void {
		try {
			$token = $this->get_token();
			$service_emails = $this->list_service_emails( $token );

			if ( ! ( $service_emails && $this->api_gateway && $post_values ) ) {
				return;
			}

			$uri = $this->uri . 'email/faleconosco';
			$email = $service_emails['emailatendimento'];
			$ddd = '';
			$phone = $post_values[4];
			$full_number = explode( ' ', $post_values[4] );

			if ( $full_number ) {
				$ddd = preg_replace( '/[^0-9]+/', '', $full_number[0] );
				$phone = $full_number[1];
			}

			$body = [
				'cliente' => $this->client,
				'nome' => $post_values[1],
				'ddd' => $ddd,
				'telefone' => $phone,
				'emailconsumidor' => $post_values[2],
				'emailassunto' => $post_values[5],
				'emailmensagem' => $post_values[6],
				'emailatendimento' => $email,
			];
			$options = [
				'headers' => [
					'Authorization' => 'bearer ' . $token,
					'Content-Type' => 'application/json; charset=UTF-8',
				],
				'body' => wp_json_encode( $body ),
			];
			$this->api_gateway->request(
				'POST',
				$uri,
				$options
			);
		} catch ( \Throwable $t ) {
			return;
		}
	}

	/**
	 * Get data by ID.
	 *
	 * @return string.
	 */
	public function get_token(): string {
		if ( ! $this->api_gateway ) {
			return '';
		}

		$body = 'username=' . $this->username . '&';
		$body .= 'password=' . $this->password . '&';
		$body .= 'grant_type=password';
		$options = [
			'headers' => ['application/x-www-form-urlencoded'],
			'body' => $body,
		];

		$response = $this->api_gateway->request(
			'POST',
			$this->uri . 'token',
			$options
		);

		$response_json = json_decode( $response, true );

		if ( count( $response_json ) ) {
			return $response_json['access_token'];
		}

		return '';
	}

	/**
	 * Get data by ID.
	 *
	 * @param string $token Token of api.
	 * @return array<string>
	 */
	public function list_service_emails( string $token ): array {
		if ( ! $this->api_gateway || ! $token ) {
			return null;
		}

		$uri = $this->uri . 'email/configuracoes/' . $this->client;
		$options = [
			'headers' => ['Authorization' => 'bearer ' . $token],
		];
		$response = $this->api_gateway->request(
			'GET',
			$uri,
			$options
		);
		$response_json = json_decode( $response, true );

		if ( count( $response_json ) ) {
			return $response_json[0];
		}

		return null;
	}

}
