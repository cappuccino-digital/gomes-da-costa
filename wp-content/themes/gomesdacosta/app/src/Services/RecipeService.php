<?php declare( strict_types = 1 );

namespace App\Services;

use function App\get_image_alt;
use function App\get_template_page_id;

class RecipeService {

	/**
	 * ProductService.
	 *
	 * @var \App\Services\ProductService
	 */
	protected $product_service;

	/**
	 * Product Category Service
	 *
	 * @var \App\Services\ProductCategoryService]
	 */
	protected $pcategory_service;

	public function __construct( ProductService $product_service, ProductCategoryService $pcategory_service ) {
		$this->product_service = $product_service;
		$this->pcategory_service = $pcategory_service;
	}

	/**
	 * Get header parameters by recipe id.
	 *
	 * @param int $post_id Recipe id.
	 * @return array<string>
	 */
	public function get_header_parameters_by_id( int $post_id ): array {
		return [
			'likes' => (int) get_post_meta( $post_id, 'post-likes', true ),
			'comments' => get_comments_number( $post_id ),
			'video' => carbon_get_post_meta( $post_id, 'crb_oembed' ),
		];
	}

	/**
	 * Get home product categories.
	 *
	 * @return array<string>
	 */
	public function get_home_product_categories(): array {
		return array_filter(
			array_map(
				function( $item ) {
					return $this->pcategory_service->get_data_by_id( (int) $item['id'] );
				},
				carbon_get_theme_option( 'recipe_product_category' )
			)
		);
	}

	/**
	 * Get random recipes
	 *
	 * @param int    $limit Limit.
	 * @param bool   $is_random Codition for random.
	 * @param int    $page Number of Page.
	 * @param string $search Name of recipe.
	 * @param string $product_type Product Type.
	 * @return array<\WP_Post>
	 */
	public function get_recipes(
		int $limit,
		bool $is_random = false,
		int $page = 1,
		string $search = '',
		string $product_type = ''
	): array {
		global $wpdb;

		$product_type_id = $this->get_product_type_id( $product_type );

		$wild = '%';

		$like_slug = $product_type;

		if ( ! $product_type ) {
			$find = $product_type;
			$like_slug = $wild . $wpdb->esc_like( $find ) . $wild;
		}

		$like_search = $wild . $wpdb->esc_like( $search ) . $wild;
		// phpcs:ignore
		$posts = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT  DISTINCT wpp.post_title, wpp.* FROM {$wpdb->prefix}posts wpp 
				LEFT JOIN {$wpdb->prefix}term_relationships wptr ON wptr.object_id = wpp.ID
				LEFT JOIN {$wpdb->prefix}term_taxonomy wptt ON wptt.term_taxonomy_id = wptr.term_taxonomy_id
				LEFT JOIN {$wpdb->prefix}terms wpt ON wpt.term_id = wptt.term_id 
				WHERE wpp.post_type = 'fw-receitas' && 
					(wptt.taxonomy = 'fw-produto-category' || wptt.taxonomy = 'fw-receita-ocasiao') &&
					(wpt.slug LIKE %s || wptt.parent = %s) && 
					wpp.post_status = 'publish' &&
					wpp.post_title LIKE %s
				ORDER BY wpp.post_modified DESC;",
				$like_slug,
				$product_type_id,
				$like_search
			)
		);

		if ( $is_random ) {
			shuffle( $posts );
		}

		$recipes_random_posts = array_chunk( $posts, $limit );

		if ( ! $recipes_random_posts ) {
			return [];
		}

		return $recipes_random_posts[$page - 1];
	}

	/**
	 * Get id at product type
	 *
	 * @param string $product_type Producty type.
	 */
	public function get_product_type_id( string $product_type ): string {
		global $wpdb;

		if ( ! $product_type ) {
			return '';
		}

		// phpcs:ignore
		$results = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT * FROM {$wpdb->prefix}terms wpt WHERE wpt.slug = %s",
				$product_type
			)
		);

		if ( ! count( $results ) ) {
			return '';
		}

		return $results[0]->term_id;
	}

	/**
	 * Process recipes
	 *
	 * @param \WP_Term $term Terms to process.
	 * @return array<string>
	 */
	public function get_term_data( \WP_Term $term ): array {
		return [
			'name' => $term->name,
			'slug' => $term->slug,
			'link' => get_term_link( $term ),
			'color' => carbon_get_term_meta( $term->term_id, 'color' ),
			'taxonomy' => $term->taxonomy,
			'image-sizes' => [
				'thumbnail' => wp_get_attachment_image( carbon_get_term_meta( $term->term_id, 'thumbnail' ), 'thumbnail' ),
				'icon-card' => wp_get_attachment_image( \carbon_get_term_meta( $term->term_id, 'thumbnail' ), 'icon-card' ),
			],
		];
	}

	/**
	 * Get term data by id
	 *
	 * @param int $term_id Term to process.
	 * @return array<string>
	 */
	public function get_term_data_by_id( int $term_id ): ?array {
		$term = get_term( $term_id );

		if ( is_a( $term, 'WP_Term' ) ) {
			return $this->get_term_data( $term );
		}

		return null;
	}

	/**
	 * Get special recipes.
	 *
	 * @return array<string>
	 */
	public function get_special_recipes(): array {
		$terms = \carbon_get_theme_option( 'special_recipe' );

		return array_filter(
			array_map(
				function( $item ) {
					return $this->get_term_data_by_id( (int) $item['id'] );
				},
				$terms
			)
		);
	}

	/**
	 * Get home featured banner parameters.
	 *
	 * @return array<string>
	 */
	public function get_home_featured_banner_parameters(): array {
		$home_banner = carbon_get_theme_option( 'home_banner' );

		return array_map(
			static function( $item ) {
				$cta_link = $item['cta_link'];
				$cta_text = $item['cta_text'];
				$new_tab = $item['new_tab'];

				$image_alt = get_image_alt( (int) $item['featured_banner'] ?? (int) $item['featured_banner_mobile'] );

				return [
					'image-sizes' => array_filter(
						[
							'desktop' => isset( $item['featured_banner'] ) ? wp_get_attachment_image_url( $item['featured_banner'], 'full' ) : false,
							'mobile' => isset( $item['featured_banner_mobile'] ) ? wp_get_attachment_image_url( $item['featured_banner_mobile'], 'full' ) : false,
						]
					),
					'alt' => $image_alt,
					'cta_link' => $cta_link,
					'cta_text' => $cta_text,
					'show_cta' => $cta_text && $cta_link,
					'new_tab' => $new_tab,
				];
			},
			$home_banner
		);
	}

	/**
	 * Get home recipes.
	 *
	 * @return array<string>
	 */
	public function get_home_recipes(): array {
		$recipes_count_mobile = (int) carbon_get_theme_option( 'home_recipes_count_mobile' );
		$recipes_count_desk = (int) carbon_get_theme_option( 'home_recipes_count_desktop' );

		return [
			'recipes_count_mobile' => $recipes_count_mobile,
			'recipes_count_desktop' => $recipes_count_desk,
			'recipes' => $this->get_recipes( $recipes_count_mobile > $recipes_count_desk ? $recipes_count_mobile : $recipes_count_desk, true ),
			'recipes_link' => get_permalink( get_template_page_id( 'templates/recipes-home.php' ) ),
		];
	}

	/**
	 * Get metadata parameter by recipe id.
	 *
	 * @param int $post_id Recipe id.
	 * @return array<string>
	 */
	public function get_metadata_parameters_by_id( int $post_id ): array {
		return [
			'total_time' => carbon_get_post_meta( $post_id, 'time' ),
			'yields' => carbon_get_post_meta( $post_id, 'yield' ),
		];
	}

	/**
	 * Get Structured Data
	 *
	 * @param array<string> $data Default recipe data.
	 * @param int           $post_id Recipe id.
	 * @return array<string>
	 */
	public function get_structured_data( array $data, int $post_id ): array {
		$metadata = $this->get_metadata_parameters_by_id( $post_id );
		$instructions = $this->text_to_array( $data['steps'] );

		$structured_data = [
			'@context' => 'https://schema.org/',
			'@type' => 'Recipe',
			'name' => get_the_title( $post_id ),
			'keywords' => strtolower( get_the_title( $post_id ) ),
			'url' => get_the_permalink( $post_id ),
			'description' => get_the_title( $post_id ),
			'image' => esc_url( get_the_post_thumbnail_url( $post_id ) ),
			'datePublished' => get_the_date( 'Y-m-d', $post_id ),
			'totalTime' => "PT{$metadata['total_time']}M",
			'prepTime' => 'PT0M',
			'recipeYield' => (int) $metadata['yields'],
			'recipeIngredient' => array_values( $this->text_to_array( $data['ingredients'] ) ),
			'recipeInstructions' => array_values(
				array_map(
					static function( $current ) {
						return [
						'type' => 'HowToStep',
						'text' => $current,
						];
					},
					$instructions
				)
			),
		];
		$terms = get_the_terms( $post_id, 'fw-produto-category' );

		if ( $terms && ! is_wp_error( $terms ) && count( $terms ) > 0 ) {
			$structured_data['recipeCategory'] = $terms[0]->name;
		}

		return $structured_data;
	}

	/**
	 * Get data by ID.
	 *
	 * @param int $post_id Post ID.
	 * @return array<string>
	 */
	public function get_data_by_id( int $post_id ): array {
		$data = [
			'used_products' => $this->get_used_products( $post_id ),
			'ingredients' => carbon_get_post_meta( $post_id, 'ingredients' ),
			'steps' => carbon_get_post_meta( $post_id, 'steps' ),
			'chef_clue' => carbon_get_post_meta( $post_id, 'chef_clue' ),
			'variation' => carbon_get_post_meta( $post_id, 'variation' ),
			'nutritional_information' => carbon_get_post_meta( $post_id, 'nutritional_information' ),
		];

		return array_merge(
			$data,
			[
				'structured_data' => $this->get_structured_data( $data, $post_id ),
			]
		);
	}

	/**
	 * Do like.
	 *
	 * @param int $post_id Post ID.
	 */
	public function do_like( int $post_id ): int {
		$likes = (int) get_post_meta( $post_id, 'post-likes', true );
		$likes += 1;
		update_post_meta( $post_id, 'post-likes', $likes );

		return $likes;
	}

	/**
	 * Remove like.
	 *
	 * @param int $post_id Post ID.
	 */
	public function remove_like( int $post_id ): int {
		$likes = (int) get_post_meta( $post_id, 'post-likes', true );
		$likes -= 1;
		update_post_meta( $post_id, 'post-likes', $likes );

		return $likes;
	}

	/**
	 * Get related query.
	 *
	 * @param int $post_id Post ID.
	 * @param int $limit Limit.
	 */
	public function get_related_query( int $post_id, int $limit ): \WP_Query {
		$args = [
			'post_type' => 'fw-receitas',
			'posts_per_page' => $limit,
		];

		if ( $post_id ) {
			$collection = $this->get_term_collection_by_id( $post_id );
			$args['post__not_in'] = [$post_id];
			$args['tax_query'] = [ //phpcs:ignore
				[
					'taxonomy' => 'fw-receita-colecao',
					'field' => 'term_id',
					'terms' => $collection ? $collection->term_id : 0,
				],
			];
		}

		return new \WP_Query( $args );
	}

	/**
	 * Get popular query.
	 *
	 * @param int $limit Limit.
	 */
	public function get_popular_query( int $limit ): \WP_Query {
		return new \WP_Query(
			[
				'post_type' => 'fw-receitas',
				'posts_per_page' => $limit,
				'orderby' => 'meta_value_num',
				'meta_key' => 'post-likes', // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_key
			]
		);
	}

	/**
	 * Get random recipes.
	 *
	 * @param int $post_id Post ID.
	 * @param int $limit Limit.
	 * @return array<\WP_Post>
	 */
	public function get_random_related( int $post_id, int $limit ): array {
		$query = $this->get_related_query( $post_id, $limit * 2 );
		$recipes = $query->get_posts();
		shuffle( $recipes );

		$randomized_posts = array_chunk( $recipes, $limit );

		if ( ! $randomized_posts ) {
			return [];
		}

		return $randomized_posts[0];
	}

	/**
	 * Get popular recipes
	 *
	 * @param int $limit Limit.
	 * @return array<\WP_Post>
	 */
	public function get_popular( int $limit ): array {
		$query = $this->get_popular_query( $limit );

		return $query->get_posts();
	}

	/**
	 * Text to array
	 *
	 * @param string $text Texto.
	 * @return array<string>
	 */
	protected function text_to_array( string $text ): array {
		return array_filter(
			array_map(
				static function( $current ) {
					return trim( wp_strip_all_tags( $current ) );
				},
				preg_split(
					'/<\/(li)?(p)?>/',
					preg_replace(
						['/(<strong>)(.*)(<\/strong>)/', '/(<ul>)/', '/(<\/ul>)/', '/(<li>)/'],
						'',
						$text
					)
				)
			)
		);
	}

	/**
	 * Get term collection by ID.
	 *
	 * @param int $post_id Post ID.
	 */
	protected function get_term_collection_by_id( int $post_id ): ?\WP_Term {
		$terms = get_the_terms( $post_id, 'fw-receita-colecao' );

		if ( ! $terms || is_wp_error( $terms ) ) {
			return null;
		}

		return $terms[0];
	}

	/**
	 * Get used products.
	 *
	 * @param int $post_id Post ID.
	 * @return array<string>
	 */
	protected function get_used_products( int $post_id ): array {
		return array_map(
			function( $product ) {
				return $this->product_service->get_data_by_id( (int) $product['id'] );
			},
			carbon_get_post_meta( $post_id, 'products' )
		);
	}

}
