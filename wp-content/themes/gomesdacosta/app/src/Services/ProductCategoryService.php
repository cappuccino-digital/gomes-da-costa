<?php declare( strict_types = 1 );

namespace App\Services;

class ProductCategoryService {

	/**
	 * Get data by ID.
	 *
	 * @param int $term_id term ID.
	 * @return array<string>|null
	 */
	public function get_data_by_id( int $term_id ): ?array {
		$term = get_term( $term_id );

		return [
			'name' => $term->name,
			'slug' => $term->slug,
			'link' => get_term_link( $term_id ),
			'color' => carbon_get_term_meta( $term_id, 'color' ),
			'taxonomy' => $term->taxonomy,
			'image-sizes' => [
				'thumbnail' => wp_get_attachment_image( carbon_get_term_meta( $term_id, 'thumbnail' ), 'thumbnail' ),
			],
		];
	}

	/**
	 * Process product category
	 *
	 * @param \WP_Term $term Terms to process.
	 * @return array<string>
	 */
	public function get_data( \WP_Term $term ): array {
		return [
			'name' => $term->name,
			'link' => get_term_link( $term ),
			'image-sizes' => [
				'large' => wp_get_attachment_image( \carbon_get_term_meta( $term->term_id, 'large' ), 'full' ),
			],
		];
	}

	/**
	 * Get product category.
	 *
	 * @return array<string>
	 */
	public function get_product_category(): array {

		$terms = get_terms(
			[
				'taxonomy' => 'fw-produto-category',
				'parent' => 0,
			]
		);

		if ( $terms && ! is_wp_error( $terms ) ) {
			return array_map(
				function( \WP_Term $item ) {
					return $this->get_data( $item );
				},
				$terms
			);
		}

		return [];
	}

	/**
	 * Get product category by ID.
	 *
	 * @return array<string>
	 */
	public function get_product_category_by_id(): array {

		$terms = get_terms(
			[
				'taxonomy' => 'fw-produto-category',
				'parent' => 0,
			]
		);

		if ( $terms && ! is_wp_error( $terms ) ) {
			return array_map(
				function( \WP_Term $item ) {
					return $this->get_data_by_id( $item->term_id );
				},
				$terms
			);
		}

		return [];
	}

}
