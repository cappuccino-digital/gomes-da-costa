<?php declare( strict_types = 1 );

namespace App\Services;

class ProductService {

	/**
	 * Products cache name
	 *
	 * @var string
	 */
	protected $products_cache_name = 'gdc_products';

	/**
	 * Selected products cache name
	 *
	 * @var string
	 */
	protected $seleced_p_cache_name = 'gdc_selected_products';

	/**
	 * Get data by ID.
	 *
	 * @param int $post_id Post ID.
	 * @return array<string>
	 */
	public function get_data_by_id( int $post_id ): array {
		return [
			'name' => get_the_title( $post_id ),
			'link' => get_permalink( $post_id ),
			'image-sizes' => [
				'thumbnail' => get_the_post_thumbnail( $post_id, 'thumbnail' ),
			],
		];
	}

	/**
	 * Get products for archive.
	 *
	 * @param int $term_id Id do termo.
	 * @return array<string>
	 */
	public function get_products_archive( int $term_id ): array {
		$children = get_term_children( $term_id, 'fw-produto-category' );
		$has_child = $children && ! is_wp_error( $children );

		$products = $this->get_all_products( $term_id, $has_child );

		$products_archive = [];

		foreach ( $products as $item ) {
			$products_archive[$item['term_name']]['products'][] = array_merge(
				$item,
				[
					'image_sizes' => [
						'thumbnail' => get_the_post_thumbnail( $item['ID'], 'full' ),
					],
					'link' => get_permalink( $item['ID'] ),
				]
			);
			$products_archive[$item['term_name']]['term_id'] = (int) $item['term_id'];
			$products_archive[$item['term_name']]['link'] = get_term_link( (int)$item['term_id'], 'fw-produto-category' );
			$products_archive[$item['term_name']]['has_child'] = $term_id === 0
				? true
				: $has_child;
		}

		return $products_archive;
	}

	/**
	 * Get all products by term_id.
	 *
	 * @param int  $term_id Term id.
	 * @param bool $has_child Has child.
	 * @return string[][]
	 */
	public function get_all_products( int $term_id, bool $has_child ): array {
		global $wpdb;

		$products = wp_cache_get( $this->products_cache_name );

		if ( $products === false ) {
			$searched_parameter = 'wt.term_id';

			if ( $has_child ) {
				$searched_parameter = 'wtt.parent';
			}

			// phpcs:disable
			$products = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT wp.post_title, wt.name AS term_name, wt.term_id, wtt.parent, wp.ID
					FROM {$wpdb->prefix}posts wp
					INNER JOIN
						{$wpdb->prefix}term_taxonomy wtt ON wtt.taxonomy = 'fw-produto-category'
					INNER JOIN
						{$wpdb->prefix}terms wt ON wt.term_id = wtt.term_id
					INNER JOIN
						{$wpdb->prefix}term_relationships wtr ON wtr.object_id = wp.ID
						AND wtr.term_taxonomy_id = wtt.term_taxonomy_id
					WHERE
						wp.post_type = 'fw-produtos'
						AND ( NOT %d OR {$searched_parameter} = %d )
					ORDER BY
						wt.term_order,
						wp.menu_order",
					$term_id,
					$term_id
				),
				ARRAY_A
			);
			// phpcs:enable
			wp_cache_set( $this->products_cache_name, $products );
		}

		return $products;
	}

	/**
	 * Get selected product categories to show in home.
	 *
	 * @return array<string>
	 */
	public function get_selected_product_categories(): array {
		global $wpdb;

		$products = wp_cache_get( $this->seleced_p_cache_name );

		if ( $products === false ) {
			$products = $wpdb->get_results( // phpcs:ignore
				"SELECT DISTINCT
					t.term_id,
					t.name
				FROM
					{$wpdb->prefix}terms t
				INNER JOIN {$wpdb->prefix}term_taxonomy tt ON
					tt.term_id = t.term_id
				LEFT JOIN {$wpdb->prefix}termmeta tm ON
					t.term_id = tm.term_id AND tm.meta_key = '_hidde'
				WHERE
					tt.taxonomy = 'fw-produto-category' AND tt.parent = 0
					AND (
						tm.meta_key IS NULL OR ( tm.meta_key = '_hidde' AND tm.meta_value != 'yes' )
					)
				ORDER BY t.term_order",
				ARRAY_A
			);

			wp_cache_set( $this->seleced_p_cache_name, $products );
		}

		return array_map(
			function( $product ) {
				return $this->get_category_data_by_id_name( (int) $product['term_id'], $product['name'] );
			},
			$products
		);
	}

	/**
	 * Get category data by id and name.
	 *
	 * @param int    $term_id term to process.
	 * @param string $name term name.
	 * @return array<string>
	 */
	public function get_category_data_by_id_name( int $term_id, string $name ): array {
		return [
			'link' => get_term_link( $term_id ),
			'name' => $name,
			'image_sizes' => [
				'large' => wp_get_attachment_image( \carbon_get_term_meta( $term_id, 'large' ), 'full' ),
				'thumbnail' => wp_get_attachment_image( \carbon_get_term_meta( $term_id, 'thumbnail' ), 'full' ),
			],
		];
	}

	/**
	 * Process product categories.
	 *
	 * @param \WP_Term $term term to process.
	 * @return array<string>
	 */
	public function get_category_data( \WP_Term $term ): array {
		return [
			'link' => get_term_link( $term ),
			'name' => $term->name,
			'image_sizes' => [
				'large' => wp_get_attachment_image( \carbon_get_term_meta( $term->term_id, 'large' ), 'full' ),
				'thumbnail' => wp_get_attachment_image( \carbon_get_term_meta( $term->term_id, 'thumbnail' ), 'full' ),
			],
		];
	}

	/**
	 * Get product category.
	 *
	 * @return array<string>
	 */
	public function get_product_categories(): array {
		$terms = get_terms(
			[
				'taxonomy' => 'fw-produto-category',
				'parent' => 0,
			]
		);

		if ( $terms && ! is_wp_error( $terms ) ) {
			return array_map(
				function( $term ) {
					return $this->get_category_data( $term );
				},
				$terms
			);
		}

		return [];
	}

	/**
	 * Get product category.
	 *
	 * @param string $product_type Type of Product..
	 * @return array<string>
	 */
	public function get_products_per_categories( string $product_type ): array {
		global $wpdb;

		$product_type_id = '';

		if ( $product_type ) {
			// phpcs:ignore
			$results = $wpdb->get_results(
				$wpdb->prepare(
					"SELECT * FROM {$wpdb->prefix}terms wpt WHERE wpt.slug = %s",
					$product_type
				)
			);

			if ( count( $results ) ) {
				$product_type_id = $results[0]->term_id;
			}
		}

		$wild = '%';
		$like = $product_type;

		if ( ! $product_type ) {
			$find = $product_type;
			$like = $wild . $wpdb->esc_like( $find ) . $wild;
		}

		// phpcs:ignore
		$products = $wpdb->get_results(
			$wpdb->prepare(
				"SELECT DISTINCT wpp.post_title, wpp.*, wpt.* FROM {$wpdb->prefix}posts wpp 
				LEFT JOIN {$wpdb->prefix}term_relationships wptr ON wptr.object_id = wpp.ID
				LEFT JOIN {$wpdb->prefix}term_taxonomy wptt ON wptt.term_taxonomy_id = wptr.term_taxonomy_id
				LEFT JOIN {$wpdb->prefix}terms wpt ON wpt.term_id = wptt.term_id 
				WHERE wpp.post_type = 'fw-produtos' && 
					(wptt.taxonomy = 'fw-produto-category') &&
					(wpt.slug LIKE %s || wptt.parent = %s) && 
					wpp.post_status = 'publish' 
				ORDER BY wpt.name;",
				$like,
				$product_type_id
			)
		);

		$keys = array_keys( $products );
		$keys = array_map(
			static function( $key ) use ( $products ) {
				return $products[$key]->name;
			},
			$keys
		);

		$products_category = [];

		foreach ( $products as $index => $product ) {
			$key = $product->name;

			if ( array_key_exists( $key, $products_category ) ) {
				array_push( $products_category[$key], $products[$index] );
			} else {
				$products_category[$key][] = $products[$index];
			}
		}

		if ( ! $products_category ) {
			return [];
		}

		return $products_category;
	}

	/**
	 * Get related query.
	 *
	 * @param int $post_id Post ID.
	 * @param int $limit Limit.
	 */
	public function get_related_query( int $post_id, int $limit ): \WP_Query {
		return new \WP_Query(
			[
				'post_type' => 'fw-receitas',
				'posts_per_page' => $limit,
				'meta_query' => [ // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_query
					[
						'key' => 'products',
						'carbon_field_property' => 'id',
						'value' => $post_id,
						'compare' => '=',
					],
				],
			]
		);
	}

	/**
	 * Get random recipes.
	 *
	 * @param int $post_id Post ID.
	 * @param int $limit Limit.
	 * @return array<\WP_Post>
	 */
	public function get_random_related( int $post_id, int $limit ): array {

		$query = $this->get_related_query( $post_id, $limit );
		$recipes = $query->get_posts();
		shuffle( $recipes );

		return $recipes;
	}

	/**
	 * Get Nutritional information by ID
	 *
	 * @param int $post_id Product ID.
	 * @return array<string>
	 */
	public function get_nutritional_information_by_id( int $post_id ): array {
		return [
			'portion_si' => carbon_get_post_meta( $post_id, 'portion_si' ),
			'portion' => carbon_get_post_meta( $post_id, 'portion' ),
			'nutritional_items' => carbon_get_post_meta( $post_id, 'items' ),
			'notes' => carbon_get_post_meta( $post_id, 'notes' ),
		];
	}

	/**
	 * Get allergies by Product ID.
	 *
	 * @param int $post_id Product id.
	 * @return array<string>
	 */
	public function get_allergies_by_id( int $post_id ): array {
		$terms = get_the_terms( $post_id, 'fw-produto-alergico' );

		if ( ! $terms || is_wp_error( $terms ) ) {
			return [];
		}

		return array_map(
			static function( $term ) {
				return [
					'image' => wp_get_attachment_image( \carbon_get_term_meta( $term->term_id, 'thumbnail' ), 'full', false, [ 'class' => 'information__icon-alignment' ] ),
					'value' => $term->name,
				];
			},
			$terms
		);
	}

	/**
	 * Get health by product ID.
	 *
	 * @param int $post_id Product ID.
	 * @return array<string>
	 */
	public function get_health_by_id( int $post_id ): array {
		$terms = get_the_terms( $post_id, 'fw-produto-saude' );

		if ( ! $terms || is_wp_error( $terms ) ) {
			return [];
		}

		return array_map(
			static function( $term ) {
				return [
					'image' => wp_get_attachment_image( \carbon_get_term_meta( $term->term_id, 'thumbnail' ), 'full', false, [ 'class' => 'information__icon-alignment' ] ),
					'value' => $term->name,
				];
			},
			$terms
		);
	}

	/**
	 * Get metadata by ID
	 *
	 * @param int $post_id Product ID.
	 * @return array<string>
	 */
	public function get_metadata_by_id( int $post_id ): array {
		$weight = [
			[
				'image' => '<svg><use xlink:href="#balance"></use></svg>',
				'value' => sprintf( 'Peso %s', carbon_get_post_meta( $post_id, 'weight' ) ),
			],
		];

		$allergies = $this->get_allergies_by_id( $post_id );

		$health = $this->get_health_by_id( $post_id );

		return [
			'health' => array_merge(
				$weight,
				$health
			),
			'allergies' => $allergies,
		];
	}

}
