<?php declare( strict_types = 1 );

namespace App\Services;

class ApiGateway extends \GuzzleHttp\Client {

	/**
	 * {@inheritDoc}
	 *
	 * @param string $method Method.
	 * @param string $uri Uri.
	 * @param array  $options Options.
	 * @return string
	 */
	public function request( $method, $uri = '', array $options = [] ) {
		return parent::request( $method, $uri, $options )->getBody()->getContents();
	}

}
