<?php declare( strict_types = 1 );

namespace App\Services;

class SocialLinksService {

	/**
	 * Map social links with svg id.
	 *
	 * @var array<<string>>
	 */
	protected static $map = [
		'facebook.com' => [
			'id' => 'facebook',
			'title' => 'Facebook',
		],
		'instagram.com' => [
			'id' => 'instagram',
			'title' => 'Instagram',
		],
		'pinterest.com' => [
			'id' => 'pinterest',
			'title' => 'Pinterest',
		],
		'twitter.com' => [
			'id' => 'twitter',
			'title' => 'Twitter',
		],
		'youtube.com' => [
			'id' => 'youtube-logo',
			'title' => 'YouTube',
		],
	];

	/**
	 * Get params by URL.
	 *
	 * @param string $url Social URL.
	 * @return array<string>
	 */
	public static function get_params_by_url( string $url ): ?array {
		foreach ( self::$map as $key => $value ) {
			if ( preg_match( "/{$key}/", $url ) ) {
				return $value;
			}
		}

		return null;
	}

}
