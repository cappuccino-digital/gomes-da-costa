<?php declare( strict_types = 1 );

namespace App\Services;

use WPEmerge\ServiceProviders\ServiceProviderInterface;

/**
 * Service provider.
 *
 * @codeCoverageIgnore
 */
class ServiceProvider implements ServiceProviderInterface {

	/**
	 * {@inheritDoc}
	 *
	 * @param \Pimple\Container $container Container.
	 * @return void
	 */
	public function register( $container ): void {
		$container[ ProductService::class ] = static function() {
			return new ProductService;
		};

		$container[ ProductCategoryService::class ] = static function() {
			return new ProductCategoryService;
		};

		$container[ ProductCategoryService::class ] = static function() {
			return new ProductCategoryService;
		};

		$container[ RecipeService::class ] = static function( $container ) {
			return new RecipeService(
				$container[ProductService::class],
				$container[ProductCategoryService::class]
			);
		};

		$container[ ApiGateway::class ] = static function() {
			return new ApiGateway;
		};

		$container[ SCService::class ] = static function( $container ) {
			return new SCService(
				$container[ApiGateway::class]
			);
		};
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param  \Pimple\Container $container Container.
	 * @return void
	 */
	public function bootstrap( $container ): void { // phpcs:ignore
		// Do nothing.
	}

}
