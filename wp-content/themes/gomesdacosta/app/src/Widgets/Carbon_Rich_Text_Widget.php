<?php declare( strict_types = 1 );

namespace App\Widgets;

use Carbon_Fields\Field\Field;
use Carbon_Fields\Widget\Widget;

/**
 * A widget with a title and rich text fields.
 */
class Carbon_Rich_Text_Widget extends Widget {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->setup(
			'rich_text',
			__( 'Rich Text', 'app' ),
			__( 'Displays a block with title and WYSIWYG content.', 'app' ),
			[
				Field::make( 'text', 'title', __( 'Title', 'app' ) ),
				Field::make( 'rich_text', 'content', __( 'Content', 'app' ) ),
			]
		);
	}

	/**
	 * Renders the widget front-end.
	 *
	 * @param array<string> $args     Widgets arguments.
	 * @param array<string> $instance Instance values.
	 * @return void
	 */
	// phpcs:ignore
	public function front_end( $args, $instance ) {
		if ( $instance['title'] ) {
			$title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );

			echo $args['before_title'] . $title . $args['after_title']; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		}

		echo apply_filters( 'the_content', $instance['content'] ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
	}

}
