<?php declare( strict_types = 1 );

namespace App\Routing;

use WPEmerge\ServiceProviders\ServiceProviderInterface;

/**
 * Provides custom route conditions.
 * This is an example class so feel free to modify or remove it.
 */
class RouteConditionsServiceProvider implements ServiceProviderInterface {

	/**
	 * {@inheritDoc}
	 *
	 * @param \Pimple\Container $container Container.
	 * @return void
	 */
	public function register( $container ) { // phpcs:ignore
		// Example route condition registration.
		// $this->registerRouteCondition( $container, 'my_condition', MyCondition::class );.
	}

	/**
	 * {@inheritDoc}
	 *
	 * @param \Pimple\Container $container Container.
	 * @return void
	 */
	public function bootstrap( $container ) { // phpcs:ignore
		// Nothing to bootstrap.
	}

	/**
	 * Register a class as a route condition
	 *
	 * @param  \Pimple\Container $container Container.
	 * @param  string            $name Name.
	 * @param  string            $class_name Class name.
	 */
	protected function registerRouteCondition( \Pimple\Container $container, string $name, string $class_name ): void {
		$container[ WPEMERGE_ROUTING_CONDITION_TYPES_KEY ] = array_merge(
			$container[ WPEMERGE_ROUTING_CONDITION_TYPES_KEY ],
			[
				$name => $class_name,
			]
		);
	}

}
