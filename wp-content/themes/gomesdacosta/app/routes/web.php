<?php declare( strict_types = 1 );

/**
 * Web Routes.
 *
 * @link https://docs.wpemerge.com/#/framework/routing/methods
 * @package WPEmergeTheme
 */

use WPEmerge\Facades\Route;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Using our ExampleController to handle the homepage, for example.
// phpcs:ignore
// Route::get()->url( '/' )->handle( 'ExampleController@home' );

// If we do not want to hardcode a url, we can use one of the available route conditions instead.
// phpcs:ignore
// Route::get()->where( 'post_id', get_option( 'page_on_front' ) )->handle( 'ExampleController@home' );

Route::get()->where( 'is_singular', 'fw-receitas' )->handle( 'App\Controllers\Web\RecipeController@index' );
Route::get()->where( 'is_singular', 'fw-produtos' )->handle( 'App\Controllers\Web\ProductController@singular' );
Route::get()->where( 'is_page_template', 'templates/recipes-home.php' )->handle( 'App\Controllers\Web\RecipeController@home_recipe' );
Route::get()->where( 'is_page_template', 'templates/contact-us.php' )->handle( 'App\Controllers\Web\ContactUsController@index' );
Route::get()->where( 'is_tax', 'fw-receita-ocasiao' )->handle( 'App\Controllers\Web\RecipeTermsController@special_recipes_archive' );
Route::get()->where( 'is_post_type_archive', 'fw-receitas' )->handle( 'App\Controllers\Web\RecipeController@recipes_archive' );
Route::get()->where( 'is_post_type_archive', 'fw-produtos' )->handle( 'App\Controllers\Web\ProductController@index' );
Route::get()->where( 'is_tax', 'fw-produto-category' )->handle( 'App\Controllers\Web\ProductController@index' );
Route::get()->where( 'is_page_template', 'templates/download.php' )->handle( 'App\Controllers\Web\ProductController@products_archive' );

/**
 * Pass all front-end requests through WPEmerge.
 * WARNING: Do not add routes after this - they will be ignored.
 *
 * @link https://docs.wpemerge.com/#/framework/routing/methods?id=handling-all-requests
 */
Route::all();
