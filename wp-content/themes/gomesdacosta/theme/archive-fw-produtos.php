<?php declare( strict_types = 1 );

/**
 * App Layout: layouts/app.php
 *
 * This is the template that is used for displaying all posts by default.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @package WPEmergeTheme
 */

?>
<div class="archive-product">
	<div class="top-banner">
		<?php
			Theme::partial( 'products/header' );
		?>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<?php
				Theme::partial( 'products/products-accordion', compact( 'products_category', 'current_term' ) );
				?>
			</div>
		</div>
	</div>

	<div class="home-page__slide-bottom">
		<div class="container">
			<?php Theme::partial( 'homepage/products-section' ); ?>
		</div>
	</div>

	<div class="home-page__social">
		<?php
			Theme::partial( 'homepage/social' );
		?>
	</div>
</div>
