<?php declare( strict_types = 1 );

/**
 * The template for displaying Comments
 *
 * The area of the page that contains comments and the comment form.
 *
 * @package WPEmergeTheme
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<section class="section-comments" id="comments">
	<?php if ( have_comments() ) : ?>
		<ol class="comments">
			<?php
			wp_list_comments(
				[
					'callback' => static function( WP_Comment $comment, array $args, int $depth ): void {
						Theme::partial(
							'comment-single',
							[
								'comment' => $comment,
								'args' => $args,
								'depth' => $depth,
							]
						);
					},
				]
			);
			?>
		</ol>

		<?php
		carbon_pagination(
			'comments',
			[
				'enable_numbers' => true,
				'prev_html' => '<a href="{URL}" class="paging__prev">' . esc_html__( 'anterior', 'app' ) . '</a>',
				'next_html' => '<a href="{URL}" class="paging__next">' . esc_html__( 'próximo', 'app' ) . '</a>',
			]
		);
		?>
	<?php else : ?>
		<?php if ( ! comments_open() ) : ?>
			<p class="no-comments"><?php esc_html_e( 'Comentários desabilitados.', 'app' ); ?></p>
		<?php endif; ?>
	<?php endif; ?>

	<?php
	comment_form(
		[
			'title_reply' => '',
			'comment_notes_before' => '',
			'class_submit' => 'submit',
			'cancel_reply_link' => __( 'Cancelar', 'app' ),
			'cancel_reply_before' => '<span class="submit">',
			'submit_button' => '<input name="%1$s" type="submit" id="%2$s" class="%3$s" value="%4$s" />',
			'comment_field' => '<p class="content comment-form-comment"><textarea aria-label="comentar" id="comment" placeholder="O que você achou dessa receita?" name="comment" aria-required="true"></textarea></p>',
			'fields' => [
				'author' => '<p class="author comment-form-comment"><input aria-label="author" id="author" name="author" placeholder="Seu nome" type="text" value="" size="30" maxlength="245"%s /></p>',
				'email' => '<p class="email comment-form-comment"><input aria-label="email" id="email" name="email" placeholder="Seu email" type="text" value="" size="30" maxlength="245"%s /></p>',
				'cookies' => '<p class="cookies comment-form-comment"><label class="show-label" for="cookie">' . _x( 'Salvar meus dados neste navegador para a próxima vez que eu comentar.', 'app' ) . '</label><br /><input id="cookie" name="wp-comment-cookies-consent" type="checkbox" value="yes"%s /></p>',
			],
		]
	);
	?>
</section>
