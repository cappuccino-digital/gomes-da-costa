<?php declare( strict_types = 1 );

/**
 * Theme header partial.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @package WPEmergeTheme
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="Content-Type" content="<?php bloginfo( 'html_type' ); ?>; charset=<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0" />

		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		<?php App\shim_wp_body_open(); ?>
		<header class="header">
			<div class="header__container container">
				<div class="header__actions">
					<div class="header__actions-button-hamburguer">
						<button type="button" class="btn-hamburguer" aria-label="Menu">
							<span class="btn-hamburguer__trace"></span>
						</button>
					</div>
				</div>

				<div class="header__navbar">
					<nav class="navbar">
						<svg class="header__iconClose">
							<use xlink:href="#close"></use>
						</svg>

						<div class="navbar__header">
							<div class="navbar__header-logo">
								<?php if ( is_front_page() ) : ?>
									<h1 style="line-height: 0;">
										<?php the_custom_logo(); ?>
									</h1>
								<?php else : ?>
									<?php the_custom_logo(); ?>
								<?php endif; ?>
							</div>
							<svg class="header__iconClose header__iconClose-menu btn-hamburguer">
								<use xlink:href="#close"></use>
							</svg>
						</div>
						<?php
						wp_nav_menu(
							[
								'theme_location' => 'app_primary',
								'container' => '',
								'container_class' => '',
								'menu_class' => 'navbar__list',
								'fallback_cb' => '',
								'walker' => new App\Walkers\CustomWalkerNavMenu,
							]
						);
						?>
						<?php Theme::partial( 'shared/navbar-social' ); ?>
					</nav>
				</div>
				<div class="header__logo">
					<?php the_custom_logo(); ?>
				</div>
				
				<?php get_search_form(); ?>

				<svg class="header__iconSearch">
					<use xlink:href="#search"></use>
				</svg>

				<svg class="header__iconClose">
					<use xlink:href="#close"></use>
				</svg>

			</div>
		</header>
