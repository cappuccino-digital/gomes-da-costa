<?php declare( strict_types = 1 );

/**
 * Base app layout.
 *
 * @package WPEmergeTheme
 */

?>
@include('header')

@if (!is_singular())
	@php App\the_title( '<h2 class="post-title">', '</h2>' ) @endphp
@endif

@yield('content')

@include('sidebar')

@include('footer')
