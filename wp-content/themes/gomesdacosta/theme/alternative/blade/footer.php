<?php declare( strict_types = 1 );

/**
 * This file is required by WordPress. Delegates the actual rendering to footer.blade.php.
 *
 * @package WPEmergeTheme
 */
WPEmerge\render( 'footer' );
