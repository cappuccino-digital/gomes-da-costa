<?php declare( strict_types = 1 );

/**
 * This file is required by WordPress. Delegates the actual rendering to index.blade.php.
 *
 * @package WPEmergeTheme
 */
WPEmerge\render( 'index' );
