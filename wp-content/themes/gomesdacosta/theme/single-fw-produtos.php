<?php declare( strict_types = 1 );

/**
 * App Layout: layouts/app.php
 *
 * This is the template that is used for displaying all posts by default.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @package WPEmergeTheme
 */

the_post();
?>

<div class="single-product__image single-product__box-head-image">
	<div class="img-bg">
		<img src="/app/themes/gomesdacosta/dist/images/bg-product.jpg">
	</div>
	<div class="product-image-thumb">

		<div class="container">

			<div class="row">
				<div class="col-12">
				<?php the_post_thumbnail( 'full', ['sizes' => '(max-width:320px) 145px, (max-width:425px) 220px, 500px'] ); ?>
				</div>
				</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-12">
			
			<div class="single-product__box-head">
				<div class="single-product__title">
					<h1 class="title title--accent"><?php echo esc_html( convert_smilies( get_the_title() ) ); ?></h1>
				</div>
				<div class="single-product__description single-product__box-head-text">
					<?php the_content(); ?>
					<div class="single-product__another-description">
						<?php echo '<strong>' . esc_html__( 'INGREDIENTES', 'app' ) . ': </strong>' . wp_kses_post( $ingredients ); ?>
					</div>
					<div class="single-product__another-description">
						<?php echo '</br> <strong>' . esc_html__( 'PESO: ', 'app' ) . '</strong>' . wp_kses_post( carbon_get_post_meta( get_the_ID(), 'weight' ) ); ?>
					</div>
				</div>
			</div>
			<?php Theme::partial( 'products/nutritional-information' ); ?>
			<div class="single-product__information">
				<?php Theme::partial( 'products/metadata' ); ?>
			</div>
		</div>
	</div>
</div>
<div class="product-recipes-box">
	<?php Theme::partial( 'products/related-recipes' ); ?>
</div>
<div class="home-page__social">
	<?php Theme::partial( 'homepage/social' ); ?>
</div>
