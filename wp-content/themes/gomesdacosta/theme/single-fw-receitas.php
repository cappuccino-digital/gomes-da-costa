<?php declare( strict_types = 1 );

/**
 * App Layout: layouts/app.php
 *
 * This is the template that is used for displaying all posts by default.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @package WPEmergeTheme
 */

?>

<div class="page-recipe">
	<div class="text-center">
		<?php Theme::partial( 'recipes/header' ); ?>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-recipe__top">
					<h1 class="post-title"><?php echo esc_html( convert_smilies( get_the_title() ) ); ?></h1>
					<?php Theme::partial( 'recipes/like' ); ?>
				</div>
			</div>

			<div class="col-12 col-md-8">
				<?php Theme::partial( 'recipes/metadata' ); ?>
			</div>
			<div class="col-12 col-md-4">
				<div class="recipe">
					<div class="recipe__info">
						<?php Theme::partial( 'recipes/used-products' ); ?>
					</div>
				</div>
			</div>
			<div class="col-12">
				<div class="page-recipe__content">
					<div class="information-recipe">
						<?php if ( $ingredients ) : ?>
							<div class="recipe-content recipe-content--ingredients">
								<div class="recipe-content__top">
									<div class="recipe-content__icon">
										<svg>
											<use xlink:href="#bread-bag"></use>
										</svg>
									</div>
									<h5 class="recipe-content__title">Ingredientes</h5>
								</div>
								<?php echo wp_kses_post( $ingredients ); ?>
							</div>
										<?php endif; ?>

						<?php if ( $steps ) : ?>
							<div class="recipe-content recipe-content--steps">
								<div class="recipe-content__top">
									<div class="recipe-content__icon">
										<svg>
											<use xlink:href="#pan"></use>
										</svg>
									</div>
									<h5 class="recipe-content__title">Modo de Preparo</h5>
								</div>
								<?php echo wp_kses_post( $steps ); ?>
							</div>
						<?php endif; ?>

						<?php if ( $chef_clue ) : ?>
							<div class="recipe-content recipe-content--chef-clue">
								<div class="recipe-content__top">
									<div class="recipe-content__icon">
										<svg>
											<use xlink:href="#chef"></use>
										</svg>
									</div>
									<h5 class="recipe-content__title">Dica do Chef</h5>
								</div>
								<?php echo wp_kses_post( $chef_clue ); ?>
							</div>
						<?php endif; ?>

						<?php if ( $variation ) : ?>
							<div class="recipe-content recipe-content--chef-clue">
								<div class="recipe-content__top">
									<div class="recipe-content__icon">
										<svg>
											<use xlink:href="#chef"></use>
										</svg>
									</div>
									<h5 class="recipe-content__title">Variação</h5>
								</div>
								<?php echo wp_kses_post( $variation ); ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				

				<?php if ( have_comments() ) : ?>
					<div class="page-recipe__comments">
						<div class="comments">
							<div class="comments__top">
								<div class="comments__icon">
									<svg>
										<use xlink:href="#speech-bubble"></use>
									</svg>
								</div>
								<span class="title title--accent comments__title">
									Comentários
								</span>
							</div>
							<?php comments_template(); ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="receitas-relacionadas">
		<div class="page-recipe__related">
			<?php Theme::partial( 'recipes/related-recipes' ); ?>
		</div>
	</div>
	<div class="home-page__slide-bottom">
		<div class="container">
			<?php Theme::partial( 'homepage/products-section' ); ?>
		</div>
	</div>

	<div class="home-page__social">
		<?php
			Theme::partial( 'homepage/social' );
		?>
	</div>

</div>

<script type="application/ld+json">
	<?php echo wp_json_encode( $structured_data ); ?>
</script>

