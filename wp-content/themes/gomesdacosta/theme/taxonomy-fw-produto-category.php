<?php declare( strict_types = 1 );

/**
 * App Layout: layouts/app.php
 *
 * This is the template that is used for displaying all posts by default.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @package WPEmergeTheme
 */

require APP_THEME_DIR . 'archive-fw-produtos.php';
