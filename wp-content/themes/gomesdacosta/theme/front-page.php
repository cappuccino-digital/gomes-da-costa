<?php declare( strict_types = 1 );

/**
 * App Layout: layouts/app.php
 *
 * This is the template that is used for displaying home page.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @package WPEmergeTheme
 */

?>

<div class="home-page">
	<?php Theme::partial( 'homepage/featured-banner' ); ?>
	<?php Theme::partial( 'homepage/header' ); ?>
	

	<div class="home-page__slide-bottom">
		<div class="container">
			<?php Theme::partial( 'homepage/products-section' ); ?>
		</div>
	</div>
	
	<div class="home-page__social">	
		<?php Theme::partial( 'homepage/social' ); ?>
	</div>

	<div class="container">
		<?php Theme::partial( 'homepage/footer-banner' ); ?>
	</div>
</div>
