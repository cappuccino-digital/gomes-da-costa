<?php declare( strict_types = 1 );

?>
<div class="col-12 not-found">
	<div class="not-found__icon">
		<svg>
			<use xlink:href="#dead-fish"></use>
		</svg>
	</div>
	<div class="not-found__description">
		<h3 class="title title--accent not-found__title ">
		<?php echo esc_html( $title ); ?></h3>
		<?php echo esc_html( $description ); ?>
	</div>
	<?php if ( $has_back_to_navigation ) : ?>
		<span class="not-found__detail">Clique para voltar a navegar.</span>

		<a class="btn-default btn-default--fat not-found__btn" href="<?php echo esc_url( home_url( '/' ) ); ?>">Voltar para a Home</a>
	<?php endif; ?>
</div>
