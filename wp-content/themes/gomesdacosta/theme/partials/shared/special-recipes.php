<?php declare( strict_types = 1 );

?>
<div class="special-list mb-4">
	<h2 class="special-list__title">
		Receitas especiais
	</h2>
	<ul class="special-list__container">
		<?php foreach ( $recipes as $recipe ) : ?>
			<li class="special-list__item">
				<a href="<?php echo esc_url( $recipe['link'] ); ?>" class="special-list__item-content">
						<div class="special-list__item-wrapper">
							<div class="special-list__item-image">
								<?php echo wp_kses( $recipe['image-sizes']['icon-card'], App\get_allowed_html_attributes_for_image() ); ?>
							</div>
							<div class="special-list__item-text">
								<?php echo esc_html( $recipe['name'] ); ?>
							</div>
						</div>
					<div class="special-list__item-arrow"></div>
				</a>
			</li>
		<?php endforeach; ?>
	</ul>
	<div class="special-list__link">
		<?php if ( count( $recipes ) > 3 ) : ?>
			<button data-component="expand-special-recipes" class="link link--medium" aria-label="Receitas Especiais">Veja mais receitas especiais</button>
		<?php endif; ?>
	</div>
</div>
