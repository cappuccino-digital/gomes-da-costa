<?php declare( strict_types = 1 );

?>
<div class="navbar__social">
	<h3 class="navbar__social-title">
		Siga-nos em nossas redes
	</h3>
	<ul class="navbar__social-list">
		<?php foreach ( $footer_social_items as $media ) : ?>
			<li class="navbar__social-list-item">
				<a
					href="<?php echo esc_url( $media['url'] ); ?>"
					target="_blank"
					rel="noopener"
					class="navbar__social-list-link"
					aria-label="<?php echo esc_url( $media['title'] ); ?>">
					<svg>
						<use xlink:href="#<?php echo esc_attr( $media['id'] ); ?>"></use>
					</svg>

				</a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>
