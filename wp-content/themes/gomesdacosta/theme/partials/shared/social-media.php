<?php declare( strict_types = 1 );

?>

<div class="social-media p-0">
	<div class="social-media__title">
		<h2 class="title title--accent">
			<?php echo esc_html( $footer_social_text ); ?>
		</h2>
	</div>
	<ul class="social-media__list">
		<?php foreach ( $footer_social_items as $media ) : ?>
			<li class="social-media__list-item">
				<a
					href="<?php echo esc_url( $media['url'] ); ?>"
					target="_blank"
					rel="noopener"
					class="social-media__list-icon"
					aria-label="<?php echo esc_url( $media['title'] ); ?>">
					<svg>
						<use xlink:href="#<?php echo esc_attr( $media['id'] ); ?>"></use>
					</svg>

				</a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>
