<?php declare( strict_types = 1 );

?>
<div class="swiper-container">
	<div class="swiper-wrapper slide">
		<?php foreach ( $recipes as $recipe ) : ?>
			<div class="slide__item swiper-slide box box--vertical recipe-item">
				<a aria-label="<?php echo esc_attr( $recipe->post_title ); ?>" href="<?php echo esc_url( get_permalink( $recipe->ID ) ); ?>" class="home-page__recipe-item">
					
					<div class="box box--recipe box--has-border">
						<div class="box__image">
							<?php echo get_the_post_thumbnail( $recipe->ID ); ?>
						</div>
						<div class="box__description">
							<div class="box__title box__title--full">
								<h4 class="title"><?php echo esc_html( get_the_title( $recipe->ID ) ); ?></h4>
							</div>
						</div>
					</div>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
	<div class="swiper-button-prev product-slide__prev"></div>
	<div class="swiper-button-next product-slide__next"></div>
</div>
<div class="home-page__recipe-button">
	<a href="/receitas-gomes-da-costa/" class="btn-default btn-default--large btn-default--section">
		Veja todas as receitas
	</a>
</div>
