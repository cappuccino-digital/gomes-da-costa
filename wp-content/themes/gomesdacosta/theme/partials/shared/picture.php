<?php declare( strict_types = 1 );

if ( count( $image_sizes ) > 0 ) : ?>
	<picture class="<?php echo esc_attr( $picture_class ); ?>">
		<?php if ( count( $image_sizes ) > 1 ) : ?>
			<source media="(min-width: <?php echo esc_attr( $mobile_width ); ?>)" srcset="<?php echo esc_url( $image_sizes['desktop'] ); ?>">
			<img alt="<?php echo esc_attr( $alt ); ?>" class="<?php echo isset( $image_class ) ? esc_attr( $image_class ) : ''; ?>" src="<?php echo esc_url( $image_sizes['mobile'] ); ?>">
		<?php else : ?>
			<img alt="<?php echo esc_attr( $alt ); ?>" src="<?php echo esc_url( $image_sizes['desktop'] ?? $image_sizes['mobile'] ); ?>">
		<?php endif; ?>
	</picture>
<?php endif; ?>
