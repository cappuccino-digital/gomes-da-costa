<?php declare( strict_types = 1 );

?>

<?php if ( $video ) : ?>
	<div class="youtube-iframe" >
		<?php echo wp_oembed_get( $video, ['weigth' => 600, 'height' => 480] ); //phpcs:ignore ?>
	</div>
<?php else : ?>

	<img src="<?php site_url() ?>/app/themes/gomesdacosta/dist/images/receita-thumb.gif" alt="" style="background-image:url('<?php the_post_thumbnail_url(); ?>'); background-size: cover; background-position: center;" />

	<!-- <?php the_post_thumbnail( 'recipe-single', ['sizes' => '(max-width:320px) 145px, (max-width:425px) 220px, 500px'] ); ?> -->
<?php endif; ?>
