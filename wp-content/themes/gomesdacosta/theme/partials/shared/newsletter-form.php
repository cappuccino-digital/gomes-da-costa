<?php declare( strict_types = 1 );

if ( $form_id ) {
	gravity_form( $form_id, true, true, false, '', true );
}
