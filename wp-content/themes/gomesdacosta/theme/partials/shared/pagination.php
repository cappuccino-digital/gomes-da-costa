<?php declare( strict_types = 1 );

?>
<div class="pagination-links">
	<?php
		carbon_pagination(
			'posts',
			[
				'enable_numbers' => true,
				'prev_html' => '<a href="{URL}" class="paging__prev order-1"><span class="d-none d-sm-block paging-first">anterior</span></a>' . ( ! $show_first_ellipsis ? '<p class="order-3">...</p>' : '' ),
				'next_html' => ( ! $show_last_ellipsis ? '<p class="order-5">...</p>' : '' ) . '<a href="{URL}" class="paging__next order-7 "><span class="d-none d-sm-block paging-first">próximo</span></a>',
				'enable_first' => true,
				'enable_last' => true,
				'first_html' => '<a href="{URL}" class="paging-first order-2 ' . ( $firsts_pages ? 'd-none' : '' ) . '"><span>{PAGE_NUMBER}</span></a>',
				'last_html' => '<a href="{URL}" class="paging-last order-6 ' . ( $lasts_pages ? 'd-none' : '' ) . '"><span>{PAGE_NUMBER}</span></a>',
				'number_limit' => 2,
				'wrapper_before' => '<div class="paging ' . ( $show_first_ellipsis || $show_last_ellipsis ? 'full' : '' ) . '">',
			]
		);
		?>
</div>
