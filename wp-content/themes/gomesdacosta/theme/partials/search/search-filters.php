<?php declare( strict_types = 1 );

?>
<h1 class="search__result-message">Os resultados da busca por "<?php echo esc_html( $search ); ?>" foram: </h1>

<div class="search__result-categories">
	<a href="/?s=<?php echo esc_html( $search ); ?>" data-value="" id="search-filter-all" class="search__result-categories--tags search__result-categories--active">Todos (<?php echo esc_html( $count['todos'] ); ?>)  </a>
	<a href="/?s=<?php echo esc_html( $search ); ?>&type=fw-produtos" data-value="fw-produtos" class="search__result-categories--tags">Produtos (<?php echo esc_html( $count['produtos'] ); ?>)  </a>
	<a href="/?s=<?php echo esc_html( $search ); ?>&type=fw-receitas" data-value="fw-receitas" class="search__result-categories--tags">Receitas (<?php echo esc_html( $count['receitas'] ); ?>) </a>
</div>
