<?php declare( strict_types = 1 );

?>
<div class="home-page__slide-top">
	<div class="home-page__slide-top-container">
		<div class="swiper-wrapper">
		<?php foreach ( $banners as $banner ) : ?>
			<div class="swiper-slide home-page__slide-top-margin-control">
				<?php Theme::partial( 'shared/picture', ['image_sizes' => $banner['image-sizes'], 'picture_class' => 'home-page__slide-top-img', 'alt' => $banner['alt']] ); ?>
				<?php if ( $banner['show_cta'] ) : ?>
					<div class="container">
						<div class="row">
							<div class="col-12">
								<div class="home-page__slide-top-button">
									<a <?php echo $banner['new_tab'] ? 'target="_blank" rel="noopener"' : ''; ?> href="<?php echo esc_url( $banner['cta_link'] ); ?>" class="btn-default btn-default--medium btn-default--no-shadow"><?php echo esc_html( $banner['cta_text'] ); ?></a>
								</div>
							</div>
						</div>
					</div>
				<?php endif; ?>
			</div>
		<?php endforeach; ?>
		</div>
		<div class="swiper-pagination"></div>
		<div class="swiper-button-next"></div>
	<div class="swiper-button-prev"></div>
	</div>
</div>
