<?php declare( strict_types = 1 );

?>
<div class="product-slide">
	<div class="__title">
		<h2 class="title title--accent title--large title--head text-center">
			<?php echo esc_html( carbon_get_theme_option( 'title-products-section' ) ); ?>
		</h2>
	</div>
	<div class="product-slide__container">
		<div class="product-slide__wrapper swiper-wrapper">
			<?php foreach ( $product_types as $product_type ) : ?>
				<a href="<?php echo esc_url( $product_type['link'] ); ?>" alt="Link para <?php echo esc_html( $product_type['name'] ); ?>" class="swiper-slide box box--product single-product__related-slide-space" >
					<div class="box__image">
						<?php echo wp_kses( $product_type['image-sizes']['large'], App\get_allowed_html_attributes_for_image() ); ?>
					</div>
					<div class="box__title mt-3">
						<h4 class="title title--sun"><?php echo esc_html( $product_type['name'] ); ?></h4>
					</div>
				</a>
			<?php endforeach; ?>
		</div>
		<div class="swiper-button-prev product-slide__prev"></div>
		<div class="swiper-button-next product-slide__next"></div>
	</div>
	<div class="product-slide__button">
		<a href="<?php echo esc_url( $products_link ); ?>" class="btn-default btn-default--large btn-default--section">
			Veja todos os produtos
		</a>
	</div>
</div>
