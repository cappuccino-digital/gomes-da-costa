<?php declare( strict_types = 1 );

?>
<div class="social-cards">
	<div class="social-cards__title">
		<h2 class="title title--accent title--large title--head text-center">
			Acompanhe nossas redes sociais
		</h2>
	</div>
	<div class="social-cards__list">
		<div class="row">
			<?php foreach ( $social_medias as $media ) : ?>
				
				<div class="social-cards__list-item col-12 col-sm-6 col-lg-4">
					<div class="box box--has-border">
						<a href="<?php echo esc_url( $media['cta_link'] ); ?>" <?php echo $media['new_tab'] ? 'target="_blank" rel="noopener"' : '	'; ?> class="box__image">
							<?php Theme::partial( "shared/{$media['cta_media']['_type']}", $media['cta_media']['parameters'] ); ?>
						</a>
						<div class="box__description">
							<a href="<?php echo esc_url( $media['cta_link'] ); ?>" <?php echo $media['new_tab'] ? 'target="_blank" rel="noopener"' : '	'; ?> class="box__title">
								<h4 class="title title--accent">
									<?php echo esc_html( $media['media_title'] ); ?>
									<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
								</h4>
								<!-- <div class="box__icon">
									<svg>
										<use xlink:href="#<?php echo esc_attr( $media['id'] ); ?>"></use>
									</svg>
								</div> -->
							</a>
							<?php if ( $media['cta_link'] && $media['cta_text'] ) : ?>
							<div class="box__button">
								<a href="<?php echo esc_url( $media['cta_link'] ); ?>" <?php echo $media['new_tab'] ? 'target="_blank" rel="noopener"' : '	'; ?> class="btn-default btn-default--large">
											<?php echo esc_html( $media['cta_text'] ); ?>
								</a>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
				
			<?php endforeach; ?>
		</div>
	</div>
</div>

