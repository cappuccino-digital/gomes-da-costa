<?php declare( strict_types = 1 );

?>
<div class="home-page__recipe">
	<div class="home-page__recipe-title">
		<h3 class="title title--accent title--large title--head text-center title--head-new">
			<?php echo esc_html( carbon_get_theme_option( 'title-recipes-section' ) ); ?>
		</h3>
	</div>
	<div class="recipe-home-slider swiper-container-initialized swiper-container-horizontal">
		<div class="swiper-wrapper">
			<?php foreach ( $recipes as $recipe ) : ?>
			<div class="recipe-item swiper-slide">
				<a href="<?php echo esc_url( get_permalink( $recipe ) ); ?>" class="home-page__recipe-item">
					<div class="box box--recipe box--has-border">
						<div class="box__image">
							<?php echo get_the_post_thumbnail( $recipe, 'medium-card', ['class' => 'box__image-content'] ); ?>
						</div>
						<div class="box__description">
							<div class="box__title box__title--full">
								<h4 class="title color-blue"><?php echo esc_html( get_the_title( $recipe ) ); ?></h4>
							</div>
						</div>
					</div>
				</a>
			</div>
			<?php endforeach; ?>
		</div>
		<div class="swiper-button-prev product-slide__prev"></div>
		<div class="swiper-button-next product-slide__next"></div>
		<div class="col-12">
			<div class="home-page__recipe-button">
				<a href="<?php echo esc_url( $recipes_link ); ?>" class="btn-default btn-default--large btn-default--section">
					Veja todas as receitas
				</a>
			</div>
		</div>
	</div>
</div>
