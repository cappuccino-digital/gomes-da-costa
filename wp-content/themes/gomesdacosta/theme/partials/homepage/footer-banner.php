<?php declare( strict_types = 1 );

?>

<div class="col-12">
	<div class="home-page__banner-bottom">
		<?php if ( $link ) : ?>
			<a <?php echo $new_tab ? 'target="_blank" rel="noopener"' : ''; ?> href="<?php echo esc_url( $link ); ?>" >
				<?php Theme::partial( 'shared/picture', ['image_sizes' => $image_sizes, 'picture_class' => '', 'alt' => $alt] ); ?>
			</a>
		<?php else : ?>
			<?php Theme::partial( 'shared/picture', ['image_sizes' => $image_sizes, 'picture_class' => '', 'alt' => $alt] ); ?>
		<?php endif; ?>
	</div>
</div>
