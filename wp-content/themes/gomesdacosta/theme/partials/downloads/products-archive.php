<?php declare( strict_types = 1 );

?>
<div class="container">
	<?php if ( $products_per_category ) : ?>
		<?php foreach ( $products_per_category as $name => $category ) : ?>
				<div class="row">
					<div class="page-downloads__category-title">
						<h1 class="title title--accent title--head">
							<?php echo esc_attr( $name ); ?>
						</h1>
					</div>
				</div>
				<div class="row">
					<?php foreach ( $category as $product ) : ?>
						<div class="col-12 col-sm-6 col-lg-4">
							<div alt="<?php echo esc_attr( $product->post_title ); ?>" >
								<div class="box box--product-download box--has-border mt-4">
									<div class="box__image">
										<?php echo get_the_post_thumbnail( $product, 'medium-card' ); ?>
									</div>
									<div class="box-product__description">
										<div class="box-product__title box__title--full">
											<?php echo sprintf( '<h4 class="title title--accent">%s</h4>', esc_attr( get_the_title( $product ) ) ); ?>
										</div>
										<div class="box-product__button">
											<?php if ( carbon_get_post_meta( $product->ID, 'crb_file' ) ) : ?>
												<a href="<?php echo esc_attr( carbon_get_post_meta( $product->ID, 'crb_file' ) ); ?>" alt="Link para o produto <?php echo esc_attr( $product->post_title ); ?>" class="btn-default btn-default--x-small" download="<?php echo esc_attr( $product->post_title ); ?>">Baixar Produto</a>
											<?php else : ?>
												<a href="<?php echo esc_attr( get_the_post_thumbnail_url( $product, 'medium-card' ) ); ?>" alt="Link para o produto <?php echo esc_attr( $product->post_title ); ?>" class="btn-default btn-default--x-small" download="<?php echo esc_attr( $product->post_title ); ?>">Baixar Produto</a>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
				<br>
			<?php endforeach; ?>
		<?php endif ?>
</div>
