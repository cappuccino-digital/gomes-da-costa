<?php declare( strict_types = 1 );

?>
<div class="container">
	<div class="category-recipes__title">
		Categorias
	</div>

	<?php if ( count( $categories ) > 0 ) : ?>
	<div class="slider-bullet slider-bullet__categories py-2">
		<div class="swiper-wrapper">
			<a class="swiper-slide card-bullet card-bullet-category" alt="Todas" href="<?php echo esc_url( $products_link ); ?>">
				<div class="card-bullet__content card-bullet-category__content">
					<?php echo wp_kses( $all_recipe_image, App\get_allowed_html_attributes_for_image() ); ?>
				</div>
				<span class="card-bullet__description">Todas</span>
			</a>
			<?php foreach ( $categories as $item ) : ?>
			<a class="swiper-slide card-bullet card-bullet-category" alt="<?php echo esc_html( $item['name'] ); ?>" data-parameter-label="<?php echo 'tipo-produto'; ?>" data-parameter-value="<?php echo esc_attr( $item['slug'] ); ?>" href="javascript:void(0);">
				<div class="card-bullet__content card-bullet-category__content" style="background: <?php echo esc_attr( $item['color'] ); ?>">
					<?php echo wp_kses( $item['image-sizes']['thumbnail'], App\get_allowed_html_attributes_for_image() ); ?>
				</div>
				<span class="card-bullet__description"><?php echo esc_html( $item['name'] ); ?></span>
			</a>
			<?php endforeach; ?>
		</div>
	</div>
	<?php endif; ?>
</div>
