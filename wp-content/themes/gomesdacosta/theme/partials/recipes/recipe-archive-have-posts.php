<?php declare( strict_types = 1 );

foreach ( $recipes as $recipe ) :
	?>
	<div class="col-12 col-sm-6 col-lg-4">
		<a href="<?php echo esc_url( get_the_permalink( $recipe ) ); ?>" alt="<?php echo esc_attr( $recipe->post_title ); ?>" >
			<div class="box box--recipe box--has-border mt-4">
				<div class="box__image">
					<?php echo get_the_post_thumbnail( $recipe, 'medium-card' ); ?>
				</div>
				<div class="box__description">
					<div class="box__title box__title--full">
						<?php echo sprintf( '<h4 class="title title--accent">%s</h4>', esc_attr( get_the_title( $recipe ) ) ); ?>
					</div>
				</div>
			</div>
		</a>
	</div>
	<?php
endforeach;
