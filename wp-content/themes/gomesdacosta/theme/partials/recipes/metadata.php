<?php declare( strict_types = 1 );

?>
<div class="card-recipe__item">
	<div class="card-information">
		<div class="card-information__icon">
			<svg>
				<use xlink:href="#alarm-clock"></use>
			</svg>
		</div>
		<div class="card-information__details">
			<span class="card-information__tip">Preparo</span>
			<span class="card-information__value">
			<?php echo esc_html( $total_time ); ?> min.</span>
		</div>
	</div>
	<div class="card-information">
		<div class="card-information__icon">
			<svg>
				<use xlink:href="#dinner-pan"></use>
			</svg>
		</div>
		<div class="card-information__details">
			<span class="card-information__tip">Rendimento</span>
			<span class="card-information__value">
				<?php
					printf(
						esc_attr(
							/* translators: Número de porções. */
							_n(
								'%d porção',
								'%d porções',
								$yields,
								'app'
							)
						),
						intval( $yields )
					);
					?>
			</span>
		</div>
	</div>
</div>
