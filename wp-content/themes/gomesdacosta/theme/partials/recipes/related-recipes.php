<?php declare( strict_types = 1 );

if ( count( $recipes ) > 0 ) : ?>
<div class="related-content">
	<div class="related-content__title mb-4">
		<h2 class="title title--accent title--large title--head"><?php echo esc_html( isset( $optional_title ) ? $optional_title : 'Receitas Relacionadas' ); ?> </h2>
	</div>
	<?php Theme::partial( 'shared/swiper', compact( 'recipes' ) ); ?>
</div>
<?php endif; ?>
