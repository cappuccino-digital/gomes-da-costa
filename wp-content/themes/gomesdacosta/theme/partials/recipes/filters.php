<?php declare( strict_types = 1 );

?>

<div class="filter">
	<div class="filter__row">
		<div data-component="open-filter" class="filter__top">
			<div class="filter__icon">
				<svg>
					<use xlink:href="#filter"></use>
				</svg>
			</div>
			<div id="toggle_others_filters" class="filter__description filter__description-large">Mais filtros</div>
		</div>

		<div id="filter_bullets" class="filter__container-bullets"></div>

	</div>

	<div data-component="filter-toggle" class="container pl-0">
		<div class="row">
			<div class="col-12">
				<div class="filter__content">
					<div class="filter__top">
						<div class="filter__icon">
							<svg>
								<use xlink:href="#alarm-clock"></use>
							</svg>
						</div>
						<div class="filter__description">
							Tempo
						</div>
					</div>
				</div>

				<div class="filter__slider">
					<div class="slider-bullet slider-bullet__time py-2">
						<div class="swiper-wrapper">
							<?php foreach ( $time as $item ) : ?>
								<a class="swiper-slide card-bullet" data-parameter-label="tempo" data-parameter-value="<?php echo esc_attr( $item['value'] ); ?>" href="javascript:void(0);" >
									<div class="card-bullet__content">
										<?php echo esc_html( $item['label'] ); ?>
									</div>
									<span class="card-bullet__description"><?php echo esc_html( $item['unit'] ); ?></span>
								</a>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>

			<div class="col-12">
				<div class="filter__content">
					<div class="filter__top">
						<div class="filter__icon">
							<svg>
								<use xlink:href="#dinner-pan"></use>
							</svg>
						</div>
						<div class="filter__description">
							Porção
						</div>
					</div>

				<div class="filter__slider">
					<div class="slider-bullet slider-bullet__portion py-2">
						<div class="swiper-wrapper">
							<?php foreach ( $portion as $item ) : ?>
								<a class="swiper-slide card-bullet" data-parameter-label="porcoes" data-parameter-value="<?php echo esc_attr( $item ); ?>" href="javascript:void(0);" >
									<div class="card-bullet__content">
										<?php echo esc_html( str_replace( '|', ' a ', $item ) ); ?>
									</div>
									<span class="card-bullet__description">porções</span>
								</a>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
