<?php declare( strict_types = 1 );

?>

<?php foreach ( $used_products as $product ) : ?>
	<div class="card-recipe__item card-recipe__item_prduct">
		<div class="used-products">
			<picture class="used-products__image">
				<a  class="used-products__link" href="<?php echo esc_url( $product['link'] ); ?>">	<?php echo wp_kses_post( $product['image-sizes']['thumbnail'] ); ?></a>
			</picture>

			<div class="used-products__description">
				<span class="used-products__tip">
					Produto usado nesta receita:
				</span>
				<span class="used-products__name">
					<?php echo esc_html( $product['name'] ); ?>
				</span>
				<!-- <a  class="used-products__link" href="<?php echo esc_url( $product['link'] ); ?>">Ver produto</a> -->
			</div>
		</div>
	</div>
<?php endforeach; ?>

