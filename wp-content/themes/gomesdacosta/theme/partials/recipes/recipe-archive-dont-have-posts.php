<?php declare( strict_types = 1 );

Theme::partial( 'shared/not-found', ['title' => 'Que Pena!', 'description' => 'Parece que ainda não temos a receita que você buscou.', 'has_back_to_navigation' => false] );
?>
<div class="col-12 mt-4 mb-5">
	<?php Theme::partial( 'recipes/related-recipes', ['optional_title' => 'Veja essas outras delícias que selecionamos para você'] ); ?>
</div>
