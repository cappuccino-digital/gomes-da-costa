<?php declare( strict_types = 1 );

?>
<div data-component="like" data-nonce="<?php echo esc_attr( $nonce ); ?>"  data-recipe-id="<?php echo esc_attr( $recipe_id ); ?>"class="page-recipe__like like">
	<div class="like__content">
		<svg>
			<use xlink:href="#heart-like"></use>
		</svg>
	</div>

	<div class="like__content">
		<svg>
			<use xlink:href="#heart-unlike"></use>
		</svg>
	</div>
</div>
