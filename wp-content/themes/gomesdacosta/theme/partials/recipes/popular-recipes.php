<?php declare( strict_types = 1 );

?>
<section class="related-content">
	<div class="related-content__title">
		<h2 class="color-blue"><?php echo esc_html( carbon_get_theme_option( 'title-recipes-most-liked' ) ); ?></h2>
	</div>
	<?php Theme::partial( 'shared/swiper', compact( 'recipes' ) ); ?>
</section>
