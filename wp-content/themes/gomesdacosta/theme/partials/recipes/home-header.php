<?php declare( strict_types = 1 );

?>
<div class="top-banner">
	<div class="archive-product__banner">
		<picture class="">
			<source media="(min-width: 575.98px)" srcset="<?php site_url() ?>/app/themes/gomesdacosta/dist/images/bg-pages.png">
			<img alt="Conheça nossas linhas de produtos" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" src="<?php site_url() ?>/app/themes/gomesdacosta/dist/images/bg-pages.png">
		</picture>
	</div>
	<div class="archive-product__title">
		<h1 class="title title--accent title--head">
			<?php echo esc_html( carbon_get_theme_option( 'title-recipes' ) ); ?>
		</h1>
	</div>
</div>

<div class="header-recipe">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="search header-recipe__search">
					<form action="<?php echo esc_url( $recipes_link ); ?>">
						<input aria-label="Busca de receitas" class="search__input" value="<?php echo esc_attr( the_search_query() ); ?>" required type="text" name="s" placeholder="Encontre uma receita">
						<button class="search__button" type="submit" aria-label="Buscar">
							<svg>
								<use xlink:href="#search"></use>
							</svg>
						</button>
					</form>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 px-0">
				<div class="category-recipes">
					<?php Theme::partial( 'recipes/recipes-category', compact( 'all_recipe_image' ) ); ?>
				</div>
			</div>
		</div>
	</div>
</div>
