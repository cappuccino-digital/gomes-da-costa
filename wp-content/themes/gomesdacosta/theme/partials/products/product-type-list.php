<?php declare( strict_types = 1 );

?>
<div class="page-product__list">
	<div class="row">
		<?php foreach ( $categories as $category ) : ?>
			<div class="col-12 col-sm-6 col-md-6 col-lg-4 page-product__list-item">
				<a aria-label="<?php echo esc_attr( $category['name'] ); ?>" href="<?php echo esc_url( $category['link'] ); ?>">
					<div class="box box--product">
						<div class="box__image">
							<?php echo wp_kses( $category['image_sizes']['large'], App\get_allowed_html_attributes_for_image() ); ?>
						</div>
						<div class="box__description">
							<div class="box__title">
								<h4 class="title title--sun">
									<?php echo esc_html( $category['name'] ); ?>
								</h4>
							</div>
						</div>
					</div>
				</a>
			</div>
		<?php endforeach; ?>
	</div>
</div>
