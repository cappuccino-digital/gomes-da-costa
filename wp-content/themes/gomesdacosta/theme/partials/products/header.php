<?php declare( strict_types = 1 );

?>


<div class="archive-product__banner">
	<picture class="">
		<source media="(min-width: 575.98px)" srcset="<?php site_url() ?>/app/themes/gomesdacosta/dist/images/bg-pages.png">
		<img alt="Conheça nossas linhas de produtos" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" src="<?php site_url() ?>/app/themes/gomesdacosta/dist/images/bg-pages.png">
	</picture>
</div>
<div class="archive-product__title">
	<h1 class="title title--accent title--head">
		<?php echo esc_html( carbon_get_theme_option( 'title-products' ) ); ?>
	</h1>
</div>

