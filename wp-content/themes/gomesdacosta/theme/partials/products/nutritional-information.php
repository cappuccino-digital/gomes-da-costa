<?php declare( strict_types = 1 );

if ( count( $nutritional_items ) > 0 ) : ?>
	<div class="single-product__accordion">
		<div id="product-accordion" class="accordion">
			<div class="accordion__title" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
				<h3 class="title title--sun">
					Informações Nutricionais
				</h3>
				<div class="accordion__title-icon">
				</div>
			</div>
			<div id="collapseOne" class="collapse " aria-labelledby="headingOne" data-parent="#product-accordion">
				<div class="accordion__content">
					<div class="nutritional-table">
						<div class="nutritional-table__header">
							<div class="nutritional-table__header-row">
								<div class="nutritional-table__header-data">Quantidade por porção**
									(<?php echo esc_html( $portion_si ); ?>)
								</div>
								<div class="nutritional-table__header-data">%Vd</div>
								<div class="nutritional-table__header-data">Porção <?php echo esc_html( $portion ); ?></div>
							</div>
						</div>
						<div class="nutritional-table__body">
							<?php foreach ( $nutritional_items as $item ) : ?>
								<div class="nutritional-table__body-row">
									<div class="nutritional-table__body-data"><?php echo esc_html( $item['name'] ); ?></div>
									<div class="nutritional-table__body-data"><?php echo esc_html( $item['vd'] ); ?></div>
									<div class="nutritional-table__body-data"><?php echo esc_html( $item['quantity'] ); ?></div>
								</div>
							<?php endforeach; ?>
						</div>
						<div class="nutritional-table__footer">
							<div class="nutritional-table__footer-description">
								<p>
									<?php echo esc_html( $notes ); ?>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
