<?php declare( strict_types = 1 );

if ( count( $recipes ) > 0 ) : ?>
<div class="single-product__related">
	<div class="related-content">
		<div class="related-content__title">
			<h2 class="title title--accent title--head">Receitas relacionadas a esse produto</h2>
		</div>
		<?php Theme::partial( 'shared/swiper', compact( 'recipes' ) ); ?>
	</div>
</div>
<?php endif; ?>
