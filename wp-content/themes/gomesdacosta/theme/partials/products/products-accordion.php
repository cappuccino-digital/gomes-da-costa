<?php declare( strict_types = 1 );

?>
<div class="archive-product__accordion">
	<?php foreach ( $products_category as $name => $item ) : ?>
		<div class="archive-product__accordion-item <?php App\the_classnames( ['archive-product__accordion-item--show' => $item['term_id'] === $current_term] ); ?>">
			<div id="archive-accordion-<?php echo esc_attr( $item['term_id'] ); ?>" class="accordion">
				<?php if ( $item['has_child'] ) : ?>
					<div class="accordion__title collapsed" url="<?php echo esc_url( $item['link'] ); ?>" title="<?php echo esc_attr( $name ); ?>" id="heading-<?php echo esc_attr( $item['term_id'] ); ?>" data-toggle="collapse" data-target="#collapse-<?php echo esc_attr( $item['term_id'] ); ?>" aria-expanded="<?php echo $item['term_id'] === $current_term ? 'true' : 'false'; ?>" aria-controls="collapse-<?php echo esc_attr( $item['term_id'] ); ?>">
						<h3 class="title title--sun">
								<?php echo esc_html( $name ); ?>
						</h3>
						<div class="<?php App\the_classnames( ['accordion__title-icon--is-opened' => $item['term_id'] === $current_term, 'accordion__title-icon' => true] ); ?>">
						</div>
					</div>
				<?php endif; ?>
				<div id="collapse-<?php echo esc_attr( $item['term_id'] ); ?>" class="<?php App\the_classnames( ['show' => $item['term_id'] === $current_term] ); ?> collapse" aria-labelledby="heading-<?php echo esc_attr( $item['term_id'] ); ?>" data-parent="#archive-accordion-<?php echo esc_attr( $item['term_id'] ); ?>">
					<div class="accordion__content pb-4">
						<div class="row">
								<?php foreach ( $item['products'] as $product ) : ?>
								<div class="col-6 col-md-4 col-lg-3 mt-4">
									<div class="box-product">
										<a href="<?php echo esc_url( $product['link'] ); ?>" alt="Link para o produto <?php echo esc_attr( $product['post_title'] ); ?>">
											<div class="box-product__image">
												<?php echo wp_kses( $product['image_sizes']['thumbnail'], App\get_allowed_html_attributes_for_image() ); ?>
											</div>
										</a>
										<div class="box-product__description">
											<div class="box-product__title">
												<a href="<?php echo esc_url( $product['link'] ); ?>" alt="Link para o produto <?php echo esc_attr( $product['post_title'] ); ?>">
													<h3>
														<?php echo esc_html( $product['post_title'] ); ?>
													</h3>
												</a>
											</div>
											<div class="box-product__button">
												<a href="<?php echo esc_url( $product['link'] ); ?>" alt="Link para o produto <?php echo esc_attr( $product['post_title'] ); ?>" class="btn-default btn-default--x-small">Ver produto</a>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>

<!-- <div class="archive-product__accordion">
	<?php foreach ( $products_category as $name => $item ) : ?>
		<div class="archive-product__accordion-item">
			<div id="archive-accordion-<?php echo esc_attr( $item['term_id'] ); ?>" class="accordion">
				<?php if ( $item['has_child'] ) : ?>
					<div class="accordion__title" url="<?php echo esc_url( $item['link'] ); ?>" title="<?php echo esc_attr( $name ); ?>" id="heading-<?php echo esc_attr( $item['term_id'] ); ?>">
						<h3 class="title title--sun">
								<?php echo esc_html( $name ); ?>
						</h3>
						<div class="accordion__title-icon">
						</div>
					</div>
				<?php endif; ?>
				<div id="collapse-<?php echo esc_attr( $item['term_id'] ); ?>" class="collapse">
					<div class="accordion__content pb-4">
						<div class="row">
								<?php foreach ( $item['products'] as $product ) : ?>
								<div class="col-6 col-md-4 col-lg-3 mt-4">
									<div class="box-product">
										<a href="<?php echo esc_url( $product['link'] ); ?>" alt="Link para o produto <?php echo esc_attr( $product['post_title'] ); ?>">
											<div class="box-product__image">
												<?php echo wp_kses( $product['image_sizes']['thumbnail'], App\get_allowed_html_attributes_for_image() ); ?>
											</div>
										</a>
										<div class="box-product__description">
											<div class="box-product__title">
												<a href="<?php echo esc_url( $product['link'] ); ?>" alt="Link para o produto <?php echo esc_attr( $product['post_title'] ); ?>">
													<h3>
														<?php echo esc_html( $product['post_title'] ); ?>
													</h3>
												</a>
											</div>
											<div class="box-product__button">
												<a href="<?php echo esc_url( $product['link'] ); ?>" alt="Link para o produto <?php echo esc_attr( $product['post_title'] ); ?>" class="btn-default btn-default--x-small">Ver produto</a>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div> -->
