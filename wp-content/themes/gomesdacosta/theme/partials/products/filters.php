<?php declare( strict_types = 1 );

?>
<div class="page-product__category">
	<p class="page-product__category-title">
		Categorias de produtos
	</p>
	<div class="page-product__category-shortcut">
		<div class="slider-bullet">
			<div class="swiper-wrapper">
				<?php foreach ( $filters as $filter ) : ?>
				<a class="swiper-slide card-bullet swiper-slide-active" href="<?php echo esc_url( $filter['link'] ); ?>">
					<div class="card-bullet__content">
						<?php echo wp_kses( $filter['image_sizes']['thumbnail'], App\get_allowed_html_attributes_for_image() ); ?>
					</div>
					<span class="card-bullet__description"><?php echo esc_html( $filter['name'] ); ?></span>
				</a>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
