<?php declare( strict_types = 1 );

?>
<div class="information">
	<?php if ( count( $health ) >= 2 ) : ?>
		<div class="information--fifth">
			<h3 class="title title--accent title--section">Mais saúde para você</h3>
			<div class="information__wrapper">
				<?php foreach ( $health as $key => $item ) : ?> 
					<?php if ( $key ) : ?>
					<div class="information__item">
						<div class="information__icon">
							<?php echo wp_kses( $item['image'], App\get_allowed_html_attributes_for_image() ); ?>
						</div>
						<div class="information__description">
							<p class="information__description-text">
								<?php echo esc_html( $item['value'] ); ?>
							</p>
						</div>
					</div>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if ( count( $allergies ) ) : ?>
		<div class="information--fifth">
			<h3 class="title title--accent title--section">Alérgicos</h3>
			<div class="information__wrapper">
				<?php foreach ( $allergies as $item ) : ?>
					<div class="information__item">
						<div class="information__icon">
							<?php echo wp_kses( $item['image'], App\get_allowed_html_attributes_for_image() ); ?>
						</div>
						<div class="information__description">
							<p class="information__description-text">
								<?php echo esc_html( $item['value'] ); ?>
							</p>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
</div>
