<?php declare( strict_types = 1 );

/**
 * Theme footer partial.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @package WPEmergeTheme
 */

?>		
		<?php get_sidebar( 'footer' ); ?>

		<?php wp_footer(); ?>
		<script>
			jQuery(document).ready(function(){

				$ = jQuery;
				
				$('.title--head-new').each(function(index, element) {
					var $this = $(this), text=$this.text().trim(), words = text.split(/\s+/);
					var lastWord = words.pop();
					words.push('<span>' + lastWord + '</span>');
					$this.html(words.join(' '));
				});

			});
		</script>
	</body>
</html>
