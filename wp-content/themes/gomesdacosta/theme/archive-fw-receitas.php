<?php declare( strict_types = 1 );

/**
 * App Layout: layouts/app.php
 *
 * This is the template that is used for displaying all posts by default.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @package WPEmergeTheme
 */

Theme::partial( 'recipes/home-header', compact( 'title', 'all_recipe_image' ) );
?>
<div class="container">
	<div class="row">
		<?php
		if ( count( $recipes ) ) :
			Theme::partial( 'recipes/recipe-archive-have-posts', compact( 'recipes' ) );
		else :
			Theme::partial( 'recipes/recipe-archive-dont-have-posts', compact( 'not_found_recipes' ) );
		endif;
		?>
	</div>
	<?php Theme::partial( 'shared/pagination' ); ?>
</div>

<div class="home-page__slide-bottom">
	<div class="container">
		<?php Theme::partial( 'homepage/products-section' ); ?>
	</div>
</div>

<div class="home-page__social">
	<?php
		Theme::partial( 'homepage/social' );
	?>
</div>


