<?php declare( strict_types = 1 );

/**
 * App Layout: layouts/app.php
 *
 * This is the template that is used for displaying 404 errors.
 *
 * @package WPEmergeTheme
 */

?>

<div class="not-found">
	<div class="not-found__icon">
		<svg>
			<use xlink:href="#dead-fish"></use>
		</svg>
	</div>
	<div class="not-found__description">
		<h3 class="title title--accent not-found__title ">ALGO DEU ERRADO!</h3>
		A página que está procurando
		não existe ou foi removida.
		<span class="not-found__detail">Clique para voltar a navegar.</span>
	</div>

	<a class="btn-default btn-default--fat not-found__btn" href="<?php echo esc_url( home_url( '/' ) ); ?>">Voltar para a Home</a>
</div>
<div class="home-page__slide-bottom">
	<div class="container">
		<?php Theme::partial( 'homepage/products-section' ); ?>
	</div>
</div>

<div class="home-page__social">
	<?php
		Theme::partial( 'homepage/social' );
	?>
</div>

