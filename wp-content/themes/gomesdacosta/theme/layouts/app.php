<?php declare( strict_types = 1 );

/**
 * Base app layout.
 *
 * @link https://docs.wpemerge.com/#/framework/views/layouts
 * @package WPEmergeTheme
 */

WPEmerge\render( 'header' );

WPEmerge\layout_content();

WPEmerge\render( 'footer' );
