<?php declare( strict_types = 1 );

/**
 * Sidebar footer.
 *
 * @link https://codex.wordpress.org/Customizing_Your_Sidebar
 * @package WPEmergeTheme
 */

?>
<footer class="footer">
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-12 col-sm-6 col-md-6 col-lg-8">
				<div class="footer-nav-container">
					<?php for ( $i = 1; $i <= 4; $i += 1 ) : ?>
						<?php if ( is_active_sidebar( "footer-{$i}" ) ) : ?>
								<ul class="footer__widgets">
									<?php dynamic_sidebar( "footer-{$i}" ); ?>
								</ul>
						<?php endif; ?>
					<?php endfor; ?>
				</div>
			</div>

			<div class="col-12 col-sm-6 col-md-6 col-lg-4">
				<div class="footer__logo">
					<?php echo wp_kses_post( App\get_footer_logo() ); ?>
				</div>
				<?php Theme::partial( 'shared/social-media' ); ?>
			</div>
		</div>

		<!-- <div class="row flex-sm-row-reverse">
			<div class="col-12 col-sm-6">
			</div>
			<div class="col-12">
				<hr class="my-5 d-none d-sm-block">
				<p class="footer__credits">
					© Copyright Gomes da Costa <?php echo intval( gmdate( 'Y' ) ); ?>
				</p>
			</div>
		</div> -->
	</div>
</footer>
