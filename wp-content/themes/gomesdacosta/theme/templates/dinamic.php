<?php declare( strict_types = 1 );

/**
 * Template Name: Página Dinámica
 * App Layout: layouts/app.php
 */

?>

<section class="page-contact-us">
	<div class="top-banner">
		<div class="archive-product__banner">
			<picture class="">
				<source media="(min-width: 575.98px)" srcset="<?php site_url() ?>/app/themes/gomesdacosta/dist/images/bg-pages.png">
				<img alt="Conheça nossas linhas de produtos" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" src="<?php site_url() ?>/app/themes/gomesdacosta/dist/images/bg-pages.png">
			</picture>
		</div>
		<div class="archive-product__title">
			<h1 class="title title--accent title--head">
				<?php the_title(); ?>
			</h1>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				
				<?php the_post(); ?>
				<?php the_content(); ?>
			</div>
		</div>
	</div>

	<div class="home-page__slide-bottom">
		<div class="container">
			<?php Theme::partial( 'homepage/products-section' ); ?>
		</div>
	</div>

	<div class="home-page__social">
		<?php
			Theme::partial( 'homepage/social' );
		?>
	</div>
