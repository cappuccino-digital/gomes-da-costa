<?php declare( strict_types = 1 );

/**
 * Template Name: Página inicial de produtos
 * App Layout: layouts/app.php
 */

?>
<div class="page-product">
	<div class="top-banner">
		<?php Theme::partial( 'products/header' ); ?>	
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="product-page-list">
					<?php Theme::partial( 'products/product-type-list' ); ?>	
				</div>
			</div>
		</div>
	</div>
	<div class="home-page__social">
		<?php
			Theme::partial( 'homepage/social' );
		?>
	</div>
</div>
