<?php declare( strict_types = 1 );

/**
 * Template Name: Página Sobre Nós
 * App Layout: layouts/app.php
 */

?>
<section class="page-about-us">
	<div class="top-banner">
		<div class="archive-product__banner">
			<picture class="">
				<source media="(min-width: 575.98px)" srcset="https://gomesdacosta.com.br/app/themes/gomesdacosta/dist/images/bg-pages.png">
				<img alt="Conheça nossas linhas de produtos" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" src="https://gomesdacosta.com.br/app/themes/gomesdacosta/dist/images/bg-pages.png">
			</picture>
		</div>
		<div class="archive-product__title">
			<h1 class="title title--accent title--head">
				<?php the_title(); ?>
			</h1>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-about-us__story">
				<div class="page-about-us__story-title">
					<h1 class="title title--accent title--head">
						Nossa história
					</h1> 
					</div>
					<div class="page-about-us__story-slide" data-component="slide">
						<div class="page-about-us__story-slide-wrapper">
							<div class="swiper-wrapper">
								<div class="swiper-slide page-about-us__story-slide-item">
									<div class="page-about-us__story-slide-box">
										<div class="page-about-us__story-slide-img">
											<img src="https://gomesdacosta.com.br/app/uploads/2019/12/gomesdacosta_logo_anos50.png" class="home-page__slide-top-img" alt="gomes-da-costa-1950">
										</div>
										<div class="page-about-us__story-description" data-element="slide-desc">
											<div class="page-about-us__story-description-title">
												<h2 class="title title--accent title--head title--large">1954</h2>
											</div>
											<div class="page-about-us__story-description-wrapper" data-element="slide-desc-content">
												<p class="page-about-us__story-description-text">
													Nasce a marca <strong>Gomes da Costa</strong> criada pelo Sr. Rubem Gomes da Costa, um sonho que se torna realidade.
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="swiper-slide page-about-us__story-slide-item">
									<div class="page-about-us__story-slide-box">
										<div class="page-about-us__story-slide-img">
											<img src="https://gomesdacosta.com.br/app/uploads/2019/12/gomesdacosta_logo_anos60.png" class="home-page__slide-top-img" alt="gomes-da-costa-1960">
										</div>
										<div class="page-about-us__story-description" data-element="slide-desc">
											<div class="page-about-us__story-description-title">
												<h2 class="title title--accent title--head title--large">1960</h2>
											</div>
											<div class="page-about-us__story-description-wrapper"  data-element="slide-desc-content">
												<p class="page-about-us__story-description-text">
													Nesta década foi criado o primeiro slogan: “<strong>As Sardinhas Gomes da Costa</strong> - de sabor típico português”.
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="swiper-slide page-about-us__story-slide-item">
									<div class="page-about-us__story-slide-box">
										<div class="page-about-us__story-slide-img">
											<img src="https://gomesdacosta.com.br/app/uploads/2019/12/gomesdacosta_logo_anos70.png" class="home-page__slide-top-img" alt="gomes-da-costa-1970">
										</div>
										<div class="page-about-us__story-description" data-element="slide-desc">
											<div class="page-about-us__story-description-title">
												<h2 class="title title--accent title--head title--large">1970</h2>
											</div>
											<div class="page-about-us__story-description-wrapper" data-element="slide-desc-content">
												<p class="page-about-us__story-description-text">
													Já em 1972, <strong>As Sardinhas Gomes da Costa</strong> conquistaram de vez o público e o 1º lugar entre as “Grandes Marcas SP 1972”.
													Em nossa marca, surgia pela primeira vez a figura do Chef, dando mais personalidade e proximidade aos nossos produtos.
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="swiper-slide page-about-us__story-slide-item">
									<div class="page-about-us__story-slide-box">
										<div class="page-about-us__story-slide-img">
											<img src="https://gomesdacosta.com.br/app/uploads/2019/12/gomesdacosta_logo_anos80.png" class="home-page__slide-top-img" alt="gomes-da-costa-1980">
										</div>
										<div class="page-about-us__story-description" data-element="slide-desc">
											<div class="page-about-us__story-description-title">
												<h2 class="title title--accent title--head title--large">1980</h2>
											</div>
											<div class="page-about-us__story-description-wrapper" data-element="slide-desc-content">
												<p class="page-about-us__story-description-text">
													Ao longo dos anos, a marca foi se renovando e ficando cada vez mais moderna, consolidando sua imagem frente ao mercado e o público.
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="swiper-slide page-about-us__story-slide-item">
									<div class="page-about-us__story-slide-box">
										<div class="page-about-us__story-slide-img">
											<img src="https://gomesdacosta.com.br/app/uploads/2019/12/gomesdacosta_logo_anos90.png" class="home-page__slide-top-img" alt="gomes-da-costa-1990">
										</div>
										<div class="page-about-us__story-description" data-element="slide-desc">
											<div class="page-about-us__story-description-title">
												<h2 class="title title--accent title--head title--large">1990</h2>
											</div>
											<div class="page-about-us__story-description-wrapper" data-element="slide-desc-content">
												<p class="page-about-us__story-description-text">
													Já na década de 90 nosso querido Chef, que volta à marca acompanhado de um novo slogan: “O que o mar tem de melhor.”
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="swiper-slide page-about-us__story-slide-item">
									<div class="page-about-us__story-slide-box">
										<div class="page-about-us__story-slide-img">
											<img src="https://gomesdacosta.com.br/app/uploads/2019/12/gomesdacosta_logo_anos00.png" class="home-page__slide-top-img" alt="gomes-da-costa-2000">
										</div>
										<div class="page-about-us__story-description" data-element="slide-desc">
											<div class="page-about-us__story-description-title">
												<h2 class="title title--accent title--head title--large">2000</h2>
											</div>
											<div class="page-about-us__story-description-wrapper" data-element="slide-desc-content">
												<p class="page-about-us__story-description-text">
													O lançamento de diversos produtos trouxe ainda mais sabor para a família. Surgiam então as linhas de <strong>Filés de Atum
													em Azeite de Oliva</strong>, as <strong>Sardinhas com Sabores</strong> e os <strong>Patês
													de Atum</strong>, além dos deliciosos <strong>Molhos com Atum</strong>.
												</p>
												<p class="page-about-us__story-description-text">
													Esse espírito moderno acompanhou também a nossa marca, que se atualizou e foi ganhando cores e contornos sempre atuais.
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="swiper-slide page-about-us__story-slide-item">
									<div class="page-about-us__story-slide-box">
										<div class="page-about-us__story-slide-img">
											<img src="https://gomesdacosta.com.br/app/uploads/2019/12/gomesdacosta_logo_anos10.png" class="home-page__slide-top-img" alt="gomes-da-costa-2010">
										</div>
										<div class="page-about-us__story-description" data-element="slide-desc">
											<div class="page-about-us__story-description-title">
												<h2 class="title title--accent title--head title--large">2010</h2>
											</div>
											<div class="page-about-us__story-description-wrapper" data-element="slide-desc-content">
												<p class="page-about-us__story-description-text">
													Nos últimos anos nos dedicamos a inovar e trazer mais praticidade ao consumidor, com o lançamento da linha de <strong>Saladas de Atum</strong> prontas
													para consumir. Criamos também os novos <strong>Filés de Salmão</strong> e os <strong>Patês de Sardinha e Salmão</strong>, além de novas opções ainda mais saudáveis
													e funcionais, como as <strong>Sardinhas com Baixo Teor de Sódio</strong>. Além disso, esta década também foi marcada por nossa entrada
													no segmento de vegetais em conserva com o objetivo de atender ainda mais as necessidades dos nossos consumidores.
												</p>
											</div>
										</div>
									</div>
								</div>
								<div class="swiper-slide page-about-us__story-slide-item">
									<div class="page-about-us__story-slide-box">
										<div class="page-about-us__story-slide-img">
											<img src="https://gomesdacosta.com.br/app/uploads/2019/12/gomesdacosta_logo_anos17.png" class="home-page__slide-top-img" alt="gomes-da-costa-2015">
										</div>
										<div class="page-about-us__story-description" data-element="slide-desc">
											<div class="page-about-us__story-description-title">
												<h2 class="title title--accent title--head title--large">2015</h2>
											</div>
											<div class="page-about-us__story-description-wrapper" data-element="slide-desc-content">
												<p class="page-about-us__story-description-text">
													E essa história continua! Temos o compromisso de oferecer produtos saudáveis e inovadores para você e toda a família. Tudo com a mesma
													qualidade e carinho que carregamos em nosso slogan: “Naturalmente.”.
												</p>
											</div>
										</div>
									</div>
								</div>

								<div class="swiper-slide">
									<div class="page-about-us__story-slide-box">
										<div class="page-about-us__story-slide-img">
											<img src="https://gomesdacosta.com.br/app/uploads/2021/05/vai-com-gosto-sobre-nos.jpg" class="home-page__slide-top-img" alt="gomes-da-costa-hoje">
										</div>
										<div class="page-about-us__story-description" data-element="slide-desc">
											<div class="page-about-us__story-description-title">
												<h2 class="title title--accent title--head title--large">Hoje</h2>
											</div>
											<div class="page-about-us__story-description-wrapper" data-element="slide-desc-content">
												<p class="page-about-us__story-description-text">
													</strong>Gomes da Costa</strong> é uma marca que entende quem você é, nossos saborosos produtos são feitos para deixar sua vida mais saudável.
												</p>
											</div>
										</div>
									</div>

								</div>
							</div>
							<div class="swiper-button-next page-about-us__story-slide-next">
							</div>
							<div class="swiper-button-prev page-about-us__story-slide-prev">
							</div>
						</div>
					</div>
					<hr class="page-about-us__separate">
					<div class="page-about-us__story-institutional">
					<div class="page-about-us__story-institutional-title">
						<h2 class="title title--accent title--head title--large">
							Missão, visão, valores
						</h2>
					</div>
						<div class="box-navigation mt-4">
							<nav class="box-navigation__nav nav" id="nav-tab" role="tablist">
								
							<a class="nav-item nav-link mr-sm-4 mr-md-5 active" id="nav-mission-tab" data-toggle="tab" data-ref="story-institutional" href="#nav-mission" role="tab" aria-controls="nav-mission" aria-selected="true">
									<div class="circled-icon circled-icon--small circled-icon--is-active">
										<div class="circled-icon__content">
											<svg><use xlink:href="#flag"></use></svg>
										</div>
									</div>
								</a>
								
								<a class="nav-item nav-link mr-sm-4 mr-md-5" id="nav-vision-tab" data-toggle="tab" data-ref="story-institutional" href="#nav-vision" role="tab" aria-controls="nav-vision" aria-selected="false">
									<div class="circled-icon circled-icon--small">
										<div class="circled-icon__content">
											<svg ><use xlink:href="#eye"></use></svg>
										</div>
									</div>
								</a>
								
								<a class="nav-item nav-link mr-sm-4 mr-md-5" id="nav-values-tab" data-toggle="tab" data-ref="story-institutional" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">
									<div class="circled-icon circled-icon--small">
										<div class="circled-icon__content">
											<svg ><use xlink:href="#value"></use></svg>
										</div>
									</div>
								</a>
								<!-- <a class="nav-item nav-link" id="nav-management-tab" data-toggle="tab" data-ref="story-institutional" href="#nav-management" role="tab" aria-controls="nav-management" aria-selected="false">
									<div class="circled-icon circled-icon--small">
										<div class="circled-icon__content">
											<svg ><use xlink:href="#value"></use></svg>
										</div>
									</div>
								</a> -->
							</nav>
							<div class="box-navigation__content tab-content" id="nav-tabContent">
								
								<div class="tab-pane fade show active" id="nav-mission" role="tabpanel" aria-labelledby="nav-mission-tab">
									<div class="box-navigation__title mb-3">
										<h2 class="title box-navigation__title-content">
											Missão
										</h2>
									</div>
									<div class="box-navigation__content-text">
										<ul class="page-about-us__story-institutional-list">
											<li class="page-about-us__story-institutional-list-item">
												<p>Oferecer alimentos saudáveis e de qualidade que satisfaçam os
													consumidores.
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<p>
													Gerar valor para todas as partes interessadas.
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<p>
													Manter um clima de trabalho que permita atingir as metas e estimule o desenvolvimento das pessoas.
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<p>
													Incentivar o hábito saudável de consumo de pescado.
												</p>
											</li>
										</ul>
									</div>
								</div>

								<div class="tab-pane fade" id="nav-vision" role="tabpanel" aria-labelledby="nav-vision-tab">
									<div class="box-navigation__title mb-4">
										<h2 class="title box-navigation__title-content">
											Visão
										</h2>
									</div>
									<div class="box-navigation__content-text">
										<p>
												Ser líder do segmento de pescado em conserva e referência de empresa de alimentação
											no Brasil, sinônimo de produtos saudáveis, inovadores e de qualidade.
										</p>
									</div>
								</div>	
								
								<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
									<div class="box-navigation__title mb-3">
										<h2 class="title box-navigation__title-content">
											Valores
										</h2>
									</div>
									<div class="box-navigation__content-text">
										<ul class="page-about-us__story-institutional-list">
											<li class="page-about-us__story-institutional-list-item">
												<h3 class="title title--black mb-2">
													Compromisso
												</h3>
												<p>
													Nos comprometemos no alcance de nossas metas atuando com ética e responsabilidade.
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<h3 class="title title--black mb-2">
													Qualidade
												</h3>
												<p>
													Geramos confiança alcançando a excelência em tudo o que fazemos.
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<h3 class="title title--black mb-2">
													Inovação
												</h3>
												<p>
													Nos antecipamos aplicando novas ideias para superar expectativas.
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<h3 class="title title--black mb-2">
													Pessoas
												</h3>
												<p>
													Criamos valor através do trabalho em equipe e do desenvolvimento das pessoas.
												</p>
											</li>
										</ul>
									</div>
								</div>
								
								<!-- <div class="tab-pane fade" id="nav-management" role="tabpanel" aria-labelledby="nav-management-tab">ticas
									<div class="box-navigation__title mb-3">
										<h2 class="title box-navigation__title-content">
											Política de Gestão
										</h2>
									</div>
									<div class="box-navigation__content-text">
										<p>
											A GDC Alimentos, através da sua marca Gomes da Costa, em conformidade com a sua missão e estratégia, atuando
											no ramo de desenvolvimento, produção e comercialização de alimentos e embalagens, tem como compromissos
										</p>
										<ul class="page-about-us__story-institutional-list">
											<li class="page-about-us__story-institutional-list-item">
												<p>
													Nos comprometemos no alcance de nossas metas atuando com ética e responsabilidade.
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<p>
													Oferecer produtos de qualidade que satisfaçam clientes e consumidores;
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<p>
													Prover alimentos saudáveis, seguros e confiáveis, assim como orientações sobre hábitos saudáveis;
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<p>
													Atuar na proteção do Meio Ambiente, visando prevenir impactos ambientais adversos por meio da
													prevenção da poluição;
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<p>
													Preservar a saúde e a segurança, conscientizando colaboradores sobre a adoção de medidas de
													controle de riscos de suas atividades;
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<p>
													Cumprir toda a legislação aplicável aos processos e produtos;
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<p>
													Atender as expectativas de clientes, consumidores, colaboradores e acionistas;
												</p>
											</li>
											<li class="page-about-us__story-institutional-list-item">
												<p>
													Gerar melhoria contínua no Sistema de Gestão Integrado.
												</p>
											</li>
										</ul>
									</div>
								</div> -->
							</div>
						</div>
					</div>
					<hr class="page-about-us__separate">
				</div>
				<div class="page-about-us__responsability">
					<div class="page-about-us__responsability-title">
						<h2 class="title title--accent title--head title--large">
							Responsabilidade Social
						</h2>
					</div>
					<div class="page-about-us__responsability-box">
						<div class="box-navigation box-navigation--full">
							<div class="box-navigation__slide page-about-us__responsability-slide">
								<div class="swiper-wrapper box-navigation__slide-wrapper nav" id="nav-tab1" role="tablist">
									<a aria-label="Responsabilidade social" class="nav-item nav-link swiper-slide box-navigation__slide-item active" id="nav-1-tab" data-toggle="tab" data-ref="social-responsability" href="#nav-1" role="tab" aria-controls="nav-1" aria-selected="true">
										<div class="circled-icon m-auto circled-icon--is-active"> 
											<div class="circled-icon__content">
												<svg ><use xlink:href="#social-responsability"></use></svg>
											</div>
										</div>
									</a>
									<a aria-label="Contribuidores" class="nav-item nav-link swiper-slide box-navigation__slide-item" id="nav-2-tab" data-toggle="tab" data-ref="social-responsability" href="#nav-2" role="tab" aria-controls="nav-2" aria-selected="false">
										<div class="circled-icon m-auto">
											<div class="circled-icon__content">
												<svg ><use xlink:href="#contributors"></use></svg>
											</div>
										</div>
									</a>
									<a aria-label="Comunidade" class="nav-item nav-link swiper-slide box-navigation__slide-item" id="nav-3-tab" data-toggle="tab" data-ref="social-responsability" href="#nav-3" role="tab" aria-controls="nav-3" aria-selected="false">
										<div class="circled-icon m-auto">
											<div class="circled-icon__content">
												<svg ><use xlink:href="#community"></use></svg>
											</div>
										</div>
									</a>
									<a aria-label="Consumidores" class="nav-item nav-link swiper-slide box-navigation__slide-item" id="nav-4-tab" data-toggle="tab" data-ref="social-responsability" href="#nav-4" role="tab" aria-controls="nav-4" aria-selected="false">
										<div class="circled-icon m-auto">
											<div class="circled-icon__content">
												<svg ><use xlink:href="#customers"></use></svg>
											</div>
										</div>
									</a>
									<a aria-label="Ambiente" class="nav-item nav-link swiper-slide box-navigation__slide-item" id="nav-5-tab" data-toggle="tab" data-ref="social-responsability" href="#nav-5" role="tab" aria-controls="nav-5" aria-selected="false">
										<div class="circled-icon m-auto">
											<div class="circled-icon__content">
												<svg ><use xlink:href="#environment"></use></svg>
											</div>
										</div>
									</a>
									<a aria-label="Provedores" class="nav-item nav-link swiper-slide box-navigation__slide-item" id="nav-6-tab" data-toggle="tab" data-ref="social-responsability" href="#nav-6" role="tab" aria-controls="nav-6" aria-selected="false">
										<div class="circled-icon m-auto">
											<div class="circled-icon__content">
												<svg ><use xlink:href="#providers"></use></svg>
											</div>
										</div>
									</a>
									<a aria-label="Voluntariado corporativo" class="nav-item nav-link swiper-slide box-navigation__slide-item" id="nav-7-tab" data-toggle="tab" data-ref="social-responsability" href="#nav-7" role="tab" aria-controls="nav-7" aria-selected="false">
										<div class="circled-icon m-auto">
											<div class="circled-icon__content">
												<svg ><use xlink:href="#corporate-volunteering"></use></svg>
											</div>
										</div>
									</a>
								</div>
								<!-- Add Pagination -->
								<div class="page-about-us__responsability-pagination swiper-pagination"></div>
							</div>
							<div class="box-navigation__content tab-content" id="nav-tab1Content">
								<div class="tab-pane fade show active" id="nav-1" role="tabpanel" aria-labelledby="nav-1-tab">
									<h2 class="title title--black mt-4 mb-3">
										Responsabilidade Social Corporativa
									</h2>
									<p>Baseados nos princípios de Responsabilidade Social Corporativa, a Gomes da Costa
									desenvolveu os seguintes eixos de atuação:</p>
								</div>
								<div class="tab-pane fade" id="nav-2" role="tabpanel" aria-labelledby="nav-2-tab">
									<h2 class="title title--black mt-4 mb-3">
										Colaboradores
									</h2>
									<p>
										Desenvolvemos ações e projetos utilizando os temas prioritários como referência sensibilizando os colaboradores para temas e
										agendas de Responsabilidade Social Corporativa: Educação, qualidade de vida, alimentação saudável e meio ambiente.
									</p>
								</div>
								<div class="tab-pane fade" id="nav-3" role="tabpanel" aria-labelledby="nav-3-tab">
									<h2 class="title title--black mt-4 mb-3">
										Comunidade
									</h2>
									<p>
										Atuamos com projetos e ações que tenham como prioridades a educação, qualidade de vida, alimentação saudável e meio ambiente. Priorizando a
										atuação social e ambiental nas comunidades do entorno da Gomes da Costa.
									</p>
								</div>
								<div class="tab-pane fade" id="nav-4" role="tabpanel" aria-labelledby="nav-4-tab">
									<h2 class="title title--black mt-4 mb-3">
										Clientes
									</h2>
									<p>
										Ampliamos o reconhecimento da marca como uma empresa socialmente responsável.
									</p>
								</div>
								<div class="tab-pane fade" id="nav-5" role="tabpanel" aria-labelledby="nav-5-tab">
									<h2 class="title title--black mt-4 mb-3">
										Meio Ambiente
									</h2>
									<p>
										Atuamos na proteção do Meio Ambiente, visando a prevenção dos impactos ambientais e preservação dos recursos naturais, relacionados ao nosso
										negócio.
									</p>
								</div>
								<div class="tab-pane fade" id="nav-6" role="tabpanel" aria-labelledby="nav-6-tab">
									<h2 class="title title--black mt-4 mb-3">
										Fornecedores
									</h2>
									<p>
										Desenvolvemos práticas, processos e projetos que tenham como objetivo o desenvolvimento e monitoramento da cadeia de fornecedores.
									</p>
								</div>
								<div class="tab-pane fade" id="nav-7" role="tabpanel" aria-labelledby="nav-7-tab">
									<h2 class="title title--black mt-4 mb-3">
										Voluntariado Empresarial
									</h2>
									<p>
										Estimulamos o Voluntariado Empresarial e Engajamento dos colaboradores, intermediando o contato/necessidade das instituições e da
										possibilidade de atuação dos colaboradores como voluntários.
									</p>
								</div>
							</div>
						</div>
					</div>
					<hr class="page-about-us__separate">
				</div>
				<div class="page-about-us__contact">
					<div class="page-about-us__contact-title">
						<h2 class="title title--accent title--head title--large">
							Faça parte do nosso time
						</h2>
					</div>
					<div class="page-about-us__contact-box">
						<div class="contact-box contact-box--padding">
							<div class="contact-box__item p-0">
								<div class="contact-box__item-header">
									<div class="contact-box__item-icon">
										<svg>
											<use xlink:href="#team"></use>
										</svg>
									</div>
									<div class="contact-box__item-title">
										<h4 class="contact-box__item-title-content">
											Trabalhe conosco
										</h4>
									</div>
								</div>
								<div class="contact-box__item-body">
									<div class="contact-box__item-description">
										<div class="contact-box__item-description-wrapper">
											<p class="contact-box__item-link">
												<a rel="noopener" href="https://platform.senior.com.br/hcmrs/hcm/curriculo/?tenant=gomesdacosta&tenantdomain=gomesdacosta.com.br#!/login/register" class="link link--medium" target="_blank">Cadastre seu currículo</a>
											</p>
											<p class="contact-box__item-link">
												<a rel="noopener" href="https://platform.senior.com.br/hcmrs/hcm/curriculo/?tenant=gomesdacosta&tenantdomain=gomesdacosta.com.br#!/vacancies/list" class="link link--medium" target="_blank">Conheça todas as nossas vagas</a>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<hr class="page-about-us__separate">
				</div>
				<div class="page-about-us__politics">
					<div class="page-about-us__accordion">
						<div id="accordion-1" class="accordion accordion--shadow">
							<div class="accordion__title" id="heading-2" data-toggle="collapse" data-target="#collapse-2" aria-expanded="true" aria-controls="collapse-2">
								<h2 class="title title--sun title--head title--large">
									Políticas
								</h2>
								<div class="accordion__title-icon">
								</div>
							</div>
							<div id="collapse-2" class="collapse" aria-labelledby="heading-2" data-parent="#accordion-1">
								<div class="accordion__content pb-1 px-4">
									<ul class="page-about-us__politics-list">
										

										<li class="page-about-us__politics-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2021/07/politica-de-gestao.pdf" class="page-about-us__politics-list-link" target="_blank">
											Política de Gestão
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="page-about-us__politics-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2021/07/manual-fornecedores-pgs.pdf" class="page-about-us__politics-list-link" target="_blank">
											Manual de Fornecedores
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="page-about-us__politics-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2021/05/politica-de-privacidade.pdf" class="page-about-us__politics-list-link" target="_blank">
											Política de Privacidade de Dados
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="page-about-us__politics-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2021/05/politica-de-cookies.pdf" class="page-about-us__politics-list-link" target="_blank">
											Política de Cookies
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="page-about-us__politics-list-item">
											<a href="http://gomesdacosta.com.br/app/uploads/2019/05/Código-de-Ética-Versão-em-Português.pdf" class="page-about-us__politics-list-link" target="_blank">
												Código de Ética (Português)
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="page-about-us__politics-list-item">
											<a href="http://gomesdacosta.com.br/app/uploads/2019/05/Código-de-Ética-Versión-en-Español.pdf" target="_blank" class="page-about-us__politics-list-link">
												Código de Ética (Español)
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="page-about-us__politics-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2020/05/POLITICA_DE_CONFLITO_DE_INTERESSE_Vers%C3%A3o_em_Portugu%C3%AAs_v01.pdf" target="_blank" class="page-about-us__politics-list-link">
												Política de Conflito de Interesse (Português)
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="page-about-us__politics-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2020/05/POLITICA_DE_CONFLICTO_DE_INTER%C3%89S_Version_en_Espa%C3%B1ol_v01.pdf" target="_blank" class="page-about-us__politics-list-link">
												Política de Conflicto de Interés (Español)
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="page-about-us__politics-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2019/04/POLITICA-DE-COMPLIANCE-Versão-em-Português.pdf" target="_blank" class="page-about-us__politics-list-link">
												Política de Compliance (Português)
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="page-about-us__politics-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2019/04/POLITICA-DE-COMPLIANCE-Versión-en-Español..pdf" target="_blank" class="page-about-us__politics-list-link">
												Política de Compliance (Español)
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="page-about-us__politics-list-item">
											<a href="http://gomesdacosta.com.br/app/uploads/2019/04/POLITICA-ANTICORRUPCIÓN-Versión-en-Español-1.pdf" target="_blank" class="page-about-us__politics-list-link">
												Política Anticorrupción (Español)
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="page-about-us__politics-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2017/06/POLITICA-DE-ANTICORRUPÇÃO-Versão-em-Português..pdf" target="_blank" class="page-about-us__politics-list-link">
												PoIítica Anticorrupção GDC Alimentos S.A.
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="page-about-us__politics-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2020/05/Condições_Gerais_de_Contratação_para_Prestação_de_Serviços-2.pdf" target="_blank" class="page-about-us__politics-list-link">
												Condições Gerais de Contratação para Prestação de Serviços
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<!-- <li class="page-about-us__politics-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2020/05/Declara%C3%A7%C3%A3o-de-informa%C3%A7%C3%B5es-n%C3%A3o-financeiras_Grupo-Calvo_2018.pdf" target="_blank" class="page-about-us__politics-list-link">
												Informe Anual de 2018
												<div class="page-about-us__politics-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li> 
										<li class="accordion__page-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2021/01/Informe-Anual-Grupo-Calvo-2019_PT.pdf" class="accordion__page-list-link" target="_blank" rel="noopener noreferrer">
												Informe Anual 2019 (Português)
												<div class="accordion__page-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>-->
										<li class="accordion__page-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2021/08/informe-anual-grupo-calvo-2020_espanhol.pdf" class="accordion__page-list-link" target="_blank" rel="noopener noreferrer">
												Informe Anual 2020 (Español)
												<div class="accordion__page-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
										<li class="accordion__page-list-item">
											<a href="https://gomesdacosta.com.br/app/uploads/2021/11/RESUMEN_EJEC_CALVO_2020_pt_04.pdf" class="accordion__page-list-link" target="_blank" rel="noopener noreferrer">
												Informe Anual 2020 (Português)
												<div class="accordion__page-list-icon">
													<svg><use xlink:href="#download"></use></svg>
												</div>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="page-about-us__certifications">
					<div class="page-about-us__accordion">
						<div id="accordion-2" class="accordion accordion--shadow">
							<div class="accordion__title" id="heading-3" data-toggle="collapse" data-target="#collapse-3" aria-expanded="true" aria-controls="collapse-3">
								<h2 class="title title--sun title--head title--large">
									Certificações
								</h2>
								<div class="accordion__title-icon">
								</div>
							</div>
							<div id="collapse-3" class="collapse" aria-labelledby="heading-3" data-parent="#accordion-2">
								<div class="accordion__content pb-1 px-4">
									<div class="page-about-us__certifications-box">
										<div class="page-about-us__certifications-box-item">
											<div class="box">
													<div class="box__description">
														<div class="box__title">
															<h4 class="title title--black title--regular box__title-content">ISO 14001:2015</h4>
														</div>
														<div class="box__button">
															<a href="https://gomesdacosta.com.br/app/uploads/2021/11/dnv-iso-14001-2015.pdf" class="btn-default" target="_blank">visualizar</a>
														</div>
													</div>
												</div>
										</div>
										<div class="page-about-us__certifications-box-item">
											<div class="box">
													<div class="box__description">
														<div class="box__title">
															<h4 class="title title--black title--regular box__title-content">ISO 9001:2015</h4>
														</div>
														<div class="box__button">
															<a href="https://gomesdacosta.com.br/app/uploads/2021/11/dnv-iso-9001-2015.pdf" class="btn-default" target="_blank">visualizar</a>
														</div>
													</div>
												</div>
										</div>
										<div class="page-about-us__certifications-box-item">
											<div class="box">
													<div class="box__description">
														<div class="box__title">
															<h4 class="title title--black title--regular box__title-content">ISO 45001:2018</h4>
														</div>
														<div class="box__button">
															<a href="https://gomesdacosta.com.br/app/uploads/2021/11/dnv-iso-45001-2018.pdf" class="btn-default" target="_blank">visualizar</a>
														</div>
													</div>
												</div>
										</div>
										<div class="page-about-us__certifications-box-item">
											<div class="box">
													<div class="box__description">
														<div class="box__title">
															<h4 class="title title--black title--regular box__title-content">Movimento Nacional ODS</h4>
														</div>
														<div class="box__button">
															<a href="https://gomesdacosta.com.br/app/uploads/2021/11/movimento-nacional-ods.pdf" class="btn-default" target="_blank">visualizar</a>
														</div>
													</div>
												</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="page-about-us__location">
					<div class="page-about-us__accordion">
						<div id="accordion-3" class="accordion accordion--shadow">
							<div class="accordion__title" id="heading-1" data-toggle="collapse" data-target="#collapse-1" aria-expanded="true" aria-controls="collapse-1">
								<h2 class="title title--sun title--head title--large">
									Onde estamos
								</h2>
								<div class="accordion__title-icon">
								</div>
							</div>
							<div id="collapse-1" class="collapse" aria-labelledby="heading-1" data-parent="#accordion-3">
								<div class="accordion__content accordion__content--wrap pb-1 px-4">
									<div class="accordion__wrapper">
										<h3 class="title title--black mb-2">
											Buenos Aires, Argentina
										</h3>
										<p class="accordion__wrapper-text">Rua Carlos Pellegrini, 989</p>
										<p class="accordion__wrapper-text">Piso 6</p>
										<p class="accordion__wrapper-text">Capital Federal/Buenos Aires</p>
										<p class="accordion__wrapper-text">CEP: 1009</p>
										<p class="accordion__wrapper-text">Tel: 00 5411 4328-6813</p>
									</div>
									<div class="accordion__wrapper">
										<h3 class="title title--black mb-2">
											Maranhão
										</h3>
										<p class="accordion__wrapper-text">Edifício Golden Tower</p>
										<p class="accordion__wrapper-text">Rua Júpiter</p>
										<p class="accordion__wrapper-text">Lote 01 - Quadra 32 - Sala 914</p>
										<p class="accordion__wrapper-text">Renascença II, São Luís/MA</p>
										<p class="accordion__wrapper-text">CEP: 65075-441</p>
										<p class="accordion__wrapper-text">Tel: (98) 3226-2566</p>
									</div>
									<div class="accordion__wrapper">
										<h3 class="title title--black mb-2">
											Minas Gerais
										</h3>
										<p class="accordion__wrapper-text">Avenida Antônio Abrahão Caran, 430</p>
										<p class="accordion__wrapper-text">Sala 302</p>
										<p class="accordion__wrapper-text">São José, Belo Horizonte/MG</p>
										<p class="accordion__wrapper-text">CEP: 31275-000</p>
										<p class="accordion__wrapper-text">Tel: (31) 3281-3501</p>
										<p class="accordion__wrapper-text">Fax: (31) 3282-4588</p>
									</div>
									<div class="accordion__wrapper">
										<h3 class="title title--black mb-2">
											Pernambuco
										</h3>
										<p class="accordion__wrapper-text">Empresarial João Roma</p>
										<p class="accordion__wrapper-text">Avenida Conselheiro Aguiar, 2333</p>
										<p class="accordion__wrapper-text">5° andar - Sala 501</p>
										<p class="accordion__wrapper-text">Boa Viagem, Recife/PE</p>
										<p class="accordion__wrapper-text">CEP: 51020-020</p>
										<p class="accordion__wrapper-text">Tel: (81) 3377-0792</p>
										<p class="accordion__wrapper-text">Fax: (81) 3377-0804</p>
									</div>
									<div class="accordion__wrapper">
										<h3 class="title title--black mb-2">
											Rio de Janeiro
										</h3>
										<p class="accordion__wrapper-text">Edifício Barra Space Center</p>
										<p class="accordion__wrapper-text">Avenida das Américas, nº 1155</p>
										<p class="accordion__wrapper-text">Sala 1606</p>
										<p class="accordion__wrapper-text">Barra da Tijuca – Rio de Janeiro – RJ</p>
										<p class="accordion__wrapper-text">CEP: 22631-000</p>
										<p class="accordion__wrapper-text">Tel: (21) 2441-7179</p>
										<p class="accordion__wrapper-text">Fax: (21) 2439-4380</p>
									</div>
									<div class="accordion__wrapper">
										<h3 class="title title--black mb-2">
											Rio Grande do Sul
										</h3>
										<p class="accordion__wrapper-text">Rua Quintino Bocaiúva, 683</p>
										<p class="accordion__wrapper-text">Sala 503oinho de Vento, Porto Alegre/RS</p>
										<p class="accordion__wrapper-text">Moinho de Vento, Porto Alegre/RS</p>
										<p class="accordion__wrapper-text">CEP: 90440-051</p>
										<p class="accordion__wrapper-text">Tel: (51) 3330-2138</p>
									</div>
									<div class="accordion__wrapper">
										<h3 class="title title--black mb-2">
											São Paulo
										</h3>
										<p class="accordion__wrapper-text">WEWORK</p>
										<p class="accordion__wrapper-text">Av. das Nações Unidas, 12901</p>
										<p class="accordion__wrapper-text">12º andar – sala 125</p>
										<p class="accordion__wrapper-text">Cidade Monções, CEP: 04778-910</p>
										<p class="accordion__wrapper-text">São Paulo - SP</p>
									</div>
									<div class="accordion__wrapper">
										<h3 class="title title--black mb-2">
											Fábrica de Embalagens
										</h3>
										<p class="accordion__wrapper-text">Avenida Presidente Castelo Branco, 640</p>
										<p class="accordion__wrapper-text">Salseiros, Itajaí/SC</p>
										<p class="accordion__wrapper-text">CEP: 88311-470</p>
										<p class="accordion__wrapper-text">Tel: (47) 3241-8800</p>
										<p class="accordion__wrapper-text">Fax: (47) 3241-8805</p>
									</div>
									<div class="accordion__wrapper">
										<h3 class="title title--black mb-2">
											Fábrica GDC Alimentos S/A
										</h3>
										<p class="accordion__wrapper-text">Rua Eugênio Pezzini, 500</p>
										<p class="accordion__wrapper-text">Cordeiros, Itajaí/SC</p>
										<p class="accordion__wrapper-text">CEP: 88311-000</p>
										<p class="accordion__wrapper-text">Tel: (47) 3341-2600</p>
									</div>
									<div class="accordion__wrapper">
										<h3 class="title title--black mb-2">
											Escritório Central
										</h3>
										<p class="accordion__wrapper-text">WEWORK</p>
										<p class="accordion__wrapper-text">Av. das Nações Unidas, 12901</p>
										<p class="accordion__wrapper-text">12º andar – sala 125</p>
										<p class="accordion__wrapper-text">Cidade Monções, CEP: 04778-910</p>
										<p class="accordion__wrapper-text">São Paulo - SP</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="home-page__slide-bottom">
		<div class="container">
			<?php Theme::partial( 'homepage/products-section' ); ?>
		</div>
	</div>

	<div class="home-page__social">
		<?php
			Theme::partial( 'homepage/social' );
		?>
	</div>
</section>
