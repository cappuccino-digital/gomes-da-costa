<?php declare( strict_types = 1 );

/**
 * Template Name: Página Downloads
 * App Layout: layouts/app.php
 */

?>
<section class="page-downloads">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-downloads__banner">
					<?php the_post_thumbnail( 'full' ); ?>
				</div>
					<div class="page-downloads__title">
						<h1 class="title title--accent title--head">
							<?php echo esc_html( carbon_get_theme_option( 'title-downloads' ) ); ?>
						</h1>
					</div>
				<?php Theme::partial( 'downloads/products-category' ); ?>
				<?php Theme::partial( 'downloads/products-archive', compact( 'products_per_category' ) ); ?>
			</div>
		</div>
	</div>

	<div class="home-page__slide-bottom">
		<div class="container">
			<?php Theme::partial( 'homepage/products-section' ); ?>
		</div>
	</div>

	<div class="home-page__social">
		<?php
			Theme::partial( 'homepage/social' );
		?>
	</div>
</section>
