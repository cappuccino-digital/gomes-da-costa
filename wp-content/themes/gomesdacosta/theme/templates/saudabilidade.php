<?php declare( strict_types = 1 );

/**
 * Template Name: Página Saudabilidade
 * App Layout: layouts/app.php
 */

?>
<div class="page-saudabilidade">
    <div class="banner-top">
        <picture class="">
            <source media="(min-width: 575.98px)" srcset="https://gomesdacosta.com.br/app/uploads/2021/10/BANNER.jpg">
            <img alt="Conheça nossas linhas de produtos" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" src="https://gomesdacosta.com.br/app/uploads/2021/10/BANNER.jpg">
        </picture>
    </div>
    <div class="container">
        <h1 class="text-center"><img src="https://gomesdacosta.com.br/app/uploads/2021/10/saudavel.png" alt="Saudável!" title="Saudável!" class="img-title"></h1>
        <div class="saudabilidade-video-top">
            <div class="video-embed" onclick="dataLayer.push({ 'event':'GAEvent', 'eventCategory': 'saudabilidade', 'eventAction': 'play', 'eventLabel': 'video:bem-saudavel-bem-do-seu-lado'});">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/triaPxFnbSs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    
        <div class="only-mobile">
            <div class="saudabilidade-cards saudabilidade-card-1">
                <div class="row">
                    <div class="col-12 col-sm-6 saudabilidade-cards-text">
                        <h2 class="title--head">É só Atum, Água e Sal. Na Real!</h2>
                        <figure>
                            <img src="https://gomesdacosta.com.br/app/uploads/2021/10/pack-1-mobile.jpg" alt="É só Atum, Água e Sal. Na Real!">
                        </figure>
                        <p>
                            Composto apenas de atum, água e sal, o Atum Sólido ao Natural Gomes da Costa é tão natural como você nunca viu!
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="saudabilidade-cards saudabilidade-card-2">
                <div class="row">
                    <div class="col-12 col-sm-6 saudabilidade-cards-text">
                        <h2 class="title--head">Nada de Conservantes!</h2>
                        <div class="video-embed" onclick="dataLayer.push({ 'event':'GAEvent', 'eventCategory': 'saudabilidade', 'eventAction': 'play', 'eventLabel': 'video:segredo-dos-nossos-produtos'});">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/eAJkTCseDno" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                        <p>
                            Livre de aditivos e com a qualidade perfeita para manter a sua rotina alimentar mais equilibrada e saudável, o Atum Sólido ao Natural Gomes da Costa não possui conservantes, mantendo o gostinho natural devido ao seu processo de fabricação. 
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="saudabilidade-cards saudabilidade-card-3">
                <div class="row">
                    <div class="col-12 col-sm-6 saudabilidade-cards-img">
                        <h2 class="title--head">Muuuito mais proteína que o ovo!</h2>
                        <figure>
                            <img src="https://gomesdacosta.com.br/app/uploads/2021/10/Eggs-scaled.jpg" alt="">
                        </figure>
                        <p>
                            Saborosa e nutritiva, uma lata de Atum Sólido ao Natural Gomes da Costa possui muito mais proteína que o ovo, favorecendo sua saúde corporal para viver melhor. 
                        </p>
                    </div>
                </div>
            </div>

            <div class="saudabilidade-cards saudabilidade-card-4">
                <div class="row">
                <div class="col-12 col-sm-6 saudabilidade-cards-text">
                        <h2 class="title--head">Beeem Saudável!</h2>
                        <figure>
                            <img src="https://gomesdacosta.com.br/app/uploads/2021/10/toast.png" alt="">
                        </figure>
                        <p>
                        Para uma alimentação equilibrada e deliciosa, conte com os benefícios do Atum Sólido ao Natural Gomes da Costa! Ele é rico em ômega-3, que promove a saúde cardiovascular e tem ação anti-inflamatória; é fonte de proteínas, um nutriente essencial que mantém a saúde da pele e do cabelo; além de ter vitamina D, perfeita para a saúde muscular e óssea. 
                        </p>
                    </div>
                </div>
            </div>

            <div class="saudabilidade-cards saudabilidade-card-5">
                <div class="row">
                    <div class="col-12 col-sm-6 saudabilidade-cards-img saudabilidade-cards-text">
                        <h2 class="title--head">Bem do seu lado!</h2>
                        <figure>
                            <img src="https://gomesdacosta.com.br/app/uploads/2021/10/oleo.png" alt="">
                        </figure>
                        <p>
                            O Atum Sólido Ao Natural Gomes da Costa é a opção saudável mais prática para o seu cardápio, e o melhor: sempre esteve em sua despensa e bem pertinho de você. Pronto para comer ou ser um dos ingredientes de suas receitas, ele é indispensável! 
                        </p>
                    </div>
                </div>
            </div>
        </div>
       
        <div class="only-desktop">
            <div class="saudabilidade-cards saudabilidade-card-1">
                <div class="row">
                    <div class="col-12 col-sm-6">
                        <figure>
                            <img src="https://gomesdacosta.com.br/app/uploads/2021/10/pack-1.png" alt="É só Atum, Água e Sal. Na Real!">
                        </figure>
                    </div>
                    <div class="col-12 col-sm-6 saudabilidade-cards-text">
                        <h2 class="title--head">É só Atum, Água e Sal. Na Real!</h2>
                        <p>
                            Composto apenas de atum, água e sal, o Atum Sólido ao Natural Gomes da Costa é tão natural como você nunca viu!
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="saudabilidade-cards saudabilidade-card-2">
                <div class="row">
                    <div class="col-12 col-sm-6 saudabilidade-cards-text">
                        <h2 class="title--head">Nada de Conservantes!</h2>
                        <p>
                            Livre de aditivos e com a qualidade perfeita para manter a sua rotina alimentar mais equilibrada e saudável, o Atum Sólido ao Natural Gomes da Costa não possui conservantes, mantendo o gostinho natural devido ao seu processo de fabricação. 
                        </p>
                    </div>
                    <div class="col-12 col-sm-6">
                        <div class="video-embed" onclick="dataLayer.push({ 'event':'GAEvent', 'eventCategory': 'saudabilidade', 'eventAction': 'play', 'eventLabel': 'video:segredo-dos-nossos-produtos'});">
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/eAJkTCseDno" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="saudabilidade-cards saudabilidade-card-3">
                <div class="row">
                    <div class="col-12 col-sm-6 saudabilidade-cards-img">
                        <figure>
                            <img src="https://gomesdacosta.com.br/app/uploads/2021/10/Eggs-scaled.jpg" alt="">
                        </figure>
                    </div>
                    <div class="col-12 col-sm-6 saudabilidade-cards-text">
                        <h2 class="title--head">Muuuito mais proteína que o ovo!</h2>
                        <p>
                            Saborosa e nutritiva, uma lata de Atum Sólido ao Natural Gomes da Costa possui muito mais proteína que o ovo, favorecendo sua saúde corporal para viver melhor. 
                        </p>
                    </div>
                </div>
            </div>

            <div class="saudabilidade-cards saudabilidade-card-4">
                <div class="row">
                <div class="col-12 col-sm-6 saudabilidade-cards-text">
                        <h2 class="title--head">Beeem Saudável!</h2>
                        <p>
                        Para uma alimentação equilibrada e deliciosa, conte com os benefícios do Atum Sólido ao Natural Gomes da Costa! Ele é rico em ômega-3, que promove a saúde cardiovascular e tem ação anti-inflamatória; é fonte de proteínas, um nutriente essencial que mantém a saúde da pele e do cabelo; além de ter vitamina D, perfeita para a saúde muscular e óssea. 
                        </p>
                    </div>
                    <div class="col-12 col-sm-6 saudabilidade-cards-img">
                        <figure>
                            <img src="https://gomesdacosta.com.br/app/uploads/2021/10/toast.png" alt="">
                        </figure>
                    </div>
                </div>
            </div>

            <div class="saudabilidade-cards saudabilidade-card-5">
                <div class="row">
                    <div class="col-12 col-sm-6 saudabilidade-cards-img">
                        <figure>
                            <img src="https://gomesdacosta.com.br/app/uploads/2021/10/oleo.png" alt="">
                        </figure>
                    </div>
                    <div class="col-12 col-sm-6 saudabilidade-cards-text">
                        <h2 class="title--head">Bem do seu lado!</h2>
                        <p>
                            O Atum Sólido Ao Natural Gomes da Costa é a opção saudável mais prática para o seu cardápio, e o melhor: sempre esteve em sua despensa e bem pertinho de você. Pronto para comer ou ser um dos ingredientes de suas receitas, ele é indispensável! 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
    <div class="container">
        <h2 class="title title--accent title--large title--head text-center do-you-know"><img src="https://gomesdacosta.com.br/app/uploads/2021/10/voce-sabia.png" alt="Você Sabia?" title="Você Sabia?" class="img-title"></h2>
    </div>
    <div class="know-page__slide-top-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide home-page__slide-top-margin-control">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide1.jpg" class="only-desktop" alt="">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide1-mobile.jpg" class="only-mobile" alt=""> 
            </div>
            <div class="swiper-slide home-page__slide-top-margin-control">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide2.jpg" class="only-desktop" alt="">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide2-mobile.jpg" class="only-mobile" alt="">
            </div>
            <div class="swiper-slide home-page__slide-top-margin-control">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide3.jpg" class="only-desktop" alt="">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide3-mobile.jpg" class="only-mobile" alt="">
            </div>
            <div class="swiper-slide home-page__slide-top-margin-control">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide4.jpg" class="only-desktop" alt="">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide4-mobile.jpg" class="only-mobile" alt="">
            </div>
            <div class="swiper-slide home-page__slide-top-margin-control">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide5.jpg" class="only-desktop" alt="">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide5-mobile.jpg" class="only-mobile" alt="">
            </div>
            <div class="swiper-slide home-page__slide-top-margin-control">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide6.jpg" class="only-desktop" alt="">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide6-mobile.jpg" class="only-mobile" alt="">
            </div>
            <div class="swiper-slide home-page__slide-top-margin-control">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide7.jpg" class="only-desktop" alt="">
                <img src="https://gomesdacosta.com.br/app/uploads/2021/10/slide7-mobile.jpg" class="only-mobile" alt="">
            </div>
        </div>
        <div class="swiper-button-next next-saudabilidade-banner know-slide__next"></div>
        <div class="swiper-button-prev prev-saudabilidade-banner know-slide__prev"></div>
        <div class="swiper-pagination"></div>
    </div>
    <div class="container">
        <h2 class="title title--accent title--large title--head text-center do-you-know">
            <img src="https://gomesdacosta.com.br/app/uploads/2021/10/como-ser-saudavel.png" alt="Como ser saudável com o atum Gomes Da Costa:" title="Como ser saudável com o atum Gomes Da Costa:" class="img-title only-desktop">
            <img src="https://gomesdacosta.com.br/app/uploads/2021/10/como-ser-saudavel-mobile.png" alt="Como ser saudável com o atum Gomes Da Costa:" title="Como ser saudável com o atum Gomes Da Costa:" class="img-title only-mobile">
        </h2>
    </div>
    <div class="container">
        
        <div class="recipe-saudabilidade-slider swiper-container-initialized swiper-container-horizontal">
            <div class="swiper-wrapper">
                <div class="recipe-saudabilidade-item swiper-slide home-page__slide-top-margin-control">
                    <a href="https://gomesdacosta.com.br/receitas/atum-embarcado/" class="home-page__recipe-item"  onclick="dataLayer.push({ 'event':'GAEvent', 'eventCategory': 'saudabilidade', 'eventAction': 'click', 'eventLabel': 'confira-a-receita-completa:que-tal-conhecer-as-nossas-receitas:atum-embarcado'});">
                        <div class="saudabilidade-recipe-box">
                            <div class="saudabilidade-recipe-img">
                                <img width="320" height="250" src="https://gomesdacosta.com.br/app/uploads/2017/04/atum-embarcado-mobile.jpg" class="attachment-medium-card size-medium-card wp-post-image" alt="Atum embarcado" loading="lazy" srcset="https://gomesdacosta.com.br/app/uploads/2017/04/atum-embarcado-mobile.jpg 320w, https://gomesdacosta.com.br/app/uploads/2017/04/atum-embarcado-mobile-300x234.jpg 300w" sizes="(max-width: 320px) 100vw, 320px">
                            </div>
                            <div class="saudabilidade-recipe-content">
                                <h3>Atum embarcado</h3>
                                <div class="btn-saudabilidade-recipe">
                                    Confira a receita completa. 
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                
                <div class="recipe-saudabilidade-item swiper-slide home-page__slide-top-margin-control">
                    <a href="https://gomesdacosta.com.br/receitas/carpaccio-de-abacaxi-alface-e-atum-ao-molho-de-iorgute/" class="home-page__recipe-item"  onclick="dataLayer.push({ 'event':'GAEvent', 'eventCategory': 'saudabilidade', 'eventAction': 'click', 'eventLabel': 'confira-a-receita-completa:Carpaccio-de-abacaxi-alface-e-atum-ao-molho-de-iorgute'});">
                        <div class="saudabilidade-recipe-box">
                            <div class="saudabilidade-recipe-img">
                                <img width="320" height="250" src="https://gomesdacosta.com.br/app/uploads/2017/04/carpaccio-de-abacaxi-alface-e-atum-ao-molho-de-iorgute-mobile.jpg" class="attachment-medium-card size-medium-card wp-post-image" alt="Carpaccio de Abacaxi, Alface e Atum ao Molho de Iogurte" loading="lazy" srcset="https://gomesdacosta.com.br/app/uploads/2017/04/carpaccio-de-abacaxi-alface-e-atum-ao-molho-de-iorgute-mobile.jpg 320w, https://gomesdacosta.com.br/app/uploads/2017/04/carpaccio-de-abacaxi-alface-e-atum-ao-molho-de-iorgute-mobile-300x234.jpg 300w" sizes="(max-width: 320px) 100vw, 320px">
                            </div>
                            <div class="saudabilidade-recipe-content">
                                <h3>Carpaccio de Abacaxi, Alface e Atum ao Molho de Iogurte </h3>
                                <div class="btn-saudabilidade-recipe">
                                        Confira a receita completa. 
                                    </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="recipe-saudabilidade-item swiper-slide home-page__slide-top-margin-control">
                    <a href="https://gomesdacosta.com.br/receitas/salada-natalina-de-batata-doce-e-atum/" class="home-page__recipe-item"  onclick="dataLayer.push({ 'event':'GAEvent', 'eventCategory': 'saudabilidade', 'eventAction': 'click', 'eventLabel': 'confira-a-receita-completa:que-tal-conhecer-as-nossas-receitas:Salada-Natalina-de-Batata-Doce-e-Atum'});">
                        <div class="saudabilidade-recipe-box">
                            <div class="saudabilidade-recipe-img">
                                <img width="320" height="250" src="https://gomesdacosta.com.br/app/uploads/2017/04/salada-natalina-de-batata-doce-e-atum-mobile.jpg" class="attachment-medium-card size-medium-card wp-post-image" alt="Salada Natalina de Batata-Doce e Atum" loading="lazy" srcset="https://gomesdacosta.com.br/app/uploads/2017/04/salada-natalina-de-batata-doce-e-atum-mobile.jpg 320w, https://gomesdacosta.com.br/app/uploads/2017/04/salada-natalina-de-batata-doce-e-atum-mobile-300x234.jpg 300w" sizes="(max-width: 320px) 100vw, 320px">				
                            </div>
                            <div class="saudabilidade-recipe-content">
                                <h3>Salada Natalina de Batata-Doce e Atum</h3>
                                <div class="btn-saudabilidade-recipe">
                                        Confira a receita completa. 
                                    </div>
                            </div>
                        </div>
                    </a>
                </div>  

                <div class="recipe-saudabilidade-item swiper-slide home-page__slide-top-margin-control">
                    <a href="https://gomesdacosta.com.br/receitas/atum-ao-marinheiro/" class="home-page__recipe-item" onclick="dataLayer.push({ 'event':'GAEvent', 'eventCategory': 'saudabilidade', 'eventAction': 'click', 'eventLabel': 'confira-a-receita-completa:que-tal-conhecer-as-nossas-receitas:Atum-ao-Marinheiro'});">
                        <div class="saudabilidade-recipe-box">
                            <div class="saudabilidade-recipe-img">
                                <img width="320" height="250" src="https://gomesdacosta.com.br/app/uploads/2017/04/atum-ao-marinheiro-mobile.jpg" class="attachment-medium-card size-medium-card wp-post-image" alt="Atum ao Marinheiro" loading="lazy" srcset="https://gomesdacosta.com.br/app/uploads/2017/04/atum-ao-marinheiro-mobile.jpg 320w, https://gomesdacosta.com.br/app/uploads/2017/04/atum-ao-marinheiro-mobile-300x234.jpg 300w" sizes="(max-width: 320px) 100vw, 320px">		
                            </div>
                            <div class="saudabilidade-recipe-content">
                                <h3>Atum ao Marinheiro</h3>
                                <div class="btn-saudabilidade-recipe">
                                    Confira a receita completa. 
                                </div>
                            </div>
                        </div>
                    </a>
                </div>  

                <div class="recipe-saudabilidade-item swiper-slide home-page__slide-top-margin-control">
                    <a href="https://gomesdacosta.com.br/receitas/penne-gomes-da-costa/ " class="home-page__recipe-item" onclick="dataLayer.push({ 'event':'GAEvent', 'eventCategory': 'saudabilidade', 'eventAction': 'click', 'eventLabel': 'confira-a-receita-completa:que-tal-conhecer-as-nossas-receitas:Penne-Gomes-da-Costa'});">
                        <div class="saudabilidade-recipe-box">
                            <div class="saudabilidade-recipe-img">
                                <img width="320" height="250" src="https://gomesdacosta.com.br/app/uploads/2017/04/penne-gomes-da-costa-mobile.jpg" class="attachment-medium-card size-medium-card wp-post-image" alt="Penne Gomes da Costa" loading="lazy" srcset="https://gomesdacosta.com.br/app/uploads/2017/04/penne-gomes-da-costa-mobile.jpg 320w, https://gomesdacosta.com.br/app/uploads/2017/04/penne-gomes-da-costa-mobile-300x234.jpg 300w" sizes="(max-width: 320px) 100vw, 320px">				
                            </div>
                            
                            <div class="saudabilidade-recipe-content">
                                <h3>Penne Gomes da Costa </h3>
                                <div class="btn-saudabilidade-recipe">
                                    Confira a receita completa. 
                                </div>
                            </div>
                        </div>
                    </a>
                </div> 
                
                <div class="recipe-saudabilidade-item swiper-slide home-page__slide-top-margin-control">
                    <a href="https://gomesdacosta.com.br/receitas/quiche-integral-de-cogumelos-e-atum/" class="home-page__recipe-item" onclick="dataLayer.push({ 'event':'GAEvent', 'eventCategory': 'saudabilidade', 'eventAction': 'click', 'eventLabel': 'confira-a-receita-completa:Quiche-Integral-de-Cogumelos-e-Atum'});">
                        <div class="saudabilidade-recipe-box">
                            <div class="saudabilidade-recipe-img">
                                <img width="320" height="250" src="https://gomesdacosta.com.br/app/uploads/2017/04/quiche-integral-de-cogumelos-e-atum-mobile.jpg" class="attachment-medium-card size-medium-card wp-post-image" alt="Quiche Integral de Cogumelos e Atum" loading="lazy" srcset="https://gomesdacosta.com.br/app/uploads/2017/04/quiche-integral-de-cogumelos-e-atum-mobile.jpg 320w, https://gomesdacosta.com.br/app/uploads/2017/04/quiche-integral-de-cogumelos-e-atum-mobile-300x234.jpg 300w" sizes="(max-width: 320px) 100vw, 320px">				
                            </div>
                            <div class="saudabilidade-recipe-content">
                                <h3>Quiche Integral de Cogumelos e Atum</h3>
                                <div class="btn-saudabilidade-recipe">
                                    Confira a receita completa. 
                                </div>
                            </div>
                        </div>
                    </a>
                </div> 
            </div>
            <div class="swiper-button-prev product-slide__prev saudabilidade-receitas-prev"></div>
            <div class="swiper-button-next product-slide__next saudabilidade-receitas-next"></div>
            <div class="col-12">
                <div class="home-page__recipe-button" onclick="dataLayer.push({ 'event':'GAEvent', 'eventCategory': 'saudabilidade', 'eventAction': 'click', 'eventLabel': 'veja-todas-as-receitas'});">
                    <a href="https://gomesdacosta.com.br/receitas-gomes-da-costa/" class="btn-saudabilidade-ver-receitas">
                        Veja todas as receitas
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="home-page__slide-bottom">
		<div class="container" onclick="dataLayer.push({ 'event':'GAEvent', 'eventCategory': 'saudabilidade', 'eventAction': 'click', 'eventLabel': 'conheca-nossos-produtos:pates'});">
			<?php Theme::partial( 'homepage/products-section' ); ?>
		</div>
	</div>
	
	<div class="home-page__social" onclick="dataLayer.push({ 'event':'GAEvent', 'eventCategory': 'saudabilidade', 'eventAction': 'click', 'eventLabel': 'acompanhe-nossas-redes-sociais:facebook'});">	
		<?php Theme::partial( 'homepage/social' ); ?>
	</div>
</div>

<style>
  @font-face {
    font-family: 'Dolpino';
    src: url('https://homolog.gomesdacosta.com.br/wp-content/themes/gomesdacosta/dist/fonts/DolpinoRegular.woff2') format('woff2'),
        url('https://homolog.gomesdacosta.com.br/wp-content/themes/gomesdacosta/dist/fonts/DolpinoRegular.woff') format('woff');
    font-weight: normal;
    font-style: normal;
    font-display: swap;
}

@font-face {
    font-family: 'Bariol';
    src: url('https://gomesdacosta.com.br/wp-content/themes/gomesdacosta/dist/fonts/Bariol-Regular.woff2') format('woff2'),
        url('https://gomesdacosta.com.br/wp-content/themes/gomesdacosta/dist/fonts/Bariol-Regular.woff') format('woff');
    font-weight: normal;
    font-style: normal;
    font-display: swap;
}


.page-saudabilidade p{
  font-family: 'Bariol';
  font-size: 22px;
}
  



    .video-embed {
        position: relative;
        padding-bottom: 56.25%;
        height: 0;
        overflow: hidden;
        max-width: 100%;
        height: auto;
    }

    .video-embed embed, 
    .video-embed iframe, 
    .video-embed object, 
    .video-embed video {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border: none;
    }
    .page-saudabilidade .banner-top{
        margin-bottom: 30px;
    }
    .page-saudabilidade h1, .page-saudabilidade h2{
        color: #072e73;
        font-family: 'bearskin_demo', sans-serif;
        /* font-family: 'Dolpino'; */
        font-size: 57px;
        font-weight: bold;
        text-transform: uppercase;
        
    }
    @media only screen and (max-width: 700px) {
      .page-saudabilidade h1, .page-saudabilidade h2 {
        font-size: 31px;
        line-height: initial;
      }

    }
    .page-saudabilidade h1{
        font-size: 80px;
    }

    .page-saudabilidade h2{
        margin-bottom: 30px;
    }
    .page-saudabilidade .saudabilidade-cards h2{
        font-family: 'bearskin_demo', sans-serif;
        /* font-family: 'Dolpino'; */
        color: #072e73;
        margin-bottom: 20px;
        font-size: 39px;
    }

    .know-page__slide-top-container{
        list-style: none;
        margin-top: 25px;
        overflow: hidden;
        padding: 0;
        position: relative;
        z-index: 1;
    }


    .know-page__slide-top-container.swiper-container-horizontal > .swiper-pagination-bullets, 
    .know-page__slide-top-container.swiper-pagination-custom, .swiper-pagination-fraction{
        bottom: 40px !important;
        margin-top: 0 !important;
    }
    

    .know-page__slide-top-container .swiper-pagination-bullets-dynamic .swiper-pagination-bullet{
        transform: scale(1);
    }

    .know-page__slide-top-container .swiper-pagination-bullet{
        border: 1px solid #295a94 !important;
        background: #FFF !important;
        height: 13px !important;
        width: 13px !important;
    }

    .know-page__slide-top-container .swiper-pagination-bullet.swiper-pagination-bullet-active{
        background: #295a94 !important;
    }

    .page-saudabilidade .saudabilidade-cards h2{
        height: 60px;
        font-size:0;
    }

    .page-saudabilidade .saudabilidade-card-1 h2{
        background: url('https://gomesdacosta.com.br/app/uploads/2021/10/title-1.jpg') no-repeat center;
        background-size: contain;
    }
    .page-saudabilidade .saudabilidade-card-2 h2{
        background: url('https://gomesdacosta.com.br/app/uploads/2021/10/title-2.jpg') no-repeat center;
        background-size: contain;
    }
    .page-saudabilidade .saudabilidade-card-3 h2{
        background: url('https://gomesdacosta.com.br/app/uploads/2021/10/title-3.jpg') no-repeat center;
        background-size: contain;
    }
    .page-saudabilidade .saudabilidade-card-4 h2{
        background: url('https://gomesdacosta.com.br/app/uploads/2021/10/title-4.jpg') no-repeat center;
        background-size: contain;
    }
    .page-saudabilidade .saudabilidade-card-5 h2{
        background: url('https://gomesdacosta.com.br/app/uploads/2021/10/title-5.jpg') no-repeat center;
        background-size: contain;
    }
    @media (max-width: 780px) {
        .only-mobile{
            display: block !important;
        }
        .only-desktop{
            display: none !important;
        }
        
        .only-mobile .saudabilidade-cards{
            margin-top: 30px;
        }

        .only-mobile .page-saudabilidade p{
            margin-bottom: 0;
        }
        .page-saudabilidade .saudabilidade-cards h2{
            height: 95px;
        }
        .page-saudabilidade .saudabilidade-card-1 h2{
            background: url('https://gomesdacosta.com.br/app/uploads/2021/10/title-1-mobile.jpg') no-repeat center;
            background-size: contain;
        }
        .page-saudabilidade .saudabilidade-card-2 h2{
            background: url('https://gomesdacosta.com.br/app/uploads/2021/10/title-2-mobile.jpg') no-repeat center;
            background-size: contain;
        }
        .page-saudabilidade .saudabilidade-card-3 h2{
            background: url('https://gomesdacosta.com.br/app/uploads/2021/10/title-3-mobile.jpg') no-repeat center;
            background-size: contain;
        }
        .page-saudabilidade .saudabilidade-card-4 h2{
            background: url('https://gomesdacosta.com.br/app/uploads/2021/10/title-4-mobile.jpg') no-repeat center;
            background-size: contain;
        }
        .page-saudabilidade .saudabilidade-card-5 h2{
            background: url('https://gomesdacosta.com.br/app/uploads/2021/10/title-5-mobile.jpg') no-repeat center;
            background-size: contain;
        }

        .page-saudabilidade .home-page__slide-top-container .swiper-button-next, 
        .page-saudabilidade .home-page__slide-top-container .swiper-button-prev{
            top: 82%;
        }
    }

    @media (min-width: 781px) {
        .only-mobile{
            display: none !important;
        }
        .only-desktop{
            display: block !important;
        }
    }

    @media only screen and (max-width: 560px) {

      .saudabilidade-cards-img {
      order: 2;
      }	

      /* .saudabilidade-cards-text {
      order: 1;
      }	 */

    }

    .page-saudabilidade .saudabilidade-cards .row{
        align-items: center;
    }

    .page-saudabilidade .recipe-saudabilidade-slider {
        list-style: none;
        margin-top: 25px;
        overflow: hidden;
        padding: 0;
        position: relative;
        z-index: 1;
    }

    .page-saudabilidade .recipe-saudabilidade-item{
        padding: 10px;
    }

    .page-saudabilidade .saudabilidade-recipe-box{
        box-shadow: 0px 0px 10px #666;
        border-radius: 10px;
        overflow: hidden;
        padding-top: 30px;
        padding-bottom: 30px;
    }

    .page-saudabilidade .saudabilidade-recipe-content{
        padding: 0 20px;
    }

    .page-saudabilidade .saudabilidade-recipe-content h3{
        text-align: center;
        font-size: 25px;
        margin: 20px 0;
        color: #072e73;
        min-height: 100px;
        display: flex;
        justify-content: center;
    }

    .page-saudabilidade .btn-{
        text-align: center;
    }
    .page-saudabilidade .btn-saudabilidade-recipe{
        background: #072e73;
        color: #FFF;
        text-align: center;
        padding: 5px 10px;
        border-radius: 16px;
        text-transform: uppercase;
    }

    .page-saudabilidade .home-page__recipe-button{
        padding: 30px 0;
        text-align: center;
    }

    .page-saudabilidade .home-page__recipe-button a{
        padding: 12px 30px;
        border-radius: 22px;
        border: 2px solid #072e73;
        color: #072e73;
        box-shadow: 4px 2px 4px;
        transition: 0.3s ease background;
    }

    .page-saudabilidade .home-page__recipe-button a:hover{
        color: #FFF;
        background: #072e73;
    }

    .saudabilidade-cards-text{
        text-align: center;
    }

    .do-you-know{
        margin-top: 40px; 
    }

    .saudabilidade-video-top{
        margin-top: 30px;
    }

    .page-saudabilidade .next-saudabilidade-banner, .page-saudabilidade .saudabilidade-receitas-next{
        background: url('https://gomesdacosta.com.br/app/uploads/2021/10/right.png');
        background-size: cover;
        height: 64px;
        width: 64px;
    }

    .page-saudabilidade .prev-saudabilidade-banner, .page-saudabilidade .saudabilidade-receitas-prev{
        background: url('https://gomesdacosta.com.br/app/uploads/2021/10/left.png');
        background-size: cover;
        height: 64px;
        width: 64px;
    }

    .page-saudabilidade .saudabilidade-receitas-next,
    .page-saudabilidade .saudabilidade-receitas-prev{
        top: 40%;
        height: 50px !important;
        width: 50px !important;
    }

    .page-saudabilidade .next-saudabilidade-banner:after, 
    .page-saudabilidade .prev-saudabilidade-banner:after,
    .page-saudabilidade .saudabilidade-receitas-next:after,
    .page-saudabilidade .saudabilidade-receitas-prev:after{
        display:none;
    }

    .img-title{
        width: inherit;
        max-width: 100%;
        margin: 0 auto;
    }

    .only-mobile{

    }

</style>