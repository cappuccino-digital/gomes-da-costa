<?php declare( strict_types = 1 );

/**
 * Template Name: Página Fale Conosco
 * App Layout: layouts/app.php
 */

?>
<section class="page-contact-us">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="page-contact-us__banner">
					<?php the_post_thumbnail( 'full' ); ?>
				</div>
				<div class="page-contact-us__listen">
					<div class="page-contact-us__title">
						<h1 class="title title--accent title--head">
							Queremos te ouvir
						</h1>
					</div>
					<div class="page-contact-us__contact-box">
						<div class="contact-box">
							<div class="contact-box__title">
								<h3 class="title title--black">
									Contate-nos
								</h3>
							</div>
							<div class="contact-box__item">
								<div class="contact-box__item-header">
									<div class="contact-box__item-icon">
										<svg>
											<use xlink:href="#phone-call"></use>
										</svg>
									</div>
									<div class="contact-box__item-title">
										<h4 class="contact-box__item-title-content">
											Atendimento ao cliente
										</h4>
									</div>
								</div>
								<div class="contact-box__item-body">
									<div class="contact-box__item-highlighted">
										<a class="contact-box__item-highlighted-content" href="tel:08007041954">0800 704 1954</a>
									</div>
									<div class="contact-box__item-highlighted">
										<a class="contact-box__item-highlighted-content" href="mailto:08007041954">sac@gomesdacosta.com.br</a>
									</div>

									<div class="contact-box__item-description">
										<p class="contact-box__item-description--text-blue contact-box__item-description--no-margin">Segunda a sexta, das 09h às 18h</p>
									</div>
								</div>
							</div>
							<div class="contact-box__item">
								<div class="contact-box__item-header">
									<div class="contact-box__item-icon">
										<svg>
											<use xlink:href="#interview"></use>
										</svg>
									</div>
									<div class="contact-box__item-title">
										<h4 class="contact-box__item-title-content">
											Assessoria de imprensa
										</h4>
									</div>
								</div>
								<div class="contact-box__item-body">
									<div class="contact-box__item-description">
										<div class="contact-box__item-description-wrapper">
											<p class="contact-box__item-description--text-blue">Alfredo César de Souza</p>
											<p class="contact-box__item-description--text-blue">
												<a href="tel:+551151890903" class="contact-box__item-description--text-blue">
													(11) 5189-0903
												</a>
											</p>
											<p class="contact-box__item-description--text-blue">
												<a href="mailto:alfredo.cesar@saxcomunicacao.com" class="contact-box__item-description--text-blue">
													alfredo.cesar@saxcomunicacao.com
												</a>
											</p>
										</div>
										<div class="contact-box__item-description-wrapper">
											<p class="contact-box__item-description--text-blue">Talita Lima</p>
											<p class="contact-box__item-description--text-blue">
												<a href="tel:+551151890921" class="contact-box__item-description--text-blue">
													(11) 5189-0921
												</a>
											</p>
											<p class="contact-box__item-description--text-blue">
												<a href="mailto:talita.lima@saxcomunicacao.com" class="contact-box__item-description--text-blue">
													talita.lima@saxcomunicacao.com
												</a>
											</p>
										</div>
									</div>
								</div>
							</div>
							<div class="contact-box__item">
								<div class="contact-box__item-header">
									<div class="contact-box__item-icon">
										<svg>
											<use xlink:href="#team"></use>
										</svg>
									</div>
									<div class="contact-box__item-title">
										<h4 class="contact-box__item-title-content">
											Trabalhe conosco
										</h4>
									</div>
								</div>
								<div class="contact-box__item-body">
									<div class="contact-box__item-description">
										<div class="contact-box__item-description-wrapper">
											<p class="contact-box__item-link">
												<a href="https://platform.senior.com.br/hcmrs/hcm/curriculo/?tenant=gomesdacosta&tenantdomain=gomesdacosta.com.br#!/login/register" class="link link--medium" target="_blank">Cadastre seu currículo</a>
											</p>
											<p class="contact-box__item-link">
												<a href="https://platform.senior.com.br/hcmrs/hcm/curriculo/?tenant=gomesdacosta&tenantdomain=gomesdacosta.com.br#!/vacancies/list" class="link link--medium" target="_blank">Conheça todas as nossas vagas</a>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php if ( $form_id ) : ?>
						<div class="page-contact-us__email">
							<?php gravity_form( $form_id, true, true, false, '', true ); ?>
						</div>
					<?php endif; ?>
				</div>
				<div class="page-contact-us__location">
					<div class="page-contact-us__accordion">
						<div class="page-contact-us__accordion-item">
							<div id="accordion-1" class="accordion accordion--shadow">
								<div class="accordion__title" id="heading-1" data-toggle="collapse" data-target="#collapse-1" aria-expanded="true" aria-controls="collapse-1">
									<h2 class="title title--sun title--large title--head">
										Onde estamos
									</h2>
									<div class="accordion__title-icon">
									</div>
								</div>
								<div id="collapse-1" class="collapse" aria-labelledby="heading-1" data-parent="#accordion-1">
									<div class="accordion__content accordion__content accordion__content--wrap pb-1 px-4">
										<div class="accordion__wrapper">
											<h3 class="title title--black mb-2">
												Buenos Aires, Argentina
											</h3>
											<p class="accordion__wrapper-text">Rua Carlos Pellegrini, 989</p>
											<p class="accordion__wrapper-text">Piso 6</p>
											<p class="accordion__wrapper-text">Capital Federal/Buenos Aires</p>
											<p class="accordion__wrapper-text">CEP: 1009</p>
											<p class="accordion__wrapper-text">Tel: 00 5411 4328-6813</p>
										</div>
										<div class="accordion__wrapper">
											<h3 class="title title--black mb-2">
												Maranhão
											</h3>
											<p class="accordion__wrapper-text">Edifício Golden Tower</p>
											<p class="accordion__wrapper-text">Rua Júpiter</p>
											<p class="accordion__wrapper-text">Lote 01 - Quadra 32 - Sala 914</p>
											<p class="accordion__wrapper-text">Renascença II, São Luís/MA</p>
											<p class="accordion__wrapper-text">CEP: 65075-441</p>
											<p class="accordion__wrapper-text">Tel: (98) 3226-2566</p>
										</div>
										<div class="accordion__wrapper">
											<h3 class="title title--black mb-2">
												Minas Gerais
											</h3>
											<p class="accordion__wrapper-text">Avenida Antônio Abrahão Caran, 430</p>
											<p class="accordion__wrapper-text">Sala 302</p>
											<p class="accordion__wrapper-text">São José, Belo Horizonte/MG</p>
											<p class="accordion__wrapper-text">CEP: 31275-000</p>
											<p class="accordion__wrapper-text">Tel: (31) 3281-3501</p>
											<p class="accordion__wrapper-text">Fax: (31) 3282-4588</p>
										</div>
										<div class="accordion__wrapper">
											<h3 class="title title--black mb-2">
												Pernambuco
											</h3>
											<p class="accordion__wrapper-text">Empresarial João Roma</p>
											<p class="accordion__wrapper-text">Avenida Conselheiro Aguiar, 2333</p>
											<p class="accordion__wrapper-text">5° andar - Sala 501</p>
											<p class="accordion__wrapper-text">Boa Viagem, Recife/PE</p>
											<p class="accordion__wrapper-text">CEP: 51020-020</p>
											<p class="accordion__wrapper-text">Tel: (81) 3377-0792</p>
											<p class="accordion__wrapper-text">Fax: (81) 3377-0804</p>
										</div>
										<div class="accordion__wrapper">
											<h3 class="title title--black mb-2">
												Rio de Janeiro
											</h3>
											<p class="accordion__wrapper-text">Edifício Barra Space Center</p>
											<p class="accordion__wrapper-text">Avenida das Américas, 1155</p>
											<p class="accordion__wrapper-text">Sala 1606</p>
											<p class="accordion__wrapper-text">Barra da Tijuca, Rio de Janeiro/RJ</p>
											<p class="accordion__wrapper-text">CEP: 22631-000</p>
											<p class="accordion__wrapper-text">Tel: (21) 2441-7179</p>
											<p class="accordion__wrapper-text">Fax: (21) 2439-4380</p>
										</div>
										<div class="accordion__wrapper">
											<h3 class="title title--black mb-2">
												Rio Grande do Sul
											</h3>
											<p class="accordion__wrapper-text">Rua Quintino Bocaiúva, 683</p>
											<p class="accordion__wrapper-text">Sala 503</p>
											<p class="accordion__wrapper-text">Moinho de Vento, Porto Alegre/RS</p>
											<p class="accordion__wrapper-text">CEP: 90440-051</p>
											<p class="accordion__wrapper-text">Tel: (51) 3330-2138</p>
										</div>
										<div class="accordion__wrapper">
											<h3 class="title title--black mb-2">
												São Paulo
											</h3>
											<p class="accordion__wrapper-text">Edifício Vila Olímpia Corporate</p>
											<p class="accordion__wrapper-text">Rua São Tome nº 86</p>
											<p class="accordion__wrapper-text">9º andar</p>
											<p class="accordion__wrapper-text">Vila Olímpia, São Paulo/SP</p>
											<p class="accordion__wrapper-text">CEP: 04551-080</p>
											<p class="accordion__wrapper-text">Tel: (11) 5503-6800</p>
										</div>
										<div class="accordion__wrapper">
											<h3 class="title title--black mb-2">
												Escritório Central
											</h3>
											<p class="accordion__wrapper-text">Edifício Vila Olímpia Corporate</p>
											<p class="accordion__wrapper-text">Rua São Tome, 86</p>
											<p class="accordion__wrapper-text">9º andar</p>
											<p class="accordion__wrapper-text">Vila Olímpia, São Paulo/SP</p>
											<p class="accordion__wrapper-text">CEP: 04551-080</p>
											<p class="accordion__wrapper-text">Tel: 0800 704 1954</p>
										</div>
										<div class="accordion__wrapper">
											<h3 class="title title--black mb-2">
												Fábrica de Embalagens
											</h3>
											<p class="accordion__wrapper-text">Avenida Presidente Castelo Branco, 640</p>
											<p class="accordion__wrapper-text">Salseiros, Itajaí/SC</p>
											<p class="accordion__wrapper-text">CEP: 88311-470</p>
											<p class="accordion__wrapper-text">Tel: (47) 3241-8800</p>
											<p class="accordion__wrapper-text">Fax: (47) 3241-8805</p>
										</div>
										<div class="accordion__wrapper">
											<h3 class="title title--black mb-2">
												Fábrica GDC Alimentos S/A
											</h3>
											<p class="accordion__wrapper-text">Rua Eugênio Pezzini, 500</p>
											<p class="accordion__wrapper-text">Cordeiros, Itajaí/SC</p>
											<p class="accordion__wrapper-text">CEP: 88311-000</p>
											<p class="accordion__wrapper-text">Tel: (47) 3341-2600</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="home-page__slide-bottom">
		<div class="container">
			<?php Theme::partial( 'homepage/products-section' ); ?>
		</div>
	</div>

	<div class="home-page__social">
		<?php
			Theme::partial( 'homepage/social' );
		?>
	</div>
</section>
