<?php declare( strict_types = 1 );

/**
 * Template Name: Página inicial de receitas
 * App Layout: layouts/app.php
 */

Theme::partial( 'recipes/home-header', compact( 'title', 'all_recipe_image' ) );
?>

<div class="category-recipes">
	<div class="row">
		<?php foreach ( $recipes as $recipe ) : ?>
			<a class="col-12 col-sm-6 col-lg-4 recipe-item" href="<?php echo esc_url( get_permalink( $recipe ) ); ?>">
				<div class="box box--recipe box--desktop mt-4">
					<div class="box__image">
						<?php echo get_the_post_thumbnail( $recipe, 'medium-card', ['class' => 'box__image-content'] ); ?>
					</div>
					<div class="box__description">
						<div class="box__title box__title--full">
							<h4 class="title color-blue"><?php echo esc_html( get_the_title( $recipe ) ); ?></h4>
						</div>
					</div>
				</div>
			</a>
		<?php endforeach; ?>

		<div class="col-12">
			<div class="category-recipes__button">
				<a href="<?php echo esc_url( $recipes_link ); ?>" class="btn-default btn-default--large btn-default--section my-3">veja todas as receitas</a>
			</div>
		</div>
	</div>

	<div class="col-12">
		<?php Theme::partial( 'recipes/popular-recipes' ); ?>
	</div>	
	<div class="home-page__slide-bottom">
		<div class="container">
			<?php Theme::partial( 'homepage/products-section' ); ?>
		</div>
	</div>

	<div class="home-page__social">
		<?php
			Theme::partial( 'homepage/social' );
		?>
	</div>
</div>

