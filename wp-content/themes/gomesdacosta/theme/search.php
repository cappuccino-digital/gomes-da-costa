<?php declare( strict_types = 1 );

/**
 * Template Name: Página de Busca
 * App Layout: layouts/app.php
 */

if ( ! is_archive() ) :
	?>
<div class="search__result container">
	<?php Theme::partial( 'search/search-filters' ); ?>
	<div class="row">
		<?php if ( ! have_posts() ) : ?>
			<?php Theme::partial( 'shared/not-found', ['title' => 'Que Pena!', 'description' => 'Parece que ainda não temos o que você buscou.', 'has_back_to_navigation' => true] ); ?>
		<?php endif; ?>
		<?php while ( have_posts() ) : ?>
			<?php the_post(); ?>
			<div class="col-12 col-sm-6 col-lg-4">
				<a href="<?php echo esc_attr( get_the_permalink() ); ?>" class="search__result-card">
					<div class="box box--recipe box--has-border mt-4">
						<div class="box__image search__result-card-image">
							<?php the_post_thumbnail( 'medium-card' ); ?>
						</div>
						<div class="box__description">
							<div class="box__title box__title--full">
								<?php echo sprintf( '<h4 class="title title--accent">%s</h4>', esc_attr( get_the_title() ) ); ?>
							</div>
						</div>
					</div>
				</a>
			</div>
		<?php endwhile; ?>
	</div>
		<?php Theme::partial( 'shared/pagination' ); ?>
</div>

<div class="home-page__slide-bottom">
	<div class="container">
		<?php Theme::partial( 'homepage/products-section' ); ?>
	</div>
</div>

<div class="home-page__social">
	<?php
		Theme::partial( 'homepage/social' );
	?>
</div>


	<?php
else :
	require 'archive-fw-receitas.php';
endif;
