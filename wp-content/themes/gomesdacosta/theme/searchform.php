<?php declare( strict_types = 1 );

/**
 * Search form partial.
 *
 * @link https://codex.wordpress.org/Styling_Theme_Forms#The_Search_Form
 * @package WPEmergeTheme
 */

?>
<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="search-form" method="get" role="search">
	<label for="s" class="search-form__label">
		<span class="screen-reader-text"><?php esc_html_e( 'Search for:', 'app' ); ?></span>

		<input required type="text" title="<?php esc_attr_e( 'Search for:', 'app' ); ?>" name="s" value="" id="s" placeholder="<?php esc_attr_e( 'Digite sua busca &hellip;', 'app' ); ?>" class="search-form__field header__search-bar"/>
	</label>

	<button type="submit" class="search-form__submit-button">
		<svg class="search-form__iconSearch header__iconSearch">
			<use xlink:href="#search"></use>
		</svg>
	</button>
	<svg class="search-form__iconClose header__iconClose">
		<use xlink:href="#close"></use>
	</svg>
</form>
