<?php
/**
 * Plugin Name: Apiki Host Purge Cache
 * Plugin URI: https://apiki.com
 * Description: This plugin works cleaning the custom cache generated on our server
 * Version: 1.0
 * Author: Apiki WordPress
 * Author URI: https://apiki.com
 */


defined('apk_log_erros') or define('apk_log_erros', false);
add_action('rt_nginx_helper_after_fastcgi_purge_all', 'apkhost_purge_all');

#when ssl is not chain
add_filter('https_ssl_verify', '__return_false');
add_filter('https_local_ssl_verify', '__return_false');

function apkhost_purge_all() {
    $url = site_url('/*');
    apkhost_purge($url);
}

add_action('rt_nginx_helper_purged_file', 'apkhost_purge_url' );
add_action('rt_nginx_helper_before_remote_purge_url', 'apkhost_purge_url' );

function apkhost_purge_url( $url ){
        apkhost_purge($url);
}


function apkhost_purge( $url = '/' ){
        $url = preg_replace('#purge/#', '', $url);
        $parse = wp_parse_url( $url );
    $_url_purge_base = $parse['scheme'] . '://' . $parse['host'] . '/clean-cache'. $parse['path'];

    $url = $_url_purge_base;
    $args = array(
        'method' => 'PURGE'
    );

    if( apk_log_erros == true ):
        $msg = sprintf(' <---> Purge URL: %s at time: %s', $url,  date_i18n( 'F j, Y H:i:s' ));
        error_log( $msg );
    endif;

    $response = wp_remote_request( $url, $args );
}