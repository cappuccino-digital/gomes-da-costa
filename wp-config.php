<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'gomesdacosta' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'C@ppuccino' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

// define('WP_SITEURL', 'http://localhost:8080');
// define('WP_HOME', 'http://localhost:8080');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'a(%KE;3rlteclLeO0X. sKD5E/x|>vB4{4m2}<T.!94JX4T+J3),0fgh6?n2K.tS');
define('SECURE_AUTH_KEY',  'nsUKg1EFh n%e95KE+yl^6X[s6JDJCwD>]Cgi?4j?=6$u+T]c`B5<>d-`|YX<lNW');
define('LOGGED_IN_KEY',    '96tR$J5HFT7nXecRz=M;8_r[Df>,mnh|(?Oh*3j}0v](Wt<qwz/}D~L@N;nog7|1');
define('NONCE_KEY',        ';38,-5.}@R4-X.`.-tG-{*6JRx1]DG$UhE739.A,w</>R**Lm7Fj)04:TM2<#pu-');
define('AUTH_SALT',        'l ^4?/dni)xHC?__-polZ$t5zZ!LPK:;2n%Xr3?:lSZu@ana3(X}Hs3B7[}RY1v8');
define('SECURE_AUTH_SALT', '=>O:.EU$)P;&Ofi{|G.5M+A+..LMuUFj0i!LOw$gP$!fKsO--q4cyN]>C?T?)v[I');
define('LOGGED_IN_SALT',   'XO-t5NS#al-V>!m|%hHj3a2f=ztA*n>0Cq4s[#!K-^vM[J5`Jg`+19>F&fXyuIc~');
define('NONCE_SALT',       'E]=w/[zeK}l]0h#mv3}p2IcJeb6BO=Mj+,+aoM*##&+MSj*M})9@a~>i}c%M)6?8');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

// define( 'WP_HOME', 'https://homolog.gomesdacosta.com.br' );
// define( 'WP_SITEURL', 'https://homolog.gomesdacosta.com.br' );

define('WP_SITEURL', 'http://localhost:8080');
define('WP_HOME', 'http://localhost:8080');

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
